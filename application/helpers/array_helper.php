<?php
/**
 * Created by PhpStorm.
 * User: Jesus Enmanuel
 * Date: 26/08/2018
 * Time: 11:17
 */

/**
 * Converts MySQL object to an associative array ej: Dropdown list
 *
 * @access	public
 * @param	object
 * @return	array
 */
if ( ! function_exists('to_assoc_array'))
{
    function to_assoc_array($result)
    {
        $option[0] = '';
        foreach($result as $row)
        {
            $option[$row->id] = $row->name;
        }
        return $option;
    }
}

/**
 * Convert to rule in array
 *
 * @access public
 * @param  array
 * @return array
 */
if(!function_exists('string_to_array'))
{
    function string_to_array($data)
    {
        $explode_value                        = array();
        $value                                = explode(',', $data);

        foreach ($value AS $key => $values)
        {
            $explode_array                    = explode(':', $value[$key]);
            $explode_value[$explode_array[0]] = $explode_array[1];
        }

        return $explode_value;
    }
}


/**
 * Converts an array to an string.
 *
 * @access	public
 * @param	array
 * @return	string
 */

if (!function_exists('array_to_string'))
{
    function array_to_string($array, $glue = ",")
    {
        $string = '';
        if($array)
        {
            $string = implode($glue, $array);
        }

        return $string;
    }
}

/**
 * Converts an array to an string.
 *
 * @access	public
 * @param	array
 * @return	string
 */

if (!function_exists('string_to_array2'))
{
    function string_to_array2($string2, $glue = ",")
    {
        $string = '';
        if($string2)
        {
            $string = explode($glue, $string2);
        }

        return $string;
    }
}

if ( ! function_exists('arrayToObject'))
{
    function arrayToObject($array)
    {
        $object = new stdClass();
        foreach ($array as $key => $value)
        {
            $object->$key = $value;
        }
        return $object;
    }
}