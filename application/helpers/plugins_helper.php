<?php
/**
 * Created by PhpStorm.
 * User: Jesus Enmanuel
 * Date: 26/08/2018
 * Time: 11:17
 */

if( ! function_exists('display_css_files')){
    function display_css_files($module)
    {
        $files = "";
        switch($module)
        {
            case "main":
                $files .= '<link href="'.base_url().'assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />';
                $files .= '<link href="'.base_url().'assets/css/bootstrap.css" rel="stylesheet" type="text/css" />';
                $files .= '<link href="'.base_url().'assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />';
                $files .= '<link href="'.base_url().'assets/plugins/ladda/dist/ladda-themeless.min.css" rel="stylesheet" type="text/css" />';
                $files .= '<link href="'.base_url().'assets/plugins/daterangepicker/css/daterangepicker.min.css" rel="stylesheet" type="text/css" />';
                $files .= '<link href="'.base_url().'assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />';
                $files .= '<link href="'.base_url().'assets/plugins/formValid/dist/jquery.formValid.min.css" rel="stylesheet" type="text/css" />';

                $files .= '<link href="'.base_url().'assets/plugins/styleCheckbox/customize.checkbox.css" rel="stylesheet" type="text/css" />';
                $files .= '<link href="'.base_url().'assets/plugins/styleCheckbox/customize.radio.css" rel="stylesheet" type="text/css" />';
                $files .= '<link href="'.base_url().'assets/plugins/jquery.filer/css/jquery.filer.css" rel="stylesheet" type="text/css" />';
                $files .= '<link href="'.base_url().'assets/plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css" rel="stylesheet" type="text/css" />';
                $files .= '<link href="'.base_url().'assets/plugins/jasny-bootstrap/css/jasny-bootstrap.css" rel="stylesheet" type="text/css" />';

                break;

            case "calendar":
                $files .= '<link href="'.base_url().'assets/plugins/colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css" />';
                $files .= '<link href="'.base_url().'assets/css/modules/calendar.css" rel="stylesheet" type="text/css" />';
                break;

            case "clients":
                $files .= '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/plugins/datatables/dataTables.bootstrap4.min.css"/>';
                $files .= '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/plugins/bootstrap-datetimepicker-master/src/css/bootstrap-datetimepicker.css"/>';
//                $files .= '<link href="'.base_url().'assets/plugins/jquery.filer/css/jquery.filer.css" rel="stylesheet" />';
//		        $files .= '<link href="'.base_url().'assets/plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css" rel="stylesheet" />';

                $files .= '<link href="'.base_url().'assets/css/modules/clients.css" rel="stylesheet" type="text/css" />';
                break;

            case "loan_analysis":
                $files .= '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/plugins/datatables/dataTables.bootstrap4.min.css"/>';
                $files .= '<link href="'.base_url().'assets/css/modules/loan_analysis.css" rel="stylesheet" type="text/css" />';
                break;

            case "dashboard":
                $files .= '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/plugins/datatables/dataTables.bootstrap4.min.css"/>';
                $files .= '<link href="'.base_url().'assets/css/modules/dashboard.css" rel="stylesheet" type="text/css" />';
                break;

            case "employees":
                $files .= '<link rel="stylesheet" type="text/css" href="'.base_url().'assets/plugins/datatables/dataTables.bootstrap4.min.css"/>';
                $files .= '<link href="'.base_url().'assets/css/modules/employees.css" rel="stylesheet" type="text/css" />';
                break;

            case "institution_banks":
                $files .= '<link href="'.base_url().'assets/css/modules/institution_banks.css" rel="stylesheet" type="text/css" />';
                break;

            case "compulsive_payments":
                $files .= '<link href="'.base_url().'assets/css/modules/compulsive_payments.css" rel="stylesheet" type="text/css" />';
                break;

            case "company":
                $files .= '<link href="'.base_url().'assets/css/modules/company.css" rel="stylesheet" type="text/css" />';
                break;
        }

        return $files;
    }
}

if( ! function_exists('display_js_files')){
    function display_js_files($module)
    {
        $files = "";
        switch($module)
        {
            case "main":
                $files .= '<script src="'.base_url().'assets/js/modernizr.min.js"></script>';
                $files .= '<script src="'.base_url().'assets/js/jquery.min.js"></script>';
                $files .= '<script src="'.base_url().'assets/js/moment.min.js"></script>';
                $files .= '<script src="'.base_url().'assets/js/popper.min.js"></script>';
                $files .= '<script src="'.base_url().'assets/js/bootstrap.min.js"></script>';
                $files .= '<script src="'.base_url().'assets/js/detect.js"></script>';
                $files .= '<script src="'.base_url().'assets/js/jquery.nicescroll.js"></script>';
                $files .= '<script src="'.base_url().'assets/plugins/ladda/dist/spin.min.js"></script>';
                $files .= '<script src="'.base_url().'assets/plugins/ladda/dist/ladda.min.js"></script>';
                $files .= '<script src="'.base_url().'assets/plugins/formValid/dist/jquery.formValid.min.js"></script>';
                $files .= '<script src="'.base_url().'assets/plugins/datatables/jquery.dataTables.min.js"></script>';
                $files .= '<script src="'.base_url().'assets/plugins/datatables/dataTables.bootstrap4.min.js"></script>';
                $files .= '<script src="'.base_url().'assets/plugins/pushjs/bin/push.min.js"></script>';
                $files .= '<script src="'.base_url().'assets/js/proccess.js"></script>';
                $files .= '<script src="'.base_url().'assets/plugins/mask/dist/jquery.mask.js"></script>';
                $files .= '<script src="'.base_url().'assets/plugins/numeric/jquery.numeric.js"></script>';
                $files .= '<script src="'.base_url().'assets/plugins/daterangepicker/js/daterangepicker.js"></script>';
                $files .= '<script src="'.base_url().'assets/plugins/sweetalert/sweetalert.js"></script>';
                $files .= '<script src="'.base_url().'assets/plugins/chosen/js/chosen.jquery.js"></script>';
                $files .= '<script src="'.base_url().'assets/js/dom.js"></script>';
                $files .= '<script src="'.base_url().'assets/js/global.helpers.js"></script>';
                $files .= '<script src="'.base_url().'assets/js/pikeadmin.js"></script>';
                $files .= '<script src="'.base_url().'assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>';
                $files .= '<script src="'.base_url().'assets/plugins/ajaxform/src/jquery.form.js"></script>';
//                $files .= '<script src="'.base_url().'assets/plugins/jasny-bootstrap/js/jasny-bootstrap.js"></script>';
                $files .= '<script src="'.base_url().'assets/plugins/fileinput/fileinput.js"></script>';
                break;

            case "calendar":
                $files .= '<script type="text/javascript" src="'.base_url().'assets/plugins/fullcalendar/fullcalendar.min.js"></script>';
                $files .= '<script type="text/javascript" src="'.base_url().'assets/plugins/fullcalendar/locale/es.js"></script>';
                $files .= '<script type="text/javascript" src="'.base_url().'assets/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>';
                $files .= '<script src="'.base_url().'assets/js/modules/calendar/calendar.js"></script>';
                break;

            case "clients":
                $files .= '<script src="'.base_url().'assets/plugins/bootstrap-datetimepicker-master/src/js/bootstrap-datetimepicker.js"></script>';
//                $files .= '<script src="'.base_url().'assets/plugins/jquery.filer/js/jquery.filer.min.js"></script>';
                $files .= '<script src="'.base_url().'assets/js/modules/clients/clients.js"></script>';
                break;

            case "loan_analysis":
                $files .= '<script src="'.base_url().'assets/js/modules/loan_analysis/loan_analysis.js"></script>';
                break;

            case "dashboard":
                $files .= '<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>';
                $files .= '<script src="'.base_url().'assets/plugins/waypoints/lib/jquery.waypoints.min.js"></script>';
                $files .= '<script src="'.base_url().'assets/plugins/counterup/jquery.counterup.min.js"></script>';
                $files .= '<script src="'.base_url().'assets/js/modules/dashboard/dashboard.js"></script>';
                break;

            case "employees":
                $files .= '<script src="'.base_url().'assets/js/modules/employees/employees.js"></script>';
                break;

            case "institution_banks":
                $files .= '<script src="'.base_url().'assets/js/modules/institution_banks/jasny-bootstrap.min.js"></script>';
                $files .= '<script src="'.base_url().'assets/js/modules/institution_banks/institution_banks.js"></script>';
                break;

            case "company":
                $files .= '<script src="http://www.jasny.net/bootstrap/dist/js/jasny-bootstrap.min.js"></script>';
                $files .= '<script src="'.base_url().'assets/js/modules/company/company.js"></script>';
                break;

            case "compulsive_payments":
                $files .= '<script src="'.base_url().'assets/js/modules/compulsive_payments/compulsive_payments.js"></script>';
                break;
        }

        return $files;
    }
}

