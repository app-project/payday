<?php
/**
 * Created by PhpStorm.
 * User: Jesus Enmanuel
 * Date: 26/08/2018
 * Time: 11:17
 */

if ( ! function_exists('_hours'))
{
    function _hours($interval = '+30 minutes')
    {
        $hours = array();
        $current = strtotime('00:00');
        $end = strtotime('23:59');
        while ($current <= $end) {
            $time = date('H:i', $current);
            $hours[$time] = date('h:i A', $current);
            $current = strtotime($interval, $current);
        }
        return $hours;
    }
}

/**
 * Timezones
 *
 * Returns an array of timezones.  This is a helper function
 * for various other ones in this library
 *
 * @access	public
 * @param	string	timezone
 * @return	string
 */
if ( ! function_exists('timezones'))
{
    function timezones($tz = '')
    {
        // Note: Don't change the order of these even though
        // some items appear to be in the wrong order

        $zones = array(
            'UM12'		=> -12,
            'UM11'		=> -11,
            'UM10'		=> -10,
            'UM95'		=> -9.5,
            'UM9'		=> -9,
            'UM8'		=> -8,
            'UM7'		=> -7,
            'UM6'		=> -6,
            'UM5'		=> -5,
            'UM45'		=> -4.5,
            'UM4'		=> -4,
            'UM35'		=> -3.5,
            'UM3'		=> -3,
            'UM2'		=> -2,
            'UM1'		=> -1,
            'UTC'		=> 0,
            'UP1'		=> +1,
            'UP2'		=> +2,
            'UP3'		=> +3,
            'UP35'		=> +3.5,
            'UP4'		=> +4,
            'UP45'		=> +4.5,
            'UP5'		=> +5,
            'UP55'		=> +5.5,
            'UP575'		=> +5.75,
            'UP6'		=> +6,
            'UP65'		=> +6.5,
            'UP7'		=> +7,
            'UP8'		=> +8,
            'UP875'		=> +8.75,
            'UP9'		=> +9,
            'UP95'		=> +9.5,
            'UP10'		=> +10,
            'UP105'		=> +10.5,
            'UP11'		=> +11,
            'UP115'		=> +11.5,
            'UP12'		=> +12,
            'UP1275'	=> +12.75,
            'UP13'		=> +13,
            'UP14'		=> +14
        );

        if ($tz == '')
        {
            return $zones;
        }

        if ($tz == 'GMT')
            $tz = 'UTC';

        return ( ! isset($zones[$tz])) ? 0 : $zones[$tz];
    }
}

if ( ! function_exists('_weekdays'))
{
    function _weekdays()
    {
        $weekdays = array(
            'SU'    =>'D',
            'MO'    =>'L',
            'TU'    =>'M',
            'WE'    =>'M',
            'TH'    =>'J',
            'FR'    =>'V',
            'SA'    =>'S'
        );

        return $weekdays;
    }
}

function timestamp_to_date($timestamp = "", $format = "d/m/Y H:i:s")
{
    if(empty($timestamp) || !is_numeric($timestamp)) $timestamp = time();
    return date($format, $timestamp);
}

if ( ! function_exists('gmt_to_local'))
{
    function gmt_to_local($time = '', $timezone = 'UTC', $dst = FALSE)
    {
        if ($time == '')
        {
            return now();
        }

        $time += timezones($timezone) * 3600;

        if ($dst == TRUE)
        {
            $time += 3600;
        }

        return $time;
    }
}

/**
 * Get "now" time
 *
 * Returns time() or its GMT equivalent based on the config file preference
 *
 * @access	public
 * @return	integer
 */
if ( ! function_exists('now'))
{
    function now()
    {
        $CI =& get_instance();

        if (strtolower($CI->config->item('time_reference')) == 'gmt')
        {
            $now = time();
            $system_time = mktime(gmdate("H", $now), gmdate("i", $now), gmdate("s", $now), gmdate("m", $now), gmdate("d", $now), gmdate("Y", $now));

            if (strlen($system_time) < 10)
            {
                $system_time = time();
                log_message('error', 'The Date class could not set a proper GMT timestamp so the local time() value was used.');
            }

            return $system_time;
        }
        else
        {
            return time();
        }
    }
}

/**
 * Human readable format
 *
 * Returns a date in this format:
 *	10 enero, 2018
 *
 * @access	public
 * @param	string a date
 * @return	integer
 */
if ( ! function_exists('human_date_format'))
{
    function human_date_format($date)
    {
        $d = explode("-", $date);
        return $d[2] . ' de ' . month_name($d[1]) . ', ' . $d[0];
    }
}

/**
 * Month Names
 *
 * Returns a month name
 *
 * @access	public
 * @param	integer	a number
 * @return	string
 */
if ( ! function_exists('month_name'))
{
    function month_name($num)
    {
        $months = array(
            '01' => 'Enero',
            '02' => 'Febrero',
            '03' => 'Marzo',
            '04' => 'Abril',
            '05' => 'Mayo',
            '06' => 'Junio',
            '07' => 'Julio',
            '08' => 'Agosto',
            '09' => 'Septiembre',
            '10' => 'Octubre',
            '11' => 'Noviembre',
            '12' => 'Diciembre'
        );

        return $months[$num];
    }
}

/**
 * Human readable format
 *
 * Returns a time in this format:
 *	10:00am
 *
 * @access	public
 * @param	string a date
 * @return	integer
 */
if ( ! function_exists('human_time_format'))
{
    function human_time_format($time)
    {
        return date('h:ia', strtotime($time));
    }
}

/**
 * isLeap
 *
 * Returns true or false:
 *
 * @access	public
 * @param
 * @return	boolean
 */
if ( ! function_exists('isLeap'))
{
    function isLeap()
    {
        return checkdate(2, 29, date('Y'));
    }
}
if ( ! function_exists('last_date'))
{
    function last_date($anho, $mes){
        if (((fmod($anho,4)==0) and (fmod($anho,100)!=0)) or (fmod($anho,400)==0)) {
            $dias_febrero = 29;
        } else {
            $dias_febrero = 28;
        }
        switch($mes) {
            case "01": return 31; break;
            case "02": return $dias_febrero; break;
            case "03": return 31; break;
            case "04": return 30; break;
            case "05": return 31; break;
            case "06": return 30; break;
            case "07": return 31; break;
            case "08": return 31; break;
            case "09": return 30; break;
            case "10": return 31; break;
            case "11": return 30; break;
            case "12": return 31; break;
        }
    }
}

if ( ! function_exists('next_payment_day'))
{
    function next_payment_day($payment_typeId, $day_work)
    {
        $CI =& get_instance();
        return $CI->custom_date->get_next_date($payment_typeId, array("day_work" => $day_work));
    }
}

if ( ! function_exists('payments_rule_biweekly_date'))
{
    function payments_rule_biweekly_date($date)
    {
        $result = array();
        $divide = explode("-", $date);

        if($divide[2] == "01" || $divide[2] == "16")
        {
            $result = array("01", "16");
        }
        if($divide[2] == "02" || $divide[2] == "17")
        {
            $result = array("02", "17");
        }
        if($divide[2] == "03" || $divide[2] == "18")
        {
            $result = array("03", "18");
        }
        if($divide[2] == "04" || $divide[2] == "19")
        {
            $result = array("04", "19");
        }
        if($divide[2] == "05" || $divide[2] == "20")
        {
            $result = array("05", "20");
        }
        if($divide[2] == "06" || $divide[2] == "21")
        {
            $result = array("06", "21");
        }
        if($divide[2] == "07" || $divide[2] == "22")
        {
            $result = array("07", "22");
        }
        if($divide[2] == "08" || $divide[2] == "23")
        {
            $result = array("08", "23");
        }
        if($divide[2] == "09" || $divide[2] == "24")
        {
            $result = array("09", "24");
        }
        if($divide[2] == "10" || $divide[2] == "25")
        {
            $result = array("10", "25");
        }
        if($divide[2] == "11" || $divide[2] == "26")
        {
            $result = array("11", "26");
        }
        if($divide[2] == "12" || $divide[2] == "27")
        {
            $result = array("12", "27");
        }
        if($divide[2] == "13" || $divide[2] == "28")
        {
            $result = array("13", "28");
        }
        if($divide[2] == "14" || $divide[2] == "29")
        {
            $result = array("14", "29");
        }
        if($divide[2] == "15" || $divide[2] == "30")
        {
            $result = array("15", "30");
        }
        return $result;
    }
}

if ( ! function_exists('payments_rule_weekly_date'))
{
    function payments_rule_weekly_date($date)
    {
        $result = array();
        $divide = explode("-", $date);

        if($divide[2] == "01" || $divide[2] == "08" || $divide[2] == "15" || $divide[2] == "22" || $divide[2] == "29")
        {
            $result = array("01", "08", "15", "22", "29");
        }
        if($divide[2] == "02" || $divide[2] == "09" || $divide[2] == "16" || $divide[2] == "23" || $divide[2] == "30" )
        {
            $result = array("02", "09", "16", "23", "30");
        }
        if($divide[2] == "03" || $divide[2] == "10" || $divide[2] == "17" || $divide[2] == "24" || $divide[2] == "31")
        {
            $result = array("03", "10", "17", "24", "31");
        }
        if($divide[2] == "04" || $divide[2] == "11" || $divide[2] == "18" || $divide[2] == "25")
        {
            $result = array("04", "11", "18", "25");
        }
        if($divide[2] == "05" || $divide[2] == "12" || $divide[2] == "19" || $divide[2] == "26")
        {
            $result = array("05", "12", "19", "26");
        }
        if($divide[2] == "06" || $divide[2] == "13" || $divide[2] == "20" || $divide[2] == "27")
        {
            $result = array("06", "13", "20", "27");
        }
        if($divide[2] == "07" || $divide[2] == "14" || $divide[2] == "21" || $divide[2] == "28")
        {
            $result = array("07", "14", "21", "28");
        }

        return $result;
    }
}

