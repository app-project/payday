<?php (defined('BASEPATH')) OR exit('No direct script access allowed');
/**
 * Created by PhpStorm.
 * User: Jesus Enmanuel
 * Date: 20/1/2018
 * Time: 4:59 PM
 */

class MY_Controller extends MX_Controller
{
    public $userId;
    public $full_name;
    public $typeId;
    public $email;

    public function __construct()
    {
        parent::__construct();

        $this->userId       = $this->session->userdata('userId');
        $this->full_name    = $this->session->userdata('full_name');
        $this->typeId       = $this->session->userdata('typeId');
        $this->email        = $this->session->userdata('email');

        $this->form_validation->CI =& $this;
        $this->form_validation->set_error_delimiters('<li>', '</li>');
    }

    public function strip_commas($string)
    {
        return str_replace(',','', $string);
    }

    public function strip_commas_2($string)
    {
        return str_replace('$','', str_replace(',','', $string));
    }

    public function format_date_($date, $format = 'Y-m-d')
    {
        return date_format( new DateTime($date), $format);
    }

    public function range_month()
    {
        $data['to']     = timestamp_to_date(gmt_to_local(now(), 'UTC', FALSE), "Y-m-t");
        $from           = timestamp_to_date(gmt_to_local(now(), 'UTC', FALSE), "Y-m-d");
        $data['from']   = date("Y-m", strtotime($from))."-01";

        return $data;
    }

    public function range_format($until = FALSE)
    {
        $param['from']  = $this->input->post('from');
        $param['to']    = $this->input->post('to');
        $param['range'] = $param['from'].' al '. $param['to'];

        if(empty($param['from']) || empty($param['to']))
        {
            $month  = $this->input->post('month');
            $year   = $this->input->post('year');

            if(empty($param['from']))
            {
                $param['from']   = $year.'-'.$month.'-01';
            }
            if(empty($param['to']))
            {
                $param['to']    = date("Y-m-t", strtotime($param['from']));
            }
            $param['range']  = strftime("Al %d de %B %Y", strtotime($param['to']));
            $param['from'] = ($until == FALSE)? $param['from'] : '';
        }
        return $param;
    }

    public function _numbers($limit = 31)
    {
        for ($i=1; $i <= $limit; $i++) {
            $numbers[$i] = $i;
        }

        return $numbers;
    }

    public function _Output($data)
    {
        $Mpdf = new Mpdf\Mpdf(array( 'mode' => 'c', 'default_font' => 'Arial', 'default_font_size' => 9));
        $file_name = ucfirst(str_replace(" ", "_", $data['file_name'])).'.pdf';
        $Mpdf->WriteHTML($data['pdf_view']);

        //I = Visualizar
        //D = Imprimir
        //S = text para enviar por correo.

        $pdf = $Mpdf->Output($file_name,'S');

        return $pdf;
    }
}