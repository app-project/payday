<?php
/**
 * Created by PhpStorm.
 * User: Jesus Enmanuel
 * Date: 20/1/2018
 * Time: 5:00 PM
 */

class MY_Model extends CI_Model
{
    public $table_name;
    public $view_name;
    public $primary_key = '';
    public $primaryFilter = 'intval';
    public $order_by = '';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $data
     * @param bool $id
     * @return bool
     */
    public function save($data, $id = FALSE)
    {
        if ($id == FALSE)
        {
            // Insert Data
            $this->db->insert($this->table_name, $data);
        }
        else
        {
            // Update Data
            $filter = $this->primaryFilter;
            $this->db->where($this->primary_key, $filter($id))->update($this->table_name, $data);
        }

        // Return the ID
        return $id == FALSE ? $this->db->insert_id() : $id;
    }

    /**
     * @param bool $module
     * @param bool $where
     * @return mixed
     */
    public function get($module = FALSE, $where = FALSE)
    {
        $table  = ($module == FALSE) ? $this->table_name : $this->view_name;

        if($where == FALSE)
        {

            return $this->db->get($table)->result();
        }
        else
        {
            return $this->db->where($where)->get($table)->result();
        }
    }

    /** return row or result in singles
     * @param bool $where
     * @param bool $single
     * @return mixed
     */
    public function get_by($where = FALSE, $single = FALSE)
    {
        is_array($where) || $where = array($where);
        $single = ($single != FALSE) ? 'row': 'result';

        return $this->db->where($where)->get($this->table_name)->$single();
    }

    /**
     * @param string $columns
     * @param bool $where
     * @param bool $module
     * @return mixed
     */
    public function datatable($columns = "*", $where = FALSE, $module = FALSE)
    {
        $table = ($module == FALSE) ? $this->table_name : $this->view_name;

        $this->db->select($columns);

        if($where == FALSE)
        {
            return $this->db->get($table)->result();
        }
        else
        {
            return $this->db->where($where)->get($table)->result();
        }
    }

    /**
     * @param string $columns
     * @param bool $where
     * @param bool $module
     * @return mixed
     */
    public function get_columns($columns = "*", $where = FALSE, $module = FALSE, $single = FALSE)
    {
        $table = ($module == FALSE) ? $this->table_name : $this->view_name;

        $single = ($single != FALSE) ? 'row': 'result';

        $this->db->select($columns);

        if($where == FALSE)
        {
            return $this->db->get($table)->result();
        }
        else
        {
            return $this->db->where($where)->get($table)->$single();
        }
    }
    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $filter = $this->primaryFilter;
        return $this->db->where($this->primary_key, $filter($id))
                        ->update($this->table_name, array('hidden' => 1));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function update(array $data, $id)
    {
        $filter = $this->primaryFilter;
        return $this->db->where($this->primary_key, $filter($id))
                        ->update($this->table_name, $data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get_single($id)
    {
        return $this->db->where($this->primary_key,$id)->get($this->table_name)->row();
    }

    /**
     * @param $columns
     * @param bool $where
     * @return array
     */
    public function get_assoc_list($columns, $where = FALSE, $placehoder = FALSE, $where_in = FALSE)
    {
        $this->db->select($columns);

        if($where != FALSE)
        {
            $this->db->where($where);
        }

        if($where_in != FALSE)
        {
            is_array($where_in) || $where_in = array($where_in);

            $this->db->where_in($this->primary_key, $where_in);
        }

        $query = $this->db->get($this->table_name)->result();

        $options = array();

        if($placehoder != TRUE)
        {
            $options[0] = "Seleccione una Opci&oacute;n";
        }

        foreach ($query AS $row)
        {
            $options[$row->id] = $row->name;
        }

        return $options;
    }

    /**
     * @param $where
     */
    public function count($where)
    {
        is_array($where) || $where = array($where);

        $this->db->where($where);
        $this->db->count_all_results($this->table_name);
    }

    /**
     * @param $where
     */
    public function count_by($where)
    {
        is_array($where) || $where = array($where);

        $this->db->where($where);
       $count =  $this->db->count_all_results($this->table_name);
       return $count;
    }

    /**
     * @param $where
     * @return mixed
     */
    public function in_table_by($where)
    {
        is_array($where) || $where = array($where);

       $count = $this->db->where($where)->from($this->table_name)->count_all_results();

       return $count;
    }

    /**
     * Checks if the record belongs to the user's company
     *
     * @param string field
     * @param mixed int or string
     * @return int
     */
    public function if_owner($key, $value)
    {
        $count = $this->db->where(array($key => $value))
            ->from($this->table_name)->count_all_results();
        return $return = ($count > 0)? TRUE : FALSE;
    }
}