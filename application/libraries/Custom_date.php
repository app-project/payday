<?php
/**
 * Created by PhpStorm.
 * User: Enmanuel
 * Date: 3/11/2019
 * Time: 6:50 PM
 */
use Carbon\Carbon;
class Custom_date
{
    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();
    }

    public function get_next_date($date_typeId, $data)
    {
        $result = FALSE;

        switch ($date_typeId)
        {
            case 1:
                $result = $this->get_next_monthly_date($date_typeId, $data);
                break;

            case 2:
                $result = $this->get_next_biweekly_date($date_typeId, $data);
                break;

            case 3:
                $result = $this->get_next_weekly_date($date_typeId, $data);
                break;
        }

        return $result;
    }

    private function get_next_monthly_date($date_typeId, $data)
    {
        $day_work       = $this->get_date_converted_carbon_object($data['day_work']);
        $current_day    = $this->get_date_converted_carbon_object(date("Y-m-d"));

        if($day_work->isCurrentMonth() && $day_work->isCurrentYear() && !$day_work->lessThan($current_day))
        {
            $exactly_date = $this->get_different_between_dates(date("Y-m-d"), $day_work, true);
            return $this->show_datetime_string($exactly_date);
        }
        else
        {
            $old_difference = $this->get_different_between_dates(date("Y-m-d"), $day_work, true);

            if(date("m") == "02")
            {
                if(isLeap())
                {
                    $day_work->subDays(1);
                }
                else
                {
                    $day_work->subDays(2);
                }
            }

            if($old_difference->invert == 1)
            {
                $multi_year = $old_difference->y * 12;
                $day_work->addMonth($old_difference->m + $multi_year + 1);
            }

            $exactly_date = $this->get_different_between_dates(date("Y-m-d"), $day_work, TRUE);
            return $this->show_datetime_string($exactly_date);
        }
    }

    private function get_next_biweekly_date($date_typeId, $data)
    {
        $result     = payments_rule_biweekly_date($data['day_work']);
        $divide     = string_to_array2($data['day_work'],"-");
        $date       = date("Y-m-d");
        $day        = date("d");
        $month      = date("m");
        $year       = date("Y");

        $day_work       = $this->get_date_converted_carbon_object($data['day_work']);
        $current_day    = $this->get_date_converted_carbon_object($date);

        if($day_work->isCurrentMonth() && $day_work->isCurrentYear() && !$day_work->lessThan($current_day))
        {
            $sub_date = ($day > 15) ? 0 : 15;
            $day_work->subDays($sub_date);
            $exactly_date = $this->get_different_between_dates($date, $day_work, true);
            return $this->show_datetime_string($exactly_date);
        }
        else
        {
            $next_date      = $this->get_date_converted_carbon_object($this->next_biweekly_date($year, $month, $day, $data['day_work']));

            if($month == "02")
            {
                if(isLeap())
                {
                    $next_date->subDays(1);
                }
                else
                {
                    $next_date->subDays(2);
                }
            }

            if($divide[1] == "02" && $divide[2] == "28")
            {
                if(isLeap())
                {
                    $next_date->addDay(1);
                }
                else
                {
                    $next_date->addDays(2);
                }
            }

            if($date != $next_date->toDateString())
            {
                if($day > $result[1])
                {
                    if(last_date($year, $month) == 31)
                    {
                        $next_date->addDays(16);
                    }
                    else
                    {
                        $next_date->addDays(15);
                    }
                }
            }

            $exactly_date   = $this->get_different_between_dates($date, $next_date, true);
            return $this->show_datetime_string($exactly_date);
        }
    }

    private function get_next_weekly_date($date_typeId, $data)
    {
        $date_to_pay = explode("-", $data['day_work']);
    }

    private function get_date_converted_carbon_object($date)
    {
        $date = explode("-", $date);
        return Carbon::createFromDate($date[0], (int)$date[1], (int)$date[2], "America/Santo_Domingo");
    }

    private function get_different_between_dates($current_date, $payment_date, $is_converted = FALSE)
    {
        $current_date = $this->get_date_converted_carbon_object($current_date);
        $date_to_pay  = ($is_converted == FALSE) ? $this->get_date_converted_carbon_object($payment_date) : $payment_date;
        return $current_date->diff($date_to_pay, false);
    }

    private function show_datetime_string($time)
    {
        $addY        = ($time->m != 0 && $time->d != 0) ? " y " : " ";
        $string      = "";
        $string     .= ($time->y != 0) ? ($time->y > 1) ? $time->y." años, " :  $time->y." año, " : "";
        $string     .= ($time->m != 0) ? ($time->m > 1) ? $time->m." meses" :  $time->m." mes" : "";
        $string     .= ($time->d != 0) ? ($time->d > 1) ? $addY.$time->d." dias" :  $addY.$time->d." dia" : "";
        return array($time->d, $string);
    }

    private function next_biweekly_date($year, $month, $day, $date_work)
    {
        $result         = payments_rule_biweekly_date($date_work);
        $result         = ($day <= $result[0]) ? $result[0] : $result[1];
        $return         = $year."-".$month."-".$result;
        return $return;
    }

    private function next_weekly_date($year, $month, $day, $date_work)
    {
        $result         = payments_rule_weekly_date($date_work);
        if($day <= "07")
        {
            $result         = $result[0];
        }
        else if($day > "07" && $day <= "14")
        {
            $result         = $result[1];
        }
        else if($day > "14" && $day <= "21")
        {
            $result         = $result[2];
        }
        else if($day > "21" && $day <= "28")
        {
            $result         = $result[3];
        }
        else
        {
            $result         = $result[4];
        }

        $return         = $year."-".$month."-".$result;
        return $return;
    }
}