<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->title?> | PaydaySoft</title>
    <meta name="description" content="Free Bootstrap 4 Admin Theme | Pike Admin">
    <meta name="author" content="Pike Web Development - https://www.pikephp.com">
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/favicon.ico">
    <?php echo display_css_files("main");?>

    <?php $module = $this->router->fetch_class();?>
    <?php echo display_css_files($module);?>
    <!-- END CSS for this page -->
    <script type="text/javascript" src="<?php echo base_url('session/js_session');?>"></script>

</head>
<body class="adminbody">
<input type="hidden" id="module" value="<?php echo $module?>">
    <div id="main">
        <!-- top bar navigation -->
        <div class="headerbar">

            <!-- LOGO -->
            <div class="headerbar-left">
                <a href="<?php echo base_url();?>dashboard" class="logo"><img alt="Logo" src="<?php echo base_url()?>assets/images/logo.png" /> <span class="text-style-title">Prestame Dinero</span></a>
            </div>

            <nav class="navbar-custom">
                <ul class="list-inline float-right mb-0">
                    <li class="list-inline-item">
                        <a class="nav-link arrow-none" href="<?php echo base_url()?>supports" target="_blank">
                            <i class="fa fa-fw fa-question-circle"></i>
                        </a>
                    </li>

                    <li class="list-inline-item dropdown notif">
                        <a class="nav-link dropdown-toggle arrow-none" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                            <i class="fa fa-fw fa-calendar-check-o"></i><span class="notif-bullet" style="display: none"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-arrow dropdown-arrow-success dropdown-lg">
                            <!-- item-->
                            <div class="dropdown-item noti-title events">
                                <h5><small><span class="label label-danger pull-xs-right count-events"></span>Eventos de hoy</small></h5>
                            </div>
                        </div>
                    </li>

                    <li class="list-inline-item dropdown notif">
                        <?php $img = ($this->session->userdata('avatar') != null || $this->session->userdata('avatar') != "")? "assets/storage/avatars/".$this->session->userdata('avatar') : "assets/images/avatars/user.png"?>
                        <a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="javascript:void(0)" role="button" aria-haspopup="false" aria-expanded="false">
                            <img src="<?php echo base_url().$img?>" alt="Profile image" class="avatar-rounded">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right profile-dropdown ">
                            <!-- item-->
                            <div class="dropdown-item noti-title">
                                <h5 class="text-overflow" title="<?php echo $this->session->userdata('full_name');?>">
                                    <small>Hola, <?php echo $this->session->userdata('full_name');?></small>
                                </h5>
                            </div>
                            <!-- item-->
                            <a href="javascript:void(0)" class="dropdown-item notify-item modal_trigger" data-url="<?php echo base_url()?>employees/employee_preview" data-target="#preview-users" data-toggle="modal">
                                <i class="fa fa-fw fa-user-circle-o"></i> <span>Perfil</span>
                            </a>

                            <a href="<?php echo base_url()?>company" class="dropdown-item notify-item">
                                <i class="fa fa-cogs bigfonts"></i> <span>Configuraci&oacute;n</span>
                            </a>

                            <!-- item-->
                            <a href="<?php echo base_url()?>logout" class="dropdown-item notify-item">
                                <i class="fa fa-sign-out"></i> <span>Cerrar Sesi&oacute;n</span>
                            </a>
                        </div>
                    </li>
                </ul>
                <ul class="list-inline menu-left mb-0">
                    <li class="float-left">
                        <button class="button-menu-mobile open-left">
                            <i class="fa fa-fw fa-bars"></i>
                        </button>
                    </li>
                </ul>
            </nav>
        </div>
        <!-- End Navigation -->