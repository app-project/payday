<!-- Left Sidebar -->
<div class="left main-sidebar">
    <div class="sidebar-inner leftscroll">
        <div id="sidebar-menu">
            <ul>
                <li class="submenu">
                    <a class="dashboard" href="<?php echo base_url();?>dashboard"><i class="fa fa-fw fa-bars"></i><span> Dashboard </span> </a>
                </li>

                <li class="submenu">
                    <a class="clients" href="<?php echo base_url();?>clients"><i class="fa fa-fw fa-users"></i><span> Clientes </span> </a>
                </li>
                <?php if($this->session->userdata('typeId') == 1):?>
                <li class="submenu">
                    <a class="employees" href="<?php echo base_url();?>employees"><i class="fa fa-fw fa-user-circle-o"></i><span> Empleados </span> </a>
                </li>
                <?php endif;?>
                <li class="submenu">
                    <a class="calendar" href="<?php echo base_url();?>calendar"><i class="fa fa-fw fa-calendar"></i><span> Calendario </span> </a>
                </li>
                <li class="submenu">
                    <a class="loan_analysis" href="<?php echo base_url();?>loan_analysis"><i class="fa fa-fw fa-spinner"></i><span> Analisis de Prestamos </span> </a>
                </li>
                <li class="submenu">
                    <a class="compulsive_payment" href="<?php echo base_url();?>compulsive_payments"><i class="fa fa-fw fa-user-secret"></i><span> Cobros Compulsivos </span> </a>
                </li>
                <li class="submenu">
                    <a href="javascript:void(0)"><i class="fa fa-file-o"></i> <span> Reportes </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="tables-basic.html">Basic Tables</a></li>
                        <li><a href="tables-datatable.html">Data Tables</a></li>
                    </ul>
                </li>
                <li class="submenu">
                    <a href="javascript:void(0)"><i class="fa fa-gears"></i> <span> Configuraci&oacute;n </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url();?>company">Empresa</a> </li>
                        <li><a href="<?php echo base_url();?>institution_banks">Instituciones Bancarias</a> </li>
                    </ul>
                </li>
                <li class="submenu">
                    <a href="<?php echo base_url();?>logout"><i class="fa fa-fw fa-sign-out"></i><span> Cerrar Sesi&oacute;n </span> </a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- End Sidebar -->
