    <footer class="footer">
        <span class="text-right"></span>
        <span class="float-right">
            Powered by <a href="#"><b>Software Solutions E & F</b></a>
        </span>
    </footer>
</div>

    <div class="modal fade custom-modal" tabindex="-1" role="dialog" aria-labelledby="preview-users" aria-hidden="true" id="preview-users">
        <div class="modal-dialog"></div>
    </div>
<!-- END main -->
<?php echo display_js_files("main");?>

<!-- END Java Script for this page -->
<?php $module = $this->router->fetch_class();?>
<?php echo display_js_files($module);?>
</body>
</html>