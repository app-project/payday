<?php

    $this->load->view('include/header/header.php');
    $this->load->view('include/navigation/navigation.php');
    $this->load->view($content);
    $this->load->view('include/footer/footer.php');