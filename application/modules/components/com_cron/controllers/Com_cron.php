<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Com_cron extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('compulsive_payments/compulsive_payments_model');
        $this->load->model('company/settings_model');
    }

    public function send_compulsive_payments_mails()
    {
        $data_result = $this->compulsive_payments_model->get_load_analysis_data();
        $result = array();

        foreach ($data_result AS $row)
        {
            $next_payment_day = next_payment_day($row->payment_dayId, $row->day_work);

            if($next_payment_day[0] == 2)
            {
                $result = array($row);
            }
        }

        $this->send_mails($result);
    }

    public function send_mails($array)
    {
        if(count($array) > 0)
        {
            $data['data_settings']  = $this->settings_model->get_by(array('hidden' => 0, 'companyId' => 1), TRUE);
            $data["result"] = $array;
            $actual_date = date("d-m-Y");
            $data['date'] =  date("Y-m-d", strtotime($actual_date."+ 1 days"));
            $html = $this->load->view("com_cron/compulsive_payments_mail_view", $data, TRUE);
            $this->sendmaild->send_mail(array(
                'to'       => 'edelacruz9713@gmail.com',
                'to_name'  => 'Jesus Enmanuel',
                'title'    => 'Proximos clientes para cobrar',
                'body'     => $html,
                'alt_body' => 'coreo enviado ssss',
            ), TRUE);
        }
    }
}
