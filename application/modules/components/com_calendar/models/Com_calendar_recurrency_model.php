<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Com_calendar_recurrency_model extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name       = 'ai_calendar_recurrency';
        $this->primary_key      = 'frequencyId';
        $this->order_by         = 'frequencyId DESC';
    }

    function get_recurrencies($not_in = FALSE)
    {
        $this->db->from($this->table_name);
        if($not_in != FALSE)
            $this->db->where_not_in($this->primary_key, $not_in);
        return $this->db->get()->result();
    }
}