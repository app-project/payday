<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Com_reminder_type_model extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name       = 'ai_calendar_reminder_types';
        $this->primary_key      = 'typeId';
        $this->order_by         = 'typeId DESC';
    }
}