<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Com_calendar extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('com_calendar/com_calendar_model');
        $this->load->model('com_calendar/com_calendar_events_reminder_model');
        $this->load->model('com_calendar/com_calendar_recurrency_model');
        $this->load->model('com_calendar/com_reminder_type_model');
    }

    /**
     * add reminders from invoices and checks
     *
     * @access	public
     * @param	array,
     * @return	array
     */

    public function add_reminder($data)
    {
        $reminder = array(
            'userId'            => $this->session->userdata('userId'),
            'amount'            => $this->strip_commas($data['amount']),
            'date_start'        =>  $data['date'],
            'date_end'          => 0,
            'all_day'           => 1,
            'time_start'        => 0,
            'time_end'          => 0,
            'repeat_type'       => 0,
            'rrule'             => '',
            'creation_date'     => timestamp_to_date(gmt_to_local(now(), 'UTC', FALSE), "Y-m-d H:i:s"),
            'calendarId'        => $data['calendarId'],
            'description'       => $data['description'],
            'docId'             => $data['docId'],
            'color'             => $data['color'],
        );

        if($eventId = $this->com_calendar_model->save($reminder))
        {
            $reminders = array(
                'eventId'               => $eventId,
                'typeId'                => 1,
                'reminder_interval'     => '',
                'before_date'           => 0
            );

            $this->com_calendar_events_reminder_model->save($reminders);
        }
    }

    /* -------- CALENDAR EVENTS -------------------------------------------------------------------------------------------------------------------------------------- */
    /**
     * events(show all my events)
     *
     * @access	public
     * @param	output,array,date,date
     * @return	array or json
     */

    public function events($output = FALSE, $ids = FALSE, $start = FALSE, $end = FALSE, $is_showed = FALSE)
    {
        $feed       = array();
        $events     = $this->com_calendar_model->get_events($start,$end, $ids, $is_showed);

        foreach($events as $row)
        {
            if(!$row->rrule)
            {
                // If is one time event

                $feed[] = array(
                    'id'            => $row->eventId,
                    'title'         => $row->description,
                    'date'          => $row->dtstart,
                    'calendar'      => $row->calendar_name,
                    'amount'        => $row->amount,
                    'start'         => $row->dtstart,
                    'color'         => $row->color,
                    'is_showed'     => $row->is_showed,
                    'className'     => 'event-item',
                    'url'           => base_url('calendar/event/'.$row->eventId.'/'.$row->dtstart)
                );
            }
            else
            {
                // If is recurrent event
                if($row->repeat_type == 0)
                {
                    $date = new DateTime($end);
                    $row->rrule .= ';until='.$date->format('Ymd');
                }

                $rrule = new RRule\RRule($row->rrule);
                foreach($rrule as $occurrence)
                {

                    $row->date  = $occurrence->format('Y-m-d');
                    $feed[]     = array(
                        'id'            => $row->eventId,
                        'title'         => $row->description,
                        'date'          => $row->date,
                        'calendar'      => $row->calendar_name,
                        'amount'        => $row->amount,
                        'start'         => $row->date,
                        'color'         => $row->color,
                        'is_showed'     => $row->is_showed,
                        'className'     => 'event-item',
                        'url'           => base_url('calendar/event/'.$row->eventId.'/'.$row->date)
                    );
                }
            }
        }

        switch($output)
        {
            case "json":
                echo json_encode($feed);
                break;

            case "array":
                return $feed;
                break;

            default:
                echo json_encode($feed);
        }
    }

    /**
     * frecuencies(return all the frecuencies availables)
     *
     * @access	public
     * @param	array
     * @return	array
     */

    public function frecuencies($not_in = FALSE)
    {
        $options        = array();
        $recurrencies   = $this->com_calendar_recurrency_model->get_recurrencies($not_in);

        $options['Recurrencia'][0]['name'] = ($not_in == FALSE)? "Solo esta vez" : "Seleccione una opción";

        foreach($recurrencies AS $row)
        {
            $options['Recurrencia'][$row->rrule]['name']            = $row->name;
            $options['Recurrencia'][$row->rrule]['data']['text']    = $row->text;
        }

        return $options;
    }

    /**
     * intervals(return all the intervals availables)
     *
     * @access	public
     * @param
     * @return	array
     */

    public function intervals()
    {
        $options        = array();
        $recurrencies   = $this->com_calendar_recurrency_model->get_recurrencies();

        foreach($recurrencies AS $row)
        {
            if(in_array($row->period_type, array('WEEK','DAY')))
            {
                $options[$row->period_type] = $row->text;
            }
        }

        return $options;
    }

    /**
     * reminder types(return all the reminder types availables)
     *
     * @access	public
     * @param
     * @return	array
     */

    public function reminder_types()
    {
        $reminder_types = $this->com_reminder_type_model->get();

        $options = array();
        foreach($reminder_types AS $row)
        {
            $options['Tipos'][$row->typeId]['name'] = $row->name;
            $options['Tipos'][$row->typeId]['data']['fonticon']  = $row->icon;
        }
        return $options;
    }

    // Calendar and ICal RRULE generation methods ------------------------
    /**
     * rrule composer (composer a special rule that we use at calendar)
     *
     * @access	public
     * @param   array
     * @return	array
     */

    public function rrule_composer($data)
    {
        if($data['repeat_type'] == 1)
        {
            $rules = array('freq','dtstart','interval','wkst','count','bymonth','byweekno','byyearday','bymonthday','byday','byhour','byminute','bysecond','bysetpos');
        }
        else if($data['repeat_type'] == 2)
        {
            $rules = array('freq','dtstart','interval','wkst','until','bymonth','byweekno','byyearday','bymonthday','byday','byhour','byminute','bysecond','bysetpos');
        }
        else
        {
            if($data['freq'] == 'WEEKLY')
            {
                $rules = array('freq','dtstart','interval','wkst','until','bymonth','byweekno','byyearday','bymonthday','byday','byhour','byminute','bysecond','bysetpos');
            }
            else
            {
                $rules = array('freq','dtstart','interval','wkst','until','bymonth','byweekno','byyearday','bymonthday','byday','byhour','byminute','bysecond','bysetpos');
            }
        }

        $rrule = 'RRULE:';

        foreach($data AS $key => $val)
        {
            if(in_array($key, $rules))
            {
                if($val)
                {
                    $val = (is_array($val))? implode(",", $val) : $val;
                    if(in_array($key, array('dtstart','until')))
                    {
                        $date = new DateTime($val);
                        $val = $date->format('Ymd');
                    }
                    $rrule .= $key.'='.$val.';';
                }
            }
        }

        return rtrim($rrule,";");
    }

    /**
     * rrule decomposer (decomposer a special rule that we use at calendar)
     *
     * @access	public
     * @param   array
     * @return	array
     */

    public function rrule_decomposer($rrule)
    {
        $array = array();
        $rules = explode(';', str_replace('RRULE:','',$rrule));

        foreach($rules AS $key => $val)
        {
            $rule = explode('=', $val);
            if(in_array($rule[0], array('dtstart','until')))
            {
                $date       = new DateTime($rule[1]);
                $rule[1]    = $date->format('Y-m-d');
            }
            $array[$rule[0]] = $rule[1];
        }

        return $array;
    }

    /**
     * rrule human readable
     *
     * @access	public
     * @param   array
     * @return	array
     */

    public function _rrule_human_readable($rule)
    {
        if($rule == '')
        {
            return "Solo una vez";
        }
        else
        {
            $rrule = new RRule\RRule($rule);
            return $rrule->humanReadable(['locale' => 'es', 'date_formatter' => function($date) { setlocale(LC_ALL, 'es_ES'); return strftime("el %a, %e %b %Y", strtotime($date->format('Y-m-d')));}]);
        }
    }
}