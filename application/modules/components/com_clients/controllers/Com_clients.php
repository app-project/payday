<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Com_clients extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        //Load Model
        $this->load->model('com_clients/com_clients_model');
    }

    /**
     * @return mixed
     */
    public function get_clients()
    {
        return $this->com_clients_model->get_clients_data();
    }

    public function get_clients_row($clientId)
    {
        return $this->com_clients_model->get_clients_row_data($clientId);
    }
}
