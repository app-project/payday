<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Com_clients_model extends MY_Model
{
    public function __construct()
    {
        $this->table_name   = "ai_clients";
        $this->view_name    = "ai_clients_view";
        $this->primary_key  = "clientId";
        $this->order_by     = "clientId";
    }

    /**
     * generate data of clients...
     */
    public function get_clients_data()
    {
        $query = $this->db->query("SELECT a.*, b.`name` AS payment_name, c.`name` AS living_name, d.`name` AS bank_name,
                                   g.`name` AS account_type
                                   FROM ai_clients AS a
	                               LEFT JOIN ai_payment_days AS b ON a.payment_dayId = b.payment_dayId 
	                               LEFT JOIN ai_living_places AS c ON a.living_placeId = c.living_placeId 
	                               LEFT JOIN ai_institution_banks AS d ON a.bankId = d.bankId 
	                               LEFT JOIN ai_clients_type_accounts AS g ON a.type_accountId = g.typeId 
                                   WHERE a.hidden = 0 GROUP BY a.clientId")->result();

        $options    = array();
        $options['Seleccione una Opción'][0] = 0;

        foreach ($query as $row)
        {
            $options['Seleccione una Opción'][$row->clientId]['name']                           = $row->first_name.' '.$row->last_name.' ('.$row->document.')';
            $options['Seleccione una Opción'][$row->clientId]['id']                             = $row->clientId;
            $options['Seleccione una Opción'][$row->clientId]['data']['bankId']                 = $row->bankId;
            $options['Seleccione una Opción'][$row->clientId]['data']['clientId']               = $row->clientId;
            $options['Seleccione una Opción'][$row->clientId]['data']['document']               = $row->document;
            $options['Seleccione una Opción'][$row->clientId]['data']['first_name']             = $row->first_name;
            $options['Seleccione una Opción'][$row->clientId]['data']['last_name']              = $row->last_name;
            $options['Seleccione una Opción'][$row->clientId]['data']['company']                = $row->company;
            $options['Seleccione una Opción'][$row->clientId]['data']['sector_company']         = $row->sector_company;
            $options['Seleccione una Opción'][$row->clientId]['data']['apartament_of_house']    = $row->apartament_of_house;
            $options['Seleccione una Opción'][$row->clientId]['data']['type_accountId']         = $row->type_accountId;
            $options['Seleccione una Opción'][$row->clientId]['data']['bank_account']           = $row->bank_account;
            $options['Seleccione una Opción'][$row->clientId]['data']['phone']                  = $row->phone;
            $options['Seleccione una Opción'][$row->clientId]['data']['mobile']                 = $row->mobile;
            $options['Seleccione una Opción'][$row->clientId]['data']['salary']                 = $row->salary;
            $options['Seleccione una Opción'][$row->clientId]['data']['post_working']           = $row->post_working;
            $options['Seleccione una Opción'][$row->clientId]['data']['department_working']     = $row->department_working;
            $options['Seleccione una Opción'][$row->clientId]['data']['user_bank']              = $row->user_bank;
            $options['Seleccione una Opción'][$row->clientId]['data']['password_bank']          = $row->password_bank;
            $options['Seleccione una Opción'][$row->clientId]['data']['type_accountId']         = $row->type_accountId;
            $options['Seleccione una Opción'][$row->clientId]['data']['payment_name']           = $row->payment_name;
            $options['Seleccione una Opción'][$row->clientId]['data']['image']                  = $row->image;
        }

        return $options;
    }

    public function get_clients_row_data($clientId)
    {
        return $this->db->query("SELECT a.*, b.`name` AS payment_name, c.`name` AS living_name, d.`name` AS bank_name,
                                   g.`name` AS account_type,
                                   h.name AS time_working
                                   FROM ai_clients AS a
	                               LEFT JOIN ai_payment_days AS b ON a.payment_dayId = b.payment_dayId 
	                               LEFT JOIN ai_living_places AS c ON a.living_placeId = c.living_placeId 
	                               LEFT JOIN ai_institution_banks AS d ON a.bankId = d.bankId 
	                               LEFT JOIN ai_clients_type_accounts AS g ON a.type_accountId = g.typeId 
	                               LEFT JOIN ai_time_working AS h ON a.time_workingId = h.time_workingId 
                                   WHERE a.hidden = 0 AND a.clientId = $clientId")->row();
    }
}