<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Com_auth extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        //load Model
        $this->load->model('com_auth/com_auth_model');
        $this->load->model('company/settings_model');
    }

    public function auth($email, $password, $encrypted)
    {
        $password = ($encrypted == FALSE) ? md5($password) : $password;
        $row      = $this->com_auth_model->auth($email, $password);
        $company  = $this->settings_model->get_columns('company,email,image,mobil,phone,politics', array('hidden' => 0, 'companyId' => 1), FALSE, TRUE);

        if($row)
        {
            $session = array(
                'is_logged_in'  => 1,
                'userId'        => $row->userId,
                'full_name'     => $row->first_name.' '.$row->last_name,
                'typeId'        => $row->typeId,
                'email'         => $row->email,
                'avatar'        => $row->image,
                'settings'      => $company,
            );

            $this->session->set_userdata($session);

            if(isset($_POST['remember_me']))
            {
                set_cookie('email', $row->email, time() + (86400)); /* 86400 = 1 day */
                set_cookie('password', $row->password, time() + (86400)); /* 86400 = 1 day */
                set_cookie('remember_me',1, time() + (86400)); /* 86400 = 1 day */
            }
            else
            {
                delete_cookie('email');
                delete_cookie('password');
                delete_cookie('remember_me');
            }

            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function js_session()
    {
        return 'var session = '.json_encode($this->session->userdata());
    }
}
