<?php if($result == 1 && count($result_data) > 0):?>
    <?php $i = 0; foreach($result_data AS $key => $row):?>
        <?php if(!empty($row[$i])):?>
            <tr>
                <td><?php echo $row[0];?> <input type="hidden" name="document[]"   value="<?php echo $row[0];?>"></td>
                <td><?php echo $row[1];?> <input type="hidden" name="first_name[]" value="<?php echo $row[1];?>"></td>
                <td><?php echo $row[2];?> <input type="hidden" name="last_name[]"  value="<?php echo $row[2];?>"></td>
                <td><?php echo $row[3];?> <input type="hidden" name="phone[]"      value="<?php echo $row[3];?>"></td>
                <td><?php echo $row[4];?> <input type="hidden" name="mobile[]"     value="<?php echo $row[4];?>"></td>
                <td><?php echo $row[5];?> <input type="hidden" name="email[]"      value="<?php echo $row[5];?>"></td>
                <td>
                    <?php echo (is_numeric($row[6]) && !empty($row[6]))? '$'.number_format($row[6],2) : 0.00;?>
                    <input type="hidden" name="requested_amount[]" value="<?php echo $row[6]; ?>">
                </td>
                <td>
                    <?php echo date_format( new DateTime($row[7]),'Y-m-d');?>
                    <input type="hidden" name="day_work[]" value="<?php echo $row[7];?>">
                </td>
                <td>
                    <?php echo (is_numeric($row[8]) && !empty($row[8]))? '$'.number_format($row[8],2) : 0.00;?>
                    <input type="hidden" name="salary[]" value="<?php echo $row[8];?>">
                </td>
            </tr>
        <?php endif;?>
    <?php endforeach;?>
    <input type="hidden" name="file_path" value="<?php echo $file_path;?>">
<?php endif;?>