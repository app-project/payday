<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Com_import extends MY_Controller
{
    protected $CI;
    protected $path;
    protected $url;
    private $upload_path;

    public function __construct()
    {
        parent::__construct();
        $this->CI=& get_instance();
        $this->CI->load->library('upload');
        $this->url  = "assets/storage/import";

        //Load Model
        $this->load->model('com_clients/com_clients_model');
    }

    public function import_upload()
    {
        $this->path = realpath(APPPATH . '../assets/storage/import/');

        if(!empty($_FILES['file']['name']))
        {
            $config = array(
                'allowed_types'         => 'xls|xlsx',
                'upload_path'           => $this->path,
                'overwrite'             => TRUE
            );

            $this->CI->upload->initialize($config);

            if($this->CI->upload->do_upload('file'))
            {
                $this->upload_path = $this->CI->upload->data('full_path');
                $response          = $this->import_table();
            }
            else
            {
                $response = array("result" => 0, "error" => $this->CI->upload->display_errors());
            }

            return $response;
        }
    }

    private function data_import()
    {
        $data_import = \yidas\phpSpreadsheet\Helper::newSpreadsheet($this->upload_path);
        $data        = $data_import->getRows();
        $response    = array();

        if(!empty($data[1]))
        {
            for ($i= 1; $i<= count($data); $i++)
            {
                if(isset($data[$i]))
                {
                    $response['result_data'][]   = $data[$i];
                    $response['result']          = 1;
                }
            }
        }
        else
        {
            $response['result'] = 0;
        }

        $response['file_path'] = $this->upload_path;

        return $response;
    }

    public function import_table()
    {
        $data = $this->data_import();
        return $this->load->view('com_import/com_import_table_view', $data, TRUE);
    }

    public function import_save($type_import)
    {
        if($type_import == 1)
        {
            $result['result']  = 0;
            $result['success'] = 0;
            $result['warinng'] = 0;
            $success           = 1;
            $warinng           = 1;

            if(isset($_POST['document']))
            {
                foreach($_POST['document'] AS $key => $row)
                {
                    $data = array(
                        'document'              => $_POST['document'][$key],
                        'first_name'            => $_POST['first_name'][$key],
                        'last_name'             => $_POST['last_name'][$key],
                        'phone'                 => $_POST['phone'][$key],
                        'mobile'                => $_POST['mobile'][$key],
                        'email'                 => $_POST['email'][$key],
                        'requested_amount'      => $this->strip_commas_2($_POST['requested_amount'][$key]),
                        'day_work'              => $this->format_date_($_POST['day_work'][$key]),
                        'salary'                => $this->strip_commas_2($_POST['salary'][$key]),
                    );


                    if($this->client_check_cedula($_POST) == TRUE)
                    {
                        $result['result']  = ($this->com_clients_model->save($data))? 1 : 0;
                        $result['success'] = $success++;
                    }
                    else
                    {
                        $result['warinng'] = $warinng++;
                    }
                }

                if(isset($_POST['file_path']))
                {
                    $this->unlink_doc($_POST['file_path']);
                }
            }

            return $result;
        }
    }

    public function unlink_doc($folder)
    {
        if(file_exists($folder))
        {
            unlink($folder);
        }
    }

    private function client_check_cedula($data)
    {
        foreach($data['document'] AS $key => $row)
        {
            $where = array(
                'LOWER(REPLACE(document," ",""))=' => clear_space($data['document'][$key]),
                'hidden'                           => 0,
            );

            return ($this->com_clients_model->in_table_by($where) > 0)? FALSE : TRUE;
        }
    }

    public function ouput_import()
    {
        $objSpreadsheet = \yidas\phpSpreadsheet\Helper::newSpreadsheet();
        $objSpreadsheet->addRow(array('cedula', 'nombre', 'apellido', 'telefono', 'celular', 'correo', 'monto a solicitar', 'f_prox_cobro', 'salario'));

        $data           = $this->com_clients_model->get_by(array('hidden' => 0));
        foreach ($data AS $key => $row)
        {
            $objSpreadsheet->addRows(array(array($row->document,
                                                 $row->first_name,
                                                 $row->last_name,
                                                 $row->phone,
                                                 $row->mobile,
                                                 $row->email,
                                                 number_format($row->requested_amount,2),
                                                 $row->day_work,
                                                 number_format($row->salary,2))));
        }

        $document = 'Listado_de_clientes_'.date('Y-m-d');
        $objSpreadsheet->output($document);
    }
}