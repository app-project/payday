<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->title       = 'Dashboard';
        $this->securities->is_logged_in($this->session->userdata('is_logged_in'));

        //load Model
        $this->load->model('dashboard/dashboard_model');
        $this->load->model('compulsive_payments/compulsive_payments_model');
        $this->load->model('employees/employees_model');
        $this->load->model('loan_analysis/loan_analysis_model');
        $this->load->model('clients/clients_model');


//        print_d($this->sendmaild->send_mail(array('to'        => 'edelacruz9713@gmail.com',
//                                                  'to_name'  => 'franklin paulino ',
//                                                  'title'    => 'coreo enviado',
//                                                  'body'     => 'coreo enviado correctamente',
//                                                  'alt_body' => 'coreo enviado ssss',
//                                                 ), FALSE));
    }

    public function index()
    {
        $data                        = array();
        $data['clients']             = $this->clients_model->count_by(array('hidden' => 0));
        $data['compulsive_payments'] = $this->compulsive_payments_model->compulsive_count();
        $data['employees']           = $this->employees_model->count_by(array('hidden' => 0));
        $data['loan_analysis']       = $this->loan_analysis_model->count_by(array('hidden' => 0));
        $data['content']             = 'dashboard/dashboard_view';
        $this->load->view('include/template', $data);
    }
}
