<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Restablecer Contraseña | PaydaySoft</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/favicon.ico">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/css/util.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/css/main.css">
    <link href="<?php echo base_url()?>assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css" />
    <!--===============================================================================================-->
</head>
<body>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100 p-l-50 p-r-50 p-t-40 p-b-30">
            <form class="login100-form" id="reset-form" action="<?php echo base_url()?>session/update_password" method="post">
                <input type="hidden" name="userId" value="<?php echo $userId?>">
                <span class="login100-form-title p-b-55 remever-title">Restablecer Contraseña</span>
                <div class="response col-md-12 alpha omega"></div>
                <span class="description">Ha solicitado restablecer su contraseña. Digite y confirme su nueva contraseña.</span>
                <div class="wrap-input100 m-b-16">
                    <input class="input100" id="password" type="password" name="password" placeholder="Nueva Contraseña">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100"><span class="lnr lnr-lock"></span></span>
                </div>
                <div class="wrap-input100 m-b-16">
                    <input class="input100" id="new_password" type="password" name="new_password" placeholder="Reescriba Nueva Contraseña">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100"><span class="lnr lnr-lock"></span></span>
                </div>
                <div class="text-center w-full">
                    <span class="password-error"></span>
                </div>
                <div class="container-login100-form-btn p-t-25">
                    <button type="submit" id="reset-button" class="login100-form-btn ladda-button" data-style="zoom-in" disabled>
                        <span class="ladda-label">Restablecer</span>
                        <span class="ladda-spinner"></span>
                    </button>
                </div>
                <div class="text-center w-full p-t-25">
                    <a class="txt1 bo1 hov1" href="<?php echo base_url()?>login">Iniciar Sesión</a>
                </div>
            </form>
        </div>
    </div>
</div>

<!--===============================================================================================-->
<script src="<?php echo base_url()?>assets/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url()?>assets/login/vendor/bootstrap/js/popper.js"></script>
<script src="<?php echo base_url()?>assets/plugins/sweetalert/sweetalert.js"></script>
<script src="<?php echo base_url()?>assets/login/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/ladda/dist/spin.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/ladda/dist/ladda.min.js"></script>
<script src="<?php echo base_url()?>assets/js/proccess.js"></script>
<script src="<?php echo base_url()?>assets/js/dom.js"></script>

<script>
    $(document).ready(function(){
        Ladda.bind('.ladda-button');

        $(document).on("click", '#reset-button', function(e) {
            e.preventDefault();
            var data = {type: "post", url: $('#reset-form').attr('action'), form: "#reset-form", doAfter: "changePassword", messageError: ".response"};
            DOM.submit(data);
        });

        $(document).on('change', "#new_password", function () {
            var password        = $('#password').val(),
                new_password    = $('#new_password').val();

            if(password != new_password){
                $('.password-error').html('Las contraseñas no coinciden').addClass('p-t-10').css("color", "#F70219");
                $('#reset-button').prop("disabled", true);
            }else {
                $('.password-error').html('').removeClass('p-t-10').css("color", "#ffffff");
                $('#reset-button').prop("disabled", false);
            }
        });
    });
</script>
</body>
</html>