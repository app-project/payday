<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Link Inválido | PaydaySoft</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/favicon.ico">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/css/util.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/css/main.css">
    <!--===============================================================================================-->
</head>
<body>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100 p-l-50 p-r-50 p-t-40 p-b-30">
            <form class="login100-form">
                <span class="login100-form-title p-b-55 remever-title">Link Inválido</span>
                <div class="response col-md-12 alpha omega"></div>
                <span class="description">Este enlace es inválido o ha caducado su vigencia, si llegaste aqu&iacute; por equivocaci&oacute;n omite este mensaje.</span>
                <div class="text-center w-full p-t-5">
                    <a class="txt1 bo1 hov1" href="<?php echo base_url()?>login">Iniciar sesi&oacute;n</a>
                </div>
                <div class="text-center w-full p-t-10">
                    <a class="txt1 bo1 hov1" href="<?php echo base_url()?>session/recover">Recuperar mi contrase&ntilde;a</a>
                </div>
            </form>
        </div>
    </div>
</div>

<!--===============================================================================================-->
<script src="<?php echo base_url()?>assets/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url()?>assets/login/vendor/bootstrap/js/popper.js"></script>
<script src="<?php echo base_url()?>assets/login/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/ladda/dist/spin.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/ladda/dist/ladda.min.js"></script>
<script src="<?php echo base_url()?>assets/js/proccess.js"></script>
<script src="<?php echo base_url()?>assets/js/dom.js"></script>

</body>
</html>