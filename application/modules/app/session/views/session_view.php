<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->title?> | PaydaySoft</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/favicon.ico">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/css/util.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/css/main.css">
    <!--===============================================================================================-->
</head>
<body>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100 p-l-50 p-r-50 p-t-40 p-b-30 content-l">
            <form class="login100-form" id="login-form" action="<?php echo base_url()?>session/auth" method="post">
                <input type="hidden" name="redirect" id="redirect" value="<?php echo $redirect?>">
                <span class="login100-form-title p-b-25">Iniciar Sesión</span>
                <div class="response col-md-12 alpha omega"></div>
                <div class="wrap-input100 m-b-16">
                    <input class="input100" type="email" name="email" placeholder="Email" value="<?php echo (isset($_COOKIE['email']))? get_cookie('email') : ''; ?>">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100"><span class="lnr lnr-envelope"></span></span>
                </div>
                <div class="wrap-input100 m-b-16">
                    <input class="input100" type="password" name="password" placeholder="Contraseña" value="<?php echo (isset($_COOKIE['password']))? get_cookie('password') : ''; ?>">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100"><span class="lnr lnr-lock"></span></span>
                </div>
                <div class="contact100-form-checkbox m-l-4">
                    <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember_me" <?php echo (isset($_COOKIE['remember_me']))? 'checked' : ''; ?>><label class="label-checkbox100" for="ckb1">Mantenerme registrado</label>
                </div>
                <div class="container-login100-form-btn p-t-25">
                    <button type="submit" id="login-submit" class="login100-form-btn ladda-button" data-style="zoom-in">
                        <span class="ladda-label">Iniciar Sesión</span>
                        <span class="ladda-spinner"></span>
                    </button>
                </div>
                <div class="text-center w-full p-t-25">
                    <a class="txt1 bo1 hov1" href="<?php echo base_url()?>session/recover">¿Olvido su contraseña?</a>
                </div>
            </form>
        </div>
    </div>
</div>

<!--===============================================================================================-->
<script src="<?php echo base_url()?>assets/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url()?>assets/login/vendor/bootstrap/js/popper.js"></script>
<script src="<?php echo base_url()?>assets/login/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/ladda/dist/spin.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/ladda/dist/ladda.min.js"></script>
<script src="<?php echo base_url()?>assets/js/proccess.js"></script>
<script src="<?php echo base_url()?>assets/js/dom.js"></script>

<script>
    $(document).ready(function(){
        Ladda.bind('.ladda-button');

        $(document).on("click", '#login-submit', function(e) {
            e.preventDefault();
            var data = {type: "post", url: $('#login-form').attr('action'), form: "#login-form", doAfter: "redirect", messageError: ".response"};
            DOM.submit(data);
        });
    });
</script>
</body>
</html>