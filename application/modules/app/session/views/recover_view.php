<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->title?> | PaydaySoft</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo base_url()?>assets/images/favicon.ico">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/css/util.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/login/css/main.css">
    <!--===============================================================================================-->
</head>
<body>

<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100 p-l-50 p-r-50 p-t-40 p-b-30">
            <form class="login100-form" id="recover-form" action="<?php echo base_url()?>session/recover_password" method="post">
                <span class="login100-form-title p-b-55 remever-title">RECUPERAR CONTRASEÑA</span>
                <div class="response col-md-12 alpha omega"></div>
                <span class="description">Introduce tu Email y te enviaremos un enlace.</span>
                <div class="wrap-input100 m-b-16">
                    <input class="input100" type="email" id="email" name="email" placeholder="Email">
                    <span class="focus-input100"></span>
                    <span class="symbol-input100"><span class="lnr lnr-envelope"></span></span>
                </div>
                <div class="text-center w-full">
                    <span class="email-error"></span>
                </div>
                <div class="container-login100-form-btn p-t-25">
                    <button type="submit" id="recover-button" class="login100-form-btn ladda-button" data-style="zoom-in" disabled>
                        <span class="ladda-label">Recuperar</span>
                        <span class="ladda-spinner"></span>
                    </button>
                </div>
                <div class="text-center w-full p-t-25">
                    <a class="txt1 bo1 hov1" href="<?php echo base_url()?>login">Iniciar Sesión</a>
                </div>
            </form>
        </div>
    </div>
</div>

<!--===============================================================================================-->
<script src="<?php echo base_url()?>assets/login/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url()?>assets/login/vendor/bootstrap/js/popper.js"></script>
<script src="<?php echo base_url()?>assets/login/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/ladda/dist/spin.min.js"></script>
<script src="<?php echo base_url()?>assets/plugins/ladda/dist/ladda.min.js"></script>
<script src="<?php echo base_url()?>assets/js/proccess.js"></script>
<script src="<?php echo base_url()?>assets/js/dom.js"></script>

<script>
    $(document).ready(function(){
        Ladda.bind('.ladda-button');

        $(document).on('change', "#email", function () {
            var email = $("#email").val();

            if(validateEmail(email) == false){
                $('.email-error').html('Email no valido').addClass('p-t-10').css("color", "#F70219");
                $('#recover-button').prop("disabled", true);
            }else {
                $('.email-error').html('').removeClass('p-t-10').css("color", "#ffffff");
                $('#recover-button').prop("disabled", false);
            }
        });

        $(document).on("click", '#recover-button', function(e) {
            e.preventDefault();
            var data = {type: "post", url: $('#recover-form').attr('action'), form: "#recover-form", doAfter: "redirect", messageError: ".response"};
            DOM.submit(data);
        });

        var validateEmail = function (email){
            var caract = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);

            if (caract.test(email) == false){
                return false;
            }else{
                return true;
            }
        }
    });
</script>
</body>
</html>