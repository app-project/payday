<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Employees extends MY_Controller
{
    public $title;
    public $users_type;
    public $status;

    public function __construct()
    {
        parent::__construct();
        $this->title        = 'Empleados';

        $this->securities->is_logged_in($this->session->userdata('is_logged_in'));
        $this->securities->is_admin($this->session->userdata('typeId'));

        //Load Model
        $this->load->model('employees/employees_model');
        $this->load->model('employees/employees_status_model');

        //Load Module
        $this->load->module('com_files/controller/com_files');

        $this->users_type   = array('1' => "Administrador", '2' => "Empleado");
        $this->status       = $this->employees_status_model->get_assoc_list('statusId AS id, name', array('hidden' => 0));
    }

    public function index()
    {
        $data = array();
        $data['content'] = 'employees/employees_view';
        $this->load->view('include/template', $data);
    }

    public function datatable()
    {
        $columns    = "userId,first_name,last_name,user_type,email,image";
        $all_users  = $this->employees_model->datatable($columns, FALSE, TRUE);

        echo json_encode(array('data' => $all_users));
    }

    public function employee_preview()
    {
        $data        = array();
        $userId      = $this->session->userdata('userId');
        $data['row'] = $this->employees_model->get_by(array('userId' => $userId, 'hidden' => 0), TRUE);
        echo json_encode(array('result' => 1, 'view' => $this->load->view('employees/widgets/employees_preview_view', $data, TRUE)));
    }

    public function employee_view($userId)
    {
        $data        = array();
        $data['row'] = $this->employees_model->get_by(array('userId' => $userId, 'hidden' => 0), TRUE);
        echo json_encode(array('result' => 1, 'view' => $this->load->view('employees/employees_view_view', $data, TRUE)));
    }

    public function add()
    {
        $return = $this->load->view('employees/employees_new_view', array(), TRUE);
        echo json_encode(array('result' => 1, 'view' => $return));
    }

    public function edit($userId)
    {
        $data['employee']   = $this->employees_model->get_single($userId);
        $return             =  $this->load->view('employees/employees_edit_view', $data, TRUE);
        echo json_encode(array('result' => 1, 'view' => $return));
    }

    public function insert()
    {
        $this->form_validation->set_rules('first_name', '<strong>Nombre</strong>', 'trim|required');
        $this->form_validation->set_rules('last_name', '<strong>Apellido</strong>', 'trim|required');
        $this->form_validation->set_rules('email', '<strong>Email</strong>', 'trim|required|valid_email|callback_employee_email_check');
        $this->form_validation->set_rules('password', '<strong>Contraseña</strong>', 'required');

        if($this->form_validation->run($this) == FALSE)
        {
            echo json_encode(array('result' => 0, 'error' => display_error(validation_errors())));
        }
        else
        {
            $data = array(
                "first_name"    => $this->input->post("first_name"),
                "last_name"     => $this->input->post("last_name"),
                "typeId"        => $this->input->post("typeId"),
                "email"         => $this->input->post("email"),
                "statusId"      => $this->input->post("statusId"),
                "password"      => md5($this->input->post("password"))
            );

            if(!empty($_FILES))
            {
                $ext                      = substr(strrchr($_FILES['file']['name'], "."), 1);
                $data_file = array(
                    'file_name'           => '',
                    'file_type'           => $ext,
                    'allowed_types'       => 'gif|jpg|png|jpeg',
                    'folder'              => 'avatars'
                );

                $result            = $this->com_files->upload($data_file);

                if($result["result"] != 0)
                {
                    $data['image']  = $result['file'];
                }
            }

            $this->employees_model->save($data);
            echo json_encode(array('result' => 1));
        }
    }

    public function update($userId)
    {
        $row = $this->employees_model->get_by(array('userId' => $userId), TRUE);

        $this->form_validation->set_rules('first_name', '</strong>Nombre<strong>', 'trim|required');
        $this->form_validation->set_rules('last_name', '</strong>Apellido<strong>', 'trim|required');
        $this->form_validation->set_rules('email', '<strong>Email</strong>', 'trim|required|valid_email|callback_employee_email_check');

        if($this->form_validation->run($this ) == FALSE)
        {
            echo json_encode(array('result' => 0, 'error' => display_error(validation_errors())));
        }
        else
        {
            $data = array(
                "first_name"    => $this->input->post("first_name"),
                "last_name"     => $this->input->post("last_name"),
                "typeId"        => $this->input->post("typeId"),
                "statusId"      => $this->input->post("statusId"),
                "email"         => $this->input->post("email"),
            );

            if(!empty($_POST['password']))
            {
                $data['password']  = md5($this->input->post("password"));
            }

            if(!empty($_FILES))
            {
                $ext                      = substr(strrchr($_FILES['file']['name'], "."), 1);
                $data_file = array(
                    'file_name'           => '',
                    'file_type'           => $ext,
                    'allowed_types'       => 'gif|jpg|png|jpeg',
                    'folder'              => 'avatars'
                );

                $result            = $this->com_files->upload($data_file);

                if($result["result"] != 0)
                {
                    $data['image']  = $result['file'];
                    if($row->image != null || $row->image != "")
                    {
                        $this->com_files->unlink_image('avatars', $row->image);
                    }
                }
            }

            $this->employees_model->save($data, $userId);

            echo json_encode(array('result' => 1));
        }
    }

    public function update_perfil($userId)
    {
        $row = $this->employees_model->get_by(array('userId' => $userId), TRUE);

        $this->form_validation->set_rules('first_name', '</strong>Nombre<strong>', 'trim|required');
        $this->form_validation->set_rules('last_name', '</strong>Apellido<strong>', 'trim|required');
        $this->form_validation->set_rules('email', '<strong>Email</strong>', 'trim|required|valid_email|callback_employee_email_check');

        if($this->form_validation->run($this) == FALSE)
        {
            echo json_encode(array('result' => 0, 'error' => display_error(validation_errors())));
        }
        else
        {
            $data = array(
                "first_name"    => $this->input->post("first_name"),
                "last_name"     => $this->input->post("last_name"),
                "email"         => $this->input->post("email"),
            );

            if(!empty($_POST['password']))
            {
                $data['password']  = md5($this->input->post("password"));
            }

            if(!empty($_FILES))
            {
                $ext                      = substr(strrchr($_FILES['file']['name'], "."), 1);
                $data_file = array(
                    'file_name'           => '',
                    'file_type'           => $ext,
                    'allowed_types'       => 'gif|jpg|png|jpeg',
                    'folder'              => 'avatars'
                );

                $result            = $this->com_files->upload($data_file);

                if($result["result"] != 0)
                {
                    $data['image']  = $result['file'];
                    if($row->image != null || $row->image != "")
                    {
                        $this->com_files->unlink_image('avatars', $row->image);
                    }
                }

                $this->session->set_userdata(array('avatar' => $data['image']));
            }

            $this->employees_model->save($data, $userId);

            echo json_encode(array('result' => 1));
        }
    }

    public function delete($userId)
    {
        if($this->employees_model->delete($userId) === TRUE)
        {
            echo json_encode(array('result' => 1));
        }
        else
        {
            echo json_encode(array('result' => 0));
        }
    }

    public function employee_email_check()
    {
        $where = array(
            'LOWER(REPLACE(email," ",""))=' => clear_space($this->input->post('email')),
            'hidden'                        => 0,
            'userId !='                     => (isset($_POST['userId']) && $_POST['userId'] != 0)? $_POST['userId'] : 0
        );

        return ($this->employees_model->in_table_by($where) > 0)? FALSE : TRUE;
    }
}
