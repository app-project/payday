<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="breadcrumb-holder">
                        <h1 class="main-title float-left">Empleados</h1>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="<?php echo base_url()?>dashboard">Inicio</a></li>
                            <li class="breadcrumb-item active">Empleados</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card mb-3">
                        <div class="card-header">
                            <span class="pull-right"><a href="javascript:void(0)" class="btn btn-primary btn-sm modal_trigger" data-url="<?php echo base_url()?>employees/add" data-toggle="modal" data-target="#modal_user"><i class="fa fa-user-plus" aria-hidden="true"></i> Nuevo</a></span>
                        </div>
                        <!-- end card-header -->
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="list" class="table table-bordered table-hover display dataTable no-footer" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Detalles de Usuario</th>
                                            <th></th>
                                            <th style="width:130px">Tipo de Usuario</th>
                                            <th></th>
                                            <th style="width:120px">Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end card-body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- END container-fluid -->
    </div>
    <!-- END content -->
</div>
<!-- END content-page -->

<div class="modal fade custom-modal" tabindex="-1" role="dialog" aria-labelledby="modal_user" aria-hidden="true" id="modal_user">
    <div class="modal-dialog"></div>
</div>

<div class="modal fade custom-modal" tabindex="-1" role="dialog" aria-labelledby="modal_user" aria-hidden="true" id="preview_users_empployes">
    <div class="modal-dialog"></div>
</div>