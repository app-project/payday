<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-fw fa-user-circle-o"></i>Vista Usuario / <?php echo $row->first_name.' '.$row->last_name;?> </h5>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-8 solid-black">
                <div class="row form-group">
                    <div class="col-md-4 text-right"><label for="first_name" class="mg-t-5">Nombre:</label></div>
                    <div class="col-md-7"><label for="" class="l-h-30"><?php echo $row->first_name;?></label></div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4 text-right"><label for="last_name" class="mg-t-5">Apellido:</label></div>
                    <div class="col-md-7"><label for="" class="l-h-30"><?php echo $row->last_name;?></label></div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4 text-right"><label for="email" class="mg-t-5">Email:</label></div>
                    <div class="col-md-7"><label for="" class="l-h-30"><?php echo $row->email;?></label></div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4 text-right"><label for="typeId">Tipo de Usuario: </label></div>
                    <div class="col-md-2">
                        <?php $chk_users = ($row->typeId == 1)? 'Administrador': 'Empleado'?>
                        <label for="typeId" class="label" style="background-color: #0a6aa1;"><?php echo $chk_users;?></label>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-4 text-right"><label for="typeId">Estado: </label></div>
                    <div class="col-md-2">
                        <?php
                        $chk_users = '';
                        switch ($row->statusId)
                        {
                            case 1:
                                $chk_users = '<label class="label" style="background-color: #00CC00;">Activo</label>';
                                break;

                            case 2:
                                $chk_users = '<label class="label" style="background-color: #90111A;">Inactivo</label>';
                                break;

                            case 3:
                                $chk_users = '<label class="label" style="background-color: #82ad2b;">Bloqueado</label>';
                                break;
                        }

                        echo $chk_users;
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row form-group">
                    <div class="col-md-12">
                        <div class="card">
                            <?php $valid_img = (!empty($row->image))? $row->image: 'user.png'?>
                            <img src="<?php echo base_url()?>assets/storage/avatars/<?php echo $valid_img;?>" width="200px" height="150px" class="img-circle card-img" alt="Responsive image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>