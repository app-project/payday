<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-fw fa-user-circle-o"></i> Perfil de Usuario / <?php echo $row->first_name." ".$row->last_name?></h5>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    </div>
    <div class="modal-body">
        <form action="<?php echo base_url('employees/update_perfil/'.$row->userId)?>" id="form" method="post" enctype="multipart/form-data" role="form">
            <div class="modal-response"></div>
             <div class="row">
            <div class="col-md-8 solid-black">
                <input type="hidden" name="userId" value="<?php echo $row->userId;?>">
                <div class="row form-group">
                    <div class="col-md-3 text-right"><label for="first_name" class="mg-t-5">Nombre:</label></div>
                    <div class="col-md-7"><input type="text" id="first_name" class="form-control" name="first_name" value="<?php echo $row->first_name;?>"/></div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3 text-right"><label for="last_name" class="mg-t-5">Apellido:</label></div>
                    <div class="col-md-7"><input id="last_name" class="form-control" name="last_name" type="text" value="<?php echo $row->last_name; ?>"/></div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3 text-right"><label for="email" class="mg-t-5">Email:</label></div>
                    <div class="col-md-7"><input id="email" class="form-control" name="email" type="email" readonly value="<?php echo $row->email;?>"/></div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3 text-right"><label for="password" class="mg-t-5">Contraseña:</label></div>
                    <div class="col-md-7"><input id="password" class="form-control" name="password" type="password" value=""/></div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3 text-right"><label for="typeId">Tipo de Usuario: </label></div>
                    <div class="col-md-2">
                        <?php $chk_users = ($row->typeId == 1)? 'Administrador': 'Empleado'?>
                        <label for="typeId" class="label" style="background-color: #0a6aa1;"><?php echo $chk_users;?></label>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-3 text-right"><label for="typeId">Estado: </label></div>
                    <div class="col-md-2">
                        <?php
                            $chk_users = '';
                            switch ($row->statusId):
                                case 1:
                                    $chk_users = '<label class="label" style="background-color: #00CC00;">Activo</label>';
                                    break;

                                case 2:
                                    $chk_users = '<label class="label" style="background-color: #90111A;">Inactivo</label>';
                                    break;

                                case 3:
                                    $chk_users = '<label class="label" style="background-color: #82ad2b;">Bloqueado</label>';
                                    break;
                            endswitch;

                            echo $chk_users;
                        ?>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row form-group">
                    <div class="col-md-9 mg-l-30">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <?php $image = ($row->image != "" || $row->image != null) ? "assets/storage/avatars/".$row->image : "assets/images/avatars/user.png"?>
                            <div class="fileinput-new img-thumbnail" style="width: 200px; height: 150px; text-align: center">
                                <img src="<?php echo base_url().$image?>"  alt="...">
                            </div>
                            <div class="fileinput-preview fileinput-exists img-thumbnail" style="max-width: 200px; max-height: 150px; text-align: center"></div>
                            <div class="file-buttons">
                                <span class="btn btn-outline-secondary btn-file"><span class="fileinput-new"><i class="fa fa-cloud-upload"></i></span><span class="fileinput-exists">Cambiar</span><input type="file" name="file"></span>
                                <a href="#" class="btn btn-outline-secondary fileinput-exists" data-dismiss="fileinput">Remover</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
    <div class="modal-footer">
        <button id="button-submit-perfil" type="submit" class="btn btn-primary ladda-button" data-style="expand-left">
            <span class="ladda-label">Guardar</span>
        </button>
    </div>
</div>

<script>
    $(document).ready(function(){

        Ladda.bind('.ladda-button');
        // $('#filer-bank').filer({
        //     limit: 1,
        //     maxSize: 3,
        //     extensions: ['jpg', 'jpeg', 'png', 'gif', 'psd'],
        //     changeInput: true,
        //     captions: {
        //         button: "Cargar Imagen",
        //         feedback: "Choose files To Upload",
        //         feedback2: "files were chosen",
        //         drop: "Drop file here to Upload",
        //         removeConfirmation: "Esta seguro que desea eliminar esta imagen?",
        //         errors: {
        //             filesLimit: "Solo se permiten subir archivos {{fi-limit}}.",
        //             filesType: "Solo se permite subir imágenes.",
        //             filesSize: "{{fi-name}} ¡Es demasiado largo! Por favor suba el archivo a {{fi-maxSize}} MB.",
        //             filesSizeAll: "¡Los archivos que has elegido son demasiado grandes! Por favor suba archivos hasta {{fi-maxSize}} MB."
        //         }
        //     }
        // });
    });
</script>