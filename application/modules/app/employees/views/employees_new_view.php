<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">Nuevo Empleado / <span class="title-input"></span> <span class="title-input-p"></span></h5>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    </div>

    <form action="<?php echo base_url('employees/insert')?>" id="form" method="post" enctype="multipart/form-data" role="form">
        <div class="modal-body">
            <div class="modal-response"></div>
             <div class="row">
                 <div class="col-md-8 solid-black">
                    <div class="row form-group">
                        <div class="col-md-3 text-right"><label for="first_name" class="mg-t-5">Nombre:</label></div>
                        <div class="col-md-7"><input type="text" id="first_name" class="form-control" name="first_name" value="<?php echo set_value('first_name')?>" data-field="first_name"/></div>
                        <div class="valid-message"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3 text-right"><label for="last_name" class="mg-t-5">Apellido:</label></div>
                        <div class="col-md-7"><input id="last_name" class="form-control" name="last_name" type="text" value="<?php echo set_value('last_name')?>" data-field="last_name"/></div>
                        <div class="valid-message"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3 text-right"><label for="email" class="mg-t-5">Email:</label></div>
                        <div class="col-md-7"><input id="email" class="form-control" name="email" type="email" value="<?php echo set_value('email')?>" data-field="email"/></div>
                        <div class="valid-message"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3 text-right"><label for="password" class="mg-t-5">Contraseña:</label></div>
                        <div class="col-md-7"><input id="password" class="form-control" name="password" type="password" value="<?php echo set_value('password')?>" data-field="password"/></div>
                        <div class="valid-message"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3 text-right"><label for="typeId">Tipo de Usuario: </label></div>
                        <div class="col-md-5">
                            <?php echo form_dropdown('typeId', $this->users_type, set_value('typeId', 2), "id='typeId' class='form-control chosen-select' placeholder='Seleccione una Opción'")?>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3 text-right"><label for="statusId">Estado: </label></div>
                        <div class="col-md-5">
                            <?php echo form_dropdown('statusId', $this->status, set_value('statusId', 1), "id='statusId' class='form-control chosen-select' placeholder='Seleccione una Opción'")?>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row form-group">
                        <div class="col-md-9">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new img-thumbnail" style="width: 200px; height: 150px; text-align: center">
                                    <img src="<?php echo base_url('assets/images/avatars/user.png')?>"  alt="...">
                                </div>
                                <div class="fileinput-preview fileinput-exists img-thumbnail" style="max-width: 200px; max-height: 150px;"></div>
                                <div class="file-buttons">
                                    <span class="btn btn-outline-secondary btn-file"><span class="fileinput-new"><i class="fa fa-cloud-upload"></i></span><span class="fileinput-exists">Cambiar</span><input type="file" name="file"></span>
                                    <a href="#" class="btn btn-outline-secondary fileinput-exists" data-dismiss="fileinput">Remover</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button id="button-submit-user" type="submit" class="btn btn-primary ladda-button" data-style="expand-left">
                <span class="ladda-label">Guardar</span>
            </button>
        </div>
    </form>
</div>

<script>
    $(document).ready(function(){
        Ladda.bind('.ladda-button');
        $('.chosen-select').chosen({dropdownParent: $('#modal_user'), width: '100%'});
    });
</script>