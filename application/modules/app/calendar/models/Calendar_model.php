<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calendar_model extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name       = 'ai_calendar_events';
        $this->view_name        = 'ai_calendar_events_view';
        $this->procedure_name   = 'calendar_events';
        $this->primary_key      = 'eventId';
        $this->order_by         = 'eventId DESC';
    }

    function get_event($eventId)
    {
        return $this->db->query("SELECT a.eventId AS eventId, a.userId AS userId, e.email AS user_email, CONCAT(e.first_name, ' ', e.last_name) as full_name, b.calendarId AS calendarId, a.amount AS amount, b.name AS calendar_name, b.color AS calendar_color,
                                IF( ( ( a.color = '' ) OR isnull( a.color ) ), b.color, a.color ) AS color,
	                          a.description AS description, a.date_start AS date_start,
                              IF(( isnull( a.date_end ) OR ( a.date_end = '' ) OR ( a.date_end = '0000-00-00' ) ), '*', a.date_end) AS date_end, a.date_start AS dtstart,
                              IF ( ( a.date_end = '0000-00-00' ), '', a.date_end ) AS until, a.all_day AS all_day, a.time_start AS time_start, a.time_end AS time_end, a.repeat_type AS repeat_type, a.rrule AS rrule, a.creation_date AS creation_date, a.hidden AS hidden
                              FROM ai_calendar_events AS a 
                              LEFT JOIN ai_calendars_type AS b ON b.calendarId = a.calendarId 
		                      LEFT JOIN ai_users AS e ON e.userId = a.userId
		                      WHERE a.eventId = $eventId
	                          GROUP BY a.eventId")->row();
    }
}