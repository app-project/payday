<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calendar_type_model extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name       = 'ai_calendars_type';
        $this->primary_key      = 'calendarId';
        $this->order_by         = 'calendarId DESC';
    }

    public function get_calendar_type()
    {
        return $this->db->query("SELECT * FROM ai_calendars_type WHERE hidden = 0")->result();
    }
}