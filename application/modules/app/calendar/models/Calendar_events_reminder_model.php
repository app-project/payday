<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calendar_events_reminder_model extends MY_Model {

    public function __construct()
    {
        parent::__construct();
        $this->table_name       = 'ai_calendar_events_reminder';
        $this->primary_key      = 'reminderId';
        $this->order_by         = 'reminderId DESC';
    }
}