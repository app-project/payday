<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Calendar extends MY_Controller
{
    public $title;
    public $reminder_types;
    public $frequency;
    public $intervals;
    public $repeat;
    public $weekdays;
    public $hours;
    public $calendar_types;

    public function __construct()
    {
        parent::__construct();
        $this->title       = 'Calendario';
        $this->securities->is_logged_in($this->session->userdata('is_logged_in'));

        //Load Model
        $this->load->model('calendar/calendar_model');
        $this->load->model('calendar/calendar_events_reminder_model');
        $this->load->model('calendar/calendar_type_model');
        $this->load->model('calendar/calendar_recurrency_model');

        //Load Module
        $this->load->module('com_calendar/controllers/com_calendar');

        $this->reminder_types   = $this->com_calendar->reminder_types();
        $this->frequency        = $this->com_calendar->frecuencies();
        $this->intervals        = $this->com_calendar->intervals();
        $this->repeat           = $this->_numbers();
        $this->weekdays         = _weekdays();
        $this->hours            = _hours();
        $this->repeat           = array(1 => 'Todos los') + $this->repeat;
        $this->calendar_types   = $this->calendar_type_model->get_assoc_list(array("calendarId AS id", "name"), array("hidden" => 0));
    }

    public function index()
    {
        $data = array();
        $data['calendar_menu']      = $this->calendar_type();
        $data['content']            = 'calendar/calendar_view';
        $this->load->view('include/template',$data);
    }

    public function add_type()
    {
        $return =  $this->load->view('calendar/calendar_add_type_view', array(), TRUE);
        echo json_encode(array('result' => 1, 'view' => $return));
    }

    public function edit_type($calendarId)
    {
        $data['row']    = $this->calendar_type_model->get_single($calendarId);
        $return         =  $this->load->view('calendar/calendar_edit_type_view', $data, TRUE);
        echo json_encode(array('result' => 1, 'view' => $return));
    }

    public function insert_type()
    {
        $this->form_validation->set_rules('color', '<strong>Color</strong>', 'trim|required');
        $this->form_validation->set_rules('name', '<strong>Nombre</strong>', 'trim|required');

        if($this->form_validation->run($this) == FALSE)
        {
            echo json_encode(array('result' => 0, 'error' => display_error(validation_errors())));
        }
        else
        {
            $data = array(
                'name'                            => $this->input->post('name'),
                'color'                           => $this->input->post('color'),
                'creation_date'                   => timestamp_to_date(gmt_to_local(now(), 'UTC', FALSE), "Y-m-d H:i:s"),
            );

            $this->calendar_type_model->save($data);

            echo json_encode(array('result' => 1, 'view' => $this->calendar_type()));
        }
    }

    public function update_type($calendarId)
    {
        $this->form_validation->set_rules('color', '<strong>Color</strong>', 'trim|required');
        $this->form_validation->set_rules('name', '<strong>Nombre</strong>', 'trim|required');

        if($this->form_validation->run($this) == FALSE)
        {
            echo json_encode(array('result' => 0, 'error' => display_error(validation_errors())));
        }
        else
        {
            $data = array(
                "color"    => $this->input->post("color"),
                "name"     => $this->input->post("name"),
            );

            $this->calendar_type_model->save($data, $calendarId);

            echo json_encode(array('result' => 1, 'view' => $this->calendar_type()));
        }
    }

    public function delete_type($calendarId)
    {
        if($this->calendar_type_model->delete($calendarId) === TRUE)
        {
            echo json_encode(array('result' => 1, 'view' => $this->calendar_type()));
        }
        else
        {
            echo json_encode(array('result' => 0));
        }
    }

    public function event($calendarId, $date)
    {
        $event                  = $this->calendar_model->get_event($calendarId);
        $event_data['hour']     = $this->_get_event_hours($event);
        $event_data['row']      = $event;
        $event_data['date']     = $date;

        $data = array(
            'id'                => $event->eventId,
            'description'       => $event->description,
            'date'              => $date,
            'calendar'          => $event->calendar_name,
            'color'             => $event->color,
            'amount'            => $event->amount,
            'content'           => $this->load->view('calendar/calendar_event_view', $event_data, true),
            'url'               => base_url('calendar/edit_event/'.$event->eventId)
        );

        echo json_encode($data);
    }

    public function events($output = FALSE, $ids = FALSE, $start = FALSE, $end = FALSE, $is_showed = FALSE)
    {
        if($ids == FALSE || $ids == 'FALSE')
        {
            $ids = objectToString($this->calendar_type_model->get_calendar_type(), "calendarId");
        }

        if($start == FALSE || $start == 'FALSE')
        {
            $start = ($this->input->post('start') !== '' || $this->input->post('start') !== NULL)? date('Y-m-d', strtotime($this->input->post('start'))) : date('Y-m').'-01';
        }

        if($end == FALSE || $end == 'FALSE')
        {
            $end = ($this->input->post('end') !== '' || $this->input->post('end') !== NULL)? date('Y-m-d', strtotime($this->input->post('end'))) : date('Y-m-t');
        }

        $this->com_calendar->events($output, $ids, $start, $end, $is_showed);
    }

    public function add_event($date = FALSE)
    {
        $data['date']   = ($date == FALSE) ? date("Y-m-d") : $date;
        $return         =  $this->load->view('calendar/calendar_add_event_view', $data, TRUE);
        echo json_encode(array('result' => 1, 'view' => $return));
    }

    public function edit_event($eventId)
    {
        $data['row']        = $this->calendar_model->get_single($eventId);
        $data['reminders']  = $this->calendar_events_reminder_model->get_by(array('eventId' => $eventId));
        $data['rrule']      = ($data['row']->rrule !='')? $this->com_calendar->rrule_decomposer($data['row']->rrule) : array('freq' => '', 'interval' => 0);
        $return             =  $this->load->view('calendar/calendar_edit_event_view', $data, TRUE);
        echo json_encode(array('result' => 1, 'view' => $return));
    }

    public function insert_event()
    {
        $this->form_validation->set_rules('description', '<strong>Titulo</strong>', 'trim|required');
        $this->form_validation->set_rules('calendarId', '<strong>Calendario</strong>', 'required|is_natural_no_zero');
        $this->form_validation->set_rules('dtstart', '<strong>Fecha</strong>', 'trim|required');

        if($this->input->post('freq') == 'WEEKLY')
        {
            $this->form_validation->set_rules('byday', 'día', 'trim|required');
        }

        if($this->form_validation->run($this) == FALSE)
        {
            echo json_encode(array('result' => 0, 'error' => display_error(validation_errors())));
        }
        else
        {
            $result = array('result' => 0);
            $rrule = ($_POST['freq'] != '0')? $this->com_calendar->rrule_composer($_POST) : '';

            $event = array(
                'userId'            => $this->userId,
                'calendarId'        => $this->input->post('calendarId'),
                'description'       => $this->input->post('description'),
                'amount'            => $this->strip_commas($this->input->post('amount')),
                'color'             => $this->input->post('color'),
                'date_start'        => $this->input->post('dtstart'),
                'date_end'          => $this->input->post('until'),
                'all_day'           => $this->input->post('all_day'),
                'time_start'        => $this->input->post('time_start'),
                'time_end'          => $this->input->post('time_end'),
                'repeat_type'       => $this->input->post('repeat_type'),
                'rrule'             => $rrule,
                'creation_date'     => timestamp_to_date(gmt_to_local(now(), 'UTC', FALSE), "Y-m-d H:i:s"),
            );

            if($eventId = $this->calendar_model->save($event))
            {
                if($this->input->post('reminder_type'))
                {
                    foreach($this->input->post('reminder_type') AS $key => $value)
                    {
                        $reminders = array(
                            'eventId'               => $eventId,
                            'typeId'                => $value,
                            'reminder_interval'     => $_POST['reminder_interval'][$key],
                            'before_date'           => $_POST['before_date'][$key]
                        );

                        $this->calendar_events_reminder_model->save($reminders);
                    }
                }

                $result = array('result' => 1);
            }
            echo json_encode($result);
        }
    }

    public function update_event($eventId)
    {
        $this->form_validation->set_rules('description', '<strong>Titulo</strong>', 'trim|required');
        $this->form_validation->set_rules('calendarId', '<strong>Calendario</strong>', 'required|is_natural_no_zero');
        $this->form_validation->set_rules('dtstart', '<strong>Fecha</strong>', 'trim|required');

        if ($this->input->post('freq') == 'WEEKLY')
        {
            $this->form_validation->set_rules('byday', 'día', 'trim|required');
        }

        if ($this->form_validation->run($this) == FALSE)
        {
            echo json_encode(array('result' => 0, 'error' => display_error(validation_errors())));
        }
        else
        {
            $result = array('result' => 0);
            $rrule = ($_POST['freq'] != '0') ? $this->com_calendar->rrule_composer($_POST) : '';

            $event = array(
                'userId'        => $this->userId,
                'calendarId'    => $this->input->post('calendarId'),
                'description'   => $this->input->post('description'),
                'amount'        => $this->strip_commas($this->input->post('amount')),
                'date_start'    => $this->input->post('dtstart'),
                'date_end'      => $this->input->post('until'),
                'all_day'       => $this->input->post('all_day'),
                'time_start'    => $this->input->post('time_start'),
                'time_end'      => $this->input->post('time_end'),
                'repeat_type'   => $this->input->post('repeat_type'),
                'rrule'         => $rrule,
                'creation_date' => timestamp_to_date(gmt_to_local(now(), 'UTC', FALSE), "Y-m-d H:i:s"),
            );

            if ($this->calendar_model->save($event, $eventId))
            {
                //Save event reminders
                if ($_POST['reminder_type'])
                {
                    foreach ($_POST['reminder_type'] AS $key => $value)
                    {
                        $reminderId = $_POST['reminderId'][$key];

                        $reminders = array(
                            'eventId'           => $eventId,
                            'typeId'            => $value,
                            'reminder_interval' => $_POST['reminder_interval'][$key],
                            'before_date'       => $_POST['before_date'][$key]
                        );

                        $saved = ($reminderId == 0) ? $this->calendar_events_reminder_model->save($reminders) : $this->calendar_events_reminder_model->save($reminders, $reminderId);
                    }
                }

                $result = array('result' => 1);
            }

            echo json_encode($result);
        }
    }

    public function hide_event($eventId)
    {
        $result = ($this->calendar_model->save(array('hidden' => 1), $eventId) == TRUE)? 1 : 0;

        echo json_encode(array('result' => $result));
    }

    public function calendar_type($output = FALSE)
    {
        $data['row']    = $this->calendar_type_model->get_calendar_type();
        $return         = $this->load->view('calendar/calendar_type_view', $data, TRUE);

        switch($output)
        {
            case "json":
                echo json_encode(array('result' => $return));
                break;
            default:
                return $return;
        }
    }

    public function _get_event_hours($event)
    {
        $time  = ($event->all_day != 1 && $event->time_start != '')? human_time_format($event->time_start) : '';
        $time .= ($event->all_day != 1 && $event->time_end != '')? ' - '. human_time_format($event->time_end) : '';

        return $time;
    }

    public function showed_event_alert($eventId)
    {
        $result = ($this->calendar_model->save(array('is_showed' => 1, "count" => 1), $eventId) == TRUE)? 1 : 0;

        echo json_encode(array('result' => $result));
    }
}
