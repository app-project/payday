<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="breadcrumb-holder">
                        <h1 class="main-title float-left">Calendario</h1>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="<?php echo base_url()?>dashboard">Inicio</a></li>
                            <li class="breadcrumb-item active">Calendario</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-xs-12 col-md-4 col-lg-4 col-xl-3">
                    <div class="card-box tilebox-one noradius" id="calendar-menu">
                        <?php echo $calendar_menu?>
                    </div>
                </div>

                <div class="col-xs-12 col-md-8 col-lg-8 col-xl-9">
                    <div class="card-box tilebox-one noradius">
                        <div id="calendar"></div>
                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>

            <form>
                <?php
                $date = date('Y-m-d');
                $NextDate = date('Y-m-d', strtotime($date . "+1 months") );
                ?>
                <input type="hidden" name="defaultDate" id="defaultDate" value="<?php echo date("Y-m-d");?>">
                <input type="hidden" name="NextDate" id="NextDate" value="<?php echo $NextDate;?>">
            </form>
            <!-- end row -->
        </div>
        <!-- END container-fluid -->
    </div>
    <!-- END content -->
</div>
<!-- END content-page -->

<div class="modal custom-modal fade" id="calendar-type" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
    </div>
</div>

<div class="modal custom-modal fade" id="calendar-event" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
    </div>
</div>