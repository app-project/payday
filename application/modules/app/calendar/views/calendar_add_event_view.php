<div class="modal-content">
    <div class="clear modal-header">
        <div class="alpha">
            <h5 class="modal-title">Nuevo Evento <span class="list-div">/</span> <span class="list-title-event"></h5>
        </div>
        <div class="omega">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        </div>
    </div>
    <div class="modal-body">
        <form action="<?php echo base_url()?>calendar/insert_event" id="form-event" method="post" enctype="multipart/form-data" role="form">
            <div class="modal-response"></div>
            <div class="form-body">
                <div class="row">
                    <div class="col-md-7 vertical-line">
                        <div class="row form-group">
                            <div class="col-md-3 text-right">
                                <label for="description" class="conf-label no-bottom">Título:</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control validate" name="description" id="description" value="<?php echo set_value('description')?>" data-field="description">
                            </div>
                            <div class="valid-message"></div>
                            <div class="col-md-12 text-center">
                            </div>
                        </div>
                        <div class="row form-group select">
                            <div class="col-md-3 text-right">
                                <label for="calendarId" class="conf-label no-bottom">Calendario:</label>
                            </div>
                            <div class="col-md-6">
                                <?php echo form_dropdown('calendarId', $this->calendar_types, set_value('calendarId'), "id='calendarId' class='form-control chosen-select validate' data-placeholder='Seleccione un calendario' data-field='calendarId'");?>
                            </div>
                            <div class="col-md-12 text-center">
                                <div class="valid-message"></div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-3 text-right">
                                <label for="amount" class="conf-label no-bottom">Monto:</label>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control number" name="amount" id="amount" value="<?php echo set_value('amount')?>" placeholder="Monto">
                            </div>
                            <div class="col-md-12 text-center">
                                <div class="valid-message"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="row form-group">
                            <div class="col-md-3 text-right">
                                <label for="dtstart" class="conf-label no-bottom">Fecha:</label>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" readonly="readonly" id="dtstart" name="dtstart" value="<?php echo $date;?>" data-validate="required">
                            </div>
                            <div class="col-md-4 alpha omega text-center">
                                <input name="all_day" id="all_day" type="checkbox" value="1" class="filled-in chk-col-light-blue" checked="checked"> <label for="all_day">Todo el dia</label>
                            </div>
                        </div>
                        <div class="row form-group p-t-1 hour hidden select">
                            <div class="col-md-3 text-right">
                                <label for="time_start" class="conf-label no-bottom">Desde:</label>
                            </div>
                            <div class="col-md-3 omega">
                                <?php echo form_dropdown('time_start', $this->hours, '10:00', "id='time_start' class='form-control chosen-select' data-placeholder=' '");?>
                            </div>
                            <div class="col-md-3 text-right">
                                <label for="time_end" class="conf-label no-bottom">Hasta:</label>
                            </div>
                            <div class="col-md-3 alpha">
                                <?php echo form_dropdown('time_end', $this->hours, '11:30',  "id='time_end' class='form-control chosen-select' data-placeholder=' '");?>
                            </div>
                        </div>
                        <div class="row form-group p-t-1 select">
                            <div class="col-md-3 text-right">
                                <label for="frequencyId" class="conf-label no-bottom">Repite:</label>
                            </div>
                            <div class="col-md-8">
                                <?php echo form_dropdown_data('freq', $this->frequency, set_value('freq'), "id='frequencyId' class='form-control chosen-select' data-placeholder=' '"); ?>
                            </div>
                        </div>
                        <div class="row form-group p-t-1 frecuency-data hidden select">
                            <div class="col-md-3 text-right">
                                <label for="freq" class="conf-label no-bottom">Cada:</label>
                            </div>
                            <div class="col-md-4">
                                <?php echo form_dropdown('interval', $this->repeat, set_value('interval'), "id='repeat_every' class='form-control chosen-select'"); ?>
                            </div>
                            <div class="col-md-2 alpha">
                                <label for="freq" class="period-label">Días</label>
                            </div>
                        </div>
                        <div class="row frecuency-week hidden">
                            <div class="col-md-12">
                                <hr>
                            </div>
                        </div>
                        <div class="row form-group frecuency-week hidden m-b-0">
                            <div class="col-md-3 text-right">
                                <label class="conf-label no-bottom">Repetir:</label>
                            </div>
                            <div class="col-md-9">
                                <?php $i = 0; foreach($this->weekdays AS $key => $value):?>
                                    <input type="checkbox" name="byday[]" class="weekday check" id="weekday_<?php echo $i?>" value="<?php echo $key?>">
                                    <label for="weekday_<?php echo $i?>" class="p-r-10 p-l-5"><?php echo $value?></label>
                                    <?php $i++; endforeach;?>
                            </div>
                        </div>
                        <div class="row frecuency-data hidden">
                            <div class="col-md-12">
                                <hr>
                            </div>
                        </div>
                        <div class="row form-group frecuency-data hidden">
                            <div class="col-md-3 text-right">
                                <label class="conf-label no-bottom">Concluye:</label>
                            </div>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-12 row">
                                        <div class="col-md-12 alpha">
                                            <input type="radio" name="repeat_type" class="event-end with-gap radio-col-blue" id="never" value="*" checked><label for="never" class="p-l-5">Indefinido</label>
                                        </div>
                                        <div class="col-md-12 alpha m-t-10">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <input type="radio" name="repeat_type" class="event-end end-repeat with-gap radio-col-blue" id="after" value="1"><label for="after" class="p-l-5">repetir</label>
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="number" class="repeat form-control" name="count" id="repeat" value="1"/>
                                                </div>
                                                <div class="col-md-4 alpha">
                                                    <label for="repeat"> &nbsp; veces</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 alpha m-t-10">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <input type="radio" name="repeat_type" class="event-end end-date with-gap radio-col-blue" id="on" value="2" />
                                                    <label for="on" class="p-l-5">el</label>
                                                </div>
                                                <div class="col-md-6 alpha">
                                                    <span class="repeat"><input type="text" class="form-control" name="until" id="until" readonly="readonly"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12"><hr></div>
                    <div class="col-md-12">
                        <div class="row select">
                            <div class="col-md-2 text-right">
                                <label for="name">Env&iacute;ame un:</label>
                            </div>
                            <div class="col-md-10" id="reminders">
                                <div class="reminder-item p-b-10 row">
                                    <div class="col-md-3 alpha"><?php echo form_dropdown_icon('reminder_type[]', $this->reminder_types, '', " class='form-control chosen-select' data-placeholder='Enviar un'");?></div>
                                    <div class="col-md-2 alpha"><input type="number" class="form-control numeric" name="before_date[]" id="before_date" value="1"></div>
                                    <div class="col-md-2 alpha"><?php echo form_dropdown('reminder_interval[]', $this->intervals, 'DAY', " class='form-control chosen-select' data-placeholder=''");?></div>
                                    <div class="col-md-2 alpha"><label for=""> antes</label></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button id="button-submit-event" type="submit" class="btn btn-primary ladda-button" data-style="expand-left">
            <span class="ladda-label">Guardar</span>
        </button>
    </div>
</div>

<script>
    $(document).ready(function(){
        Ladda.bind('.ladda-button');

        $('#dtstart, #until').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1901,
            maxYear: parseInt(moment().format('YYYY'),10),
            locale: {
                format: 'YYYY-MM-DD'
            }
        });

        $('.chosen-select').chosen({
            dropdownParent: $('#calendar-event'),
            width: '100%'
        });
    });
</script>
