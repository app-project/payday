<div class="row">
    <div class="col-md-10"></div>
    <div class="col-md-2 text-right">
        <a href="javascript:void(0);" class="popover-hide" data-id="<?php echo $row->eventId;?>"><i class="icon icon-Close-Window"></i></a>
    </div>
</div>
<div class="event-body scroll row m-l-10">
    <div class="col-md-4 alpha omega text-right"><strong>Fecha:</strong></div>
    <div class="col-md-8"><?php echo human_date_format($date)?> <?php echo $hour;?></div>
    <div class="col-md-4 alpha omega text-right p-t-5"><strong>Calendario:</strong></div>
    <div class="col-md-8 p-t-5"><?php echo $row->calendar_name;?></div>
    <div class="col-md-4 alpha omega text-right p-t-5"><strong>Monto:</strong></div>
    <div class="col-md-8 p-t-5">$<?php echo number_format($row->amount,2);?></div>
    <div class="col-md-4 alpha omega text-right p-t-5"><strong>Creado por:</strong></div>
    <div class="col-md-8 p-t-5"><?php echo $row->full_name;?></div>
    <div class="col-md-4 alpha omega text-right p-t-5"><strong>Descripción:</strong></div>
    <div class="col-md-8 p-t-5"><?php echo $row->description?></div>
</div>
<hr>
<div class="event-options row">
    <div class="col-md-4"><a href="javascript:void(0);" class="popover-hide" data-id="<?php echo $row->eventId;?>">Cerrar</a></div>
    <div class="col-md-8">
        <ul class="options">
            <li>|</li>
            <li><a href="javascript:void(0);" class="cal-edit-event atb" data-route="calendar/edit_event" data-url="<?php echo base_url('calendar/edit_event/'.$row->eventId)?>"><strong>Editar</strong></a></li>
            <li>|</li>
            <li><a href="javascript:void(0);" class="cal-del-event atb" data-route="calendar/hide_event" data-url="<?php echo base_url('calendar/hide_event/'.$row->eventId)?>">Borrar</a></li>
        </ul>
    </div>
    <div class="clearfix"></div>
</div>