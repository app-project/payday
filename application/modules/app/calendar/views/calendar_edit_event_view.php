<div class="modal-content">
    <div class="clear modal-header">
        <div class="alpha">
            <h5 class="modal-title">Editar Evento <span class="list-div">/</span> <span class="list-title-event"><?php echo $row->description?></span></h5>
        </div>
        <div class="omega">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        </div>
    </div>
    <div class="modal-body">
        <form action="<?php echo base_url("calendar/update_event/".$row->eventId)?>" id="form-event" method="post" enctype="multipart/form-data" role="form">
            <div class="modal-response"></div>
            <div class="form-body">
                <div class="row">
                    <div class="col-md-7 vertical-line">
                        <div class="row form-group">
                            <div class="col-md-3 text-right">
                                <label for="description" class="conf-label no-bottom">Título:</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control validate" name="description" id="description" value="<?php echo set_value('description', $row->description)?>" data-field="description">
                            </div>
                            <div class="valid-message"></div>
                            <div class="col-md-12 text-center">

                            </div>
                        </div>
                        <div class="row form-group select">
                            <div class="col-md-3 text-right">
                                <label for="calendarId" class="conf-label no-bottom">Calendario:</label>
                            </div>
                            <div class="col-md-6">
                                <?php echo form_dropdown('calendarId', $this->calendar_types, set_value('calendarId', $row->calendarId), "id='calendarId' class='form-control chosen-select validate' data-placeholder='Seleccione un calendario' data-field='calendarId'");?>
                            </div>
                            <div class="col-md-12 text-center">
                                <div class="valid-message"></div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-3 text-right">
                                <label for="amount" class="conf-label no-bottom">Monto:</label>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="form-control number" name="amount" id="amount" value="<?php echo set_value('amount', $row->amount)?>" placeholder="Monto">
                            </div>
                            <div class="col-md-12 text-center">
                                <div class="valid-message"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="row form-group">
                            <div class="col-md-3 text-right">
                                <label for="dtstart" class="conf-label no-bottom">Fecha:</label>
                            </div>
                            <div class="col-md-5">
                                <input type="text" class="form-control" readonly="readonly" id="dtstart" name="dtstart" value="<?php echo $row->date_start;?>" data-validate="required">
                            </div>
                            <div class="col-md-4 alpha omega text-center">
                                <?php $ckecked = ($row->all_day == 1)? 'checked' : '';?>
                                <input name="all_day" id="all_day" type="checkbox" value="1" class="filled-in chk-col-light-blue" <?php echo $ckecked;?>> <label for="all_day">Todo el dia</label>
                            </div>
                        </div>
                        <?php $hidden = ($row->all_day == 1)? 'hidden' : '';?>
                        <div class="row form-group p-t-1 hour <?php echo $hidden?> select">
                            <div class="col-md-3 text-right">
                                <label for="time_start" class="conf-label no-bottom">Desde:</label>
                            </div>
                            <div class="col-md-3 omega">
                                <?php echo form_dropdown('time_start', $this->hours, '10:00', "id='time_start' class='form-control chosen-select' data-placeholder=' '");?>
                            </div>
                            <div class="col-md-3 text-right">
                                <label for="time_end" class="conf-label no-bottom">Hasta:</label>
                            </div>
                            <div class="col-md-3 alpha">
                                <?php echo form_dropdown('time_end', $this->hours, '11:30',  "id='time_end' class='form-control chosen-select' data-placeholder=' '");?>
                            </div>
                        </div>
                        <div class="row form-group p-t-1 select">
                            <div class="col-md-3 text-right">
                                <label for="frequencyId" class="conf-label no-bottom">Repite:</label>
                            </div>
                            <div class="col-md-8">
                                <?php echo form_dropdown_data('freq', $this->frequency, $rrule['freq'], "id='frequencyId' class='form-control chosen-select' data-placeholder=' '"); ?>
                            </div>
                        </div>
                        <?php $frecuency = ($rrule['interval'] != '0')? '' : 'hidden';?>
                        <div class="row form-group p-t-1 frecuency-data <?php echo $frecuency;?> select">
                            <div class="col-md-3 text-right">
                                <label for="freq" class="conf-label no-bottom">Cada:</label>
                            </div>
                            <div class="col-md-4">
                                <?php echo form_dropdown('interval', $this->repeat, $rrule['interval'], "id='repeat_every' class='form-control chosen-select'"); ?>
                            </div>
                            <div class="col-md-2 alpha">
                                <label for="freq" class="period-label"><?php echo $this->frequency['Recurrencia'][$rrule['freq']]['data']['text'];?></label>
                            </div>
                        </div>
                        <?php $weekdays = ($rrule['freq'] == 'WEEKLY')? '' : 'hidden';?>
                        <div class="row frecuency-week <?php echo $weekdays;?>">
                            <div class="col-md-12">
                                <hr>
                            </div>
                        </div>
                        <div class="row form-group frecuency-week <?php echo $weekdays;?> m-b-0">
                            <div class="col-md-3 text-right">
                                <label class="conf-label no-bottom">Repetir:</label>
                            </div>
                            <div class="col-md-9">
                                <?php $i = 0; foreach($this->weekdays AS $key => $value):?>
                                    <?php $weekday = (isset($rrule['byday']))? explode(',', $rrule['byday']) : array();?>
                                    <?php $checked = (in_array($key, $weekday))? 'checked' : '';?>
                                    <input type="checkbox" name="byday[]" class="weekday check" id="weekday_<?php echo $i?>" value="<?php echo $key?>" <?php echo $checked;?>>
                                    <label for="weekday_<?php echo $i?>" class="p-r-10 p-l-5"><?php echo $value?></label>
                                <?php $i++; endforeach;?>
                            </div>
                        </div>
                        <div class="row frecuency-data <?php echo $frecuency;?>">
                            <div class="col-md-12">
                                <hr>
                            </div>
                        </div>
                        <div class="row form-group frecuency-data <?php echo $frecuency;?>">
                            <div class="col-md-3 text-right">
                                <label class="conf-label no-bottom">Concluye:</label>
                            </div>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-md-12 row">
                                        <div class="col-md-12 alpha">
                                            <?php $never = ($row->repeat_type == '0')? 'checked' : '';?>
                                            <input type="radio" name="repeat_type" class="event-end with-gap radio-col-blue" id="never" value="*" <?php echo $never;?>><label for="never" class="p-l-5">Indefinido</label>
                                        </div>
                                        <div class="col-md-12 alpha m-t-10">
                                            <div class="row">
                                                <?php $after = ($row->repeat_type == '1')? 'checked' : '';?>
                                                <?php $count = (isset($rrule['count']))? $rrule['count'] : '';?>
                                                <div class="col-md-4">
                                                    <input type="radio" name="repeat_type" class="event-end end-repeat with-gap radio-col-blue" id="after" value="1" <?php echo $after;?>><label for="after" class="p-l-5">repetir</label>
                                                </div>
                                                <div class="col-md-4">
                                                    <input type="number" class="repeat form-control" name="count" id="repeat" value="<?php echo $count;?>"/>
                                                </div>
                                                <div class="col-md-4 alpha">
                                                    <label for="repeat"> &nbsp; veces</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 alpha m-t-10">
                                            <div class="row">
                                                <?php $on = ($row->repeat_type == '2')? 'checked' : '';?>
                                                <?php $until = (isset($rrule['until']))? $rrule['until'] : '';?>
                                                <div class="col-md-3">
                                                    <input type="radio" name="repeat_type" class="event-end end-date with-gap radio-col-blue" id="on" value="2" <?php echo $on;?>/>
                                                    <label for="on" class="p-l-5">el</label>
                                                </div>
                                                <div class="col-md-6 alpha">
                                                    <span class="repeat"><input type="text" class="form-control" name="until" id="until" readonly="readonly" <?php echo $until;?> value="<?php echo (isset($rrule['until']))? $rrule['until'] : ''?>"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12"><hr></div>
                    <div class="col-md-12">
                        <div class="row select">
                            <div class="col-md-2 text-right">
                                <label for="name">Env&iacute;ame un:</label>
                            </div>
                            <div class="col-md-10" id="reminders">
                                <?php foreach($reminders AS $reminder):?>
                                <div class="reminder-item p-b-10 row">
                                    <input type="hidden" name="reminderId[]" value="<?php echo $reminder->reminderId?>">
                                    <div class="col-md-3 alpha"><?php echo form_dropdown_icon('reminder_type[]', $this->reminder_types, $reminder->typeId, " class='form-control chosen-select' data-placeholder='Enviar un'");?></div>
                                    <div class="col-md-2 alpha"><input type="number" class="form-control numeric" name="before_date[]" id="before_date" value="<?php echo $reminder->before_date;?>"></div>
                                    <div class="col-md-2 alpha"><?php echo form_dropdown('reminder_interval[]', $this->intervals, $reminder->reminder_interval, " class='form-control chosen-select' data-placeholder=''");?></div>
                                        <div class="col-md-2"><label for=""> antes</label></div>
                                </div>
                                <?php endforeach;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button id="button-submit-event" type="submit" class="btn btn-primary ladda-button" data-style="expand-left">
            <span class="ladda-label">Guardar</span>
        </button>
    </div>
</div>

<script>
    $(document).ready(function(){
        Ladda.bind('.ladda-button');

        $('#dtstart, #until').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            minYear: 1901,
            maxYear: parseInt(moment().format('YYYY'),10),
            locale: {
                format: 'YYYY-MM-DD'
            }
        });

        $('.chosen-select').chosen({
            dropdownParent: $('#calendar-event'),
            width: '100%'
        });
    });
</script>
