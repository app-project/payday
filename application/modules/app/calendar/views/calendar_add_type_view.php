<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">Nuevo Calendario</h5>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    </div>
    <div class="modal-body">
        <form action="<?php echo base_url()?>calendar/insert_type" id="form-type" method="post" enctype="multipart/form-data" role="form">
            <div class="modal-response"></div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="color">Color</label>
                        <input type="text" id="color" class="form-control" name="color" value="<?php echo set_value('color')?>" data-color="#4980b5" readonly data-field="color"/>
                        <div class="valid-message"></div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Nombre</label>
                        <input id="name" class="form-control" name="name" type="text" value="<?php echo set_value('name')?>" data-field="name"/>
                        <div class="valid-message"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button id="button-submit" type="submit" class="btn btn-primary ladda-button" data-style="expand-left">
            <span class="ladda-label">Guardar</span>
        </button>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('#color').colorpicker();
        Ladda.bind('.ladda-button');
    });
</script>