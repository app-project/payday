<div id='external-events'>
    <div class="row">
        <div class="col-md-8">
            <h4>Calendarios</h4>
        </div>
        <div class="col-md-4 text-right">
            <a href="#" class="modal_trigger btn btn-primary" data-url="<?php echo base_url()?>calendar/add_type" data-toggle="modal" data-target="#calendar-type"><i class="fa fa-plus"></i></a>
        </div>
    </div>
    <hr>
    <?php foreach($row AS $calendar):?>
    <div class='fc-event' style="background-color:<?php echo $calendar->color?>"><a href="" class="modal_trigger" data-url="<?php echo base_url()?>calendar/edit_type/<?php echo $calendar->calendarId?>" data-toggle="modal" data-target="#calendar-type"><?php echo $calendar->name?></a> <a href="#" data-route="calendar/delete_type" class="pull-right delete_calendar" data-id="<?php echo $calendar->calendarId;?>"><i class="fa fa-close"></i></a></div>
    <?php endforeach;?>
    <div class="clearfix"></div>
    <hr>
    <a class="btn btn-primary modal_trigger" data-url="<?php echo base_url()?>calendar/add_event" data-toggle="modal" data-target="#calendar-event"><i class="fa fa-plus"></i> Nuevo</a>
</div>
