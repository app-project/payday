<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_days_model extends MY_Model
{
    public function __construct()
    {
        $this->table_name   = "ai_payment_days";
        $this->view_name    = "ai_payment_days";
        $this->primary_key  = "payment_dayId";
        $this->order_by     = "payment_dayId";
    }
}