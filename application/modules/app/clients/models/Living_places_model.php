<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Living_places_model extends MY_Model
{
    public function __construct()
    {
        $this->table_name   = "ai_living_places";
        $this->view_name    = "ai_living_places";
        $this->primary_key  = "living_placeId";
        $this->order_by     = "living_placeId";
    }
}