<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Time_working_model extends MY_Model
{
    public function __construct()
    {
        $this->table_name   = "ai_time_working";
        $this->view_name    = "ai_time_working";
        $this->primary_key  = "time_workingId";
        $this->order_by     = "time_workingId";
    }
}