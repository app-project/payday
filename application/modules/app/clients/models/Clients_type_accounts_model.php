<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Clients_type_accounts_model extends MY_Model
{
    public function __construct()
    {
        $this->table_name   = "ai_clients_type_accounts";
        $this->view_name    = "ai_clients_type_accounts";
        $this->primary_key  = "typeId";
        $this->order_by     = "typeId";
    }
}