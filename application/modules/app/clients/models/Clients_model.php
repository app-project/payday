<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Clients_model extends MY_Model
{
    public function __construct()
    {
        $this->table_name   = "ai_clients";
        $this->view_name    = "ai_clients_view";
        $this->primary_key  = "clientId";
        $this->order_by     = "clientId";
    }
}