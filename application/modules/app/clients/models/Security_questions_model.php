<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Security_questions_model extends MY_Model
{
    public function __construct()
    {
        $this->table_name   = "ai_security_questions";
        $this->view_name    = "ai_security_questions";
        $this->primary_key  = "questionId";
        $this->order_by     = "questionId";
    }
}