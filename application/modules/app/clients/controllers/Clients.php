<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends MY_Controller
{
    public $living;
    public $color;
    public $levels;
    public $remains;
    public $time_working;
    public $columns;
    public $bank;
    public $type;
    public $payment_day;

    public function __construct()
    {
        parent::__construct();
        $this->title       = 'Clientes';
        $this->securities->is_logged_in($this->session->userdata('is_logged_in'));

        //Load Model
        $this->load->model('clients/clients_model');
        $this->load->model('clients/living_places_model');
        $this->load->model('clients/security_questions_model');
        $this->load->model('clients/time_working_model');
        $this->load->model('clients/living_places_model');
        $this->load->model('clients/clients_type_accounts_model');
        $this->load->model('clients/payment_days_model');
        $this->load->model('clients/docs_model');
        $this->load->model('institution_banks/institution_bank_model');

        //Load Module
        $this->load->module('com_files/controller/com_files');
        $this->load->module('com_clients/controller/com_clients');
        $this->load->module('com_import/controller/com_import');

        $this->valid_import  = array('valid' => 0, 'warinng' => 0, 'success' => 0);
        $this->living        = $this->living_places_model->get_assoc_list('living_placeId AS id, name', array('hidden' => 0));
        $this->type          = $this->clients_type_accounts_model->get_assoc_list('typeId AS id, CONCAT(code, "-", name) AS name', array('hidden' => 0));
        $this->payment_day   = $this->payment_days_model->get_assoc_list('payment_dayId AS id, name', array('hidden' => 0));
        $this->time_working  = $this->time_working_model->get_assoc_list('time_workingId AS id, name', array('hidden' => 0));
        $this->bank          = $this->institution_bank_model->get_assoc_list('bankId AS id, name', array('hidden' => 0));
        $this->columns       = "clientId,document,first_name,last_name,salary,bank_name,address,phone,image";
    }

    public function index()
    {
        $data = array();
        $data['content'] = 'clients/clients_view';
        $this->load->view('include/template', $data);
    }

    public function datatable()
    {
       $all_clients = $this->clients_model->datatable($this->columns, FALSE, TRUE);
        echo json_encode(array('data' => $all_clients));
    }

    public function add()
    {
        $data['content'] = 'clients/clients_new_view';
        $this->load->view('include/template', $data);
    }

    public function edit($clientId)
    {
        $data['row']                 = $this->clients_model->get_by(array('clientId' => $clientId), TRUE);
        $data['security_questions']  = $this->security_questions_model->get_by(array("clientId" => $clientId));
        $data['docs']                = $this->docs_model->get_by(array("clientId" => $clientId, "hidden" => 0));
        $data['content']             = 'clients/clients_edit_view';
        $this->load->view('include/template', $data);
    }

    public function preview($clientId)
    {
        $data['row']                 = $this->com_clients->get_clients_row($clientId);
        $data['docs']                = $this->docs_model->get_by(array("clientId" => $clientId, "hidden" => 0));
        $data['security_questions']  = $this->security_questions_model->get_by(array("clientId" => $clientId));
        echo json_encode(array('result' => 1, 'view'=> $this->load->view('clients/widgets/clients_preview_view', $data, TRUE)));
    }

    public function insert()
    {
        $this->form_validation->set_rules('document','<strong>Cédula</strong>','trim|required');
        $this->form_validation->set_rules('bankId','<strong>Banco</strong>','is_natural_no_zero');
        $this->form_validation->set_rules('payment_dayId','<strong>Dia de Cobro</strong>','is_natural_no_zero');
        $this->form_validation->set_rules('type_accountId','<strong>Tipo de Cuenta</strong>','is_natural_no_zero');
        $this->form_validation->set_rules('living_placeId','<strong>Vivienda</strong>','is_natural_no_zero');
        $this->form_validation->set_rules('time_workingId','<strong>Tiempo Laborando</strong>','is_natural_no_zero');

        if($this->form_validation->run($this) == FALSE)
        {
            echo json_encode(array('result' => 0, 'error' => display_error(validation_errors())));
        }
        else
        {
            $data = array(
                'document'                  => $this->input->post('document', TRUE),
                'first_name'                => $this->input->post('first_name', TRUE),
                'last_name'                 => $this->input->post('last_name', TRUE),
                'phone'                     => $this->input->post('phone', TRUE),
                'phone_work'                => $this->input->post('phone_work', TRUE),
                'ext_phone_work'            => $this->input->post('ext_phone_work', TRUE),
                'email'                     => $this->input->post('email', TRUE),
                'email_work'                => $this->input->post('email_work', TRUE),
                'name_facebook'             => $this->input->post('name_facebook', TRUE),
                'link_facebook'             => $this->input->post('link_facebook', TRUE),
                'bankId'                    => $this->input->post('bankId', TRUE),
                'user_bank'                 => $this->input->post('user_bank', TRUE),
                'password_bank'             => $this->input->post('password_bank', TRUE),
                'password_transactional'    => $this->input->post('password_transactional', TRUE),
                'living_placeId'            => $this->input->post('living_placeId', TRUE),
                'street'                    => $this->input->post('street', TRUE),
                'residency'                 => $this->input->post('residency', TRUE),
                'sector'                    => $this->input->post('sector', TRUE),
                'province'                  => $this->input->post('province', TRUE),
                'entering_by'               => $this->input->post('entering_by', TRUE),
                'between_the_street'        => $this->input->post('between_the_street', TRUE),
                'reference_point'           => $this->input->post('reference_point', TRUE),
                'living_color'              => $this->input->post('living_color', TRUE),
                'apartament_of_house'       => (isset($_POST['apartament_of_house']))? $this->input->post('apartament_of_house', TRUE) : 0,
                'levels_apartment'          => $this->input->post('levels_apartment', TRUE),
                'iron_color'                => $this->input->post('iron_color', TRUE),
                'awning_color'              => $this->input->post('awning_color', TRUE),
                'remains_apartment'         => $this->input->post('remains_apartment', TRUE),
                'from_hour'                 => $this->input->post('from_hour', TRUE),
                'to_hour'                   => $this->input->post('to_hour', TRUE),
                'day_work'                  => $this->input->post('day_work', TRUE),
                'company'                   => $this->input->post('company', TRUE),
                'sector_company'            => $this->input->post('sector_company', TRUE),
                'address_company'           => $this->input->post('address_company', TRUE),
                'time_workingId'            => $this->input->post('time_workingId', TRUE),
                'department_working'        => $this->input->post('department_working', TRUE),
                'post_working'              => $this->input->post('post_working', TRUE),
                'day_ingress'               => $this->input->post('day_ingress', TRUE),
                'salary'                    => $this->strip_commas($this->input->post('salary', TRUE)),
                'mobile_boss'               => $this->input->post('mobile_boss', TRUE),
                'first_boss'                => $this->input->post('first_boss', TRUE),
                'last_boss'                 => $this->input->post('last_boss', TRUE),
                'phone_boss'                => $this->input->post('phone_boss', TRUE),
                'document_reference'        => $this->input->post('document_reference', TRUE),
                'first_reference'           => $this->input->post('first_reference', TRUE),
                'last_reference'            => $this->input->post('last_reference', TRUE),
                'address_reference'         => $this->input->post('address_reference', TRUE),
                'phone_reference'           => $this->input->post('phone_reference', TRUE),
                'bank_account'              => $this->input->post('bank_account', TRUE),
                'mobile'                    => $this->input->post('mobile', TRUE),
                'type_accountId'            => $this->input->post('type_accountId', TRUE),
                'payment_dayId'             => $this->input->post('payment_dayId', TRUE),
                'requested_amount'          => $this->strip_commas($this->input->post('requested_amount', TRUE)),
            );

            if(!empty($_FILES))
            {
                $ext                      = substr(strrchr($_FILES['file']['name'], "."), 1);
                $data_file = array(
                    'file_name'           => '',
                    'file_type'           => $ext,
                    'allowed_types'       => 'gif|jpg|png|jpeg',
                    'folder'              => 'avatars'
                );

                $result            = $this->com_files->upload($data_file);

                if($result["result"] != 0)
                {
                    $data['image']  = $result['file'];
                }
            }

            if($clientId = $this->clients_model->save($data))
            {
                foreach($_POST['question'] AS $key => $value)
                {
                    $questions = array(
                        'clientId'  => $clientId,
                        'question'  => $_POST['question'][$key],
                        'response'  => $_POST['response'][$key],
                    );

                    $this->security_questions_model->save($questions);
                }

                echo json_encode(array('result' => 1, 'url' => base_url('clients')));
            }
        }
    }

    public function update($clientId)
    {
        $row = $this->clients_model->get_by(array('clientId' => $clientId), TRUE);

        $this->form_validation->set_rules('document','<strong>Cédula</strong>','trim|required');
        $this->form_validation->set_rules('bankId','<strong>Banco</strong>','is_natural_no_zero');
        $this->form_validation->set_rules('payment_dayId','<strong>Dia de Cobro</strong>','is_natural_no_zero');
        $this->form_validation->set_rules('type_accountId','<strong>Tipo de Cuenta</strong>','is_natural_no_zero');
        $this->form_validation->set_rules('living_placeId','<strong>Vivienda</strong>','is_natural_no_zero');
        $this->form_validation->set_rules('time_workingId','<strong>Tiempo Laborando</strong>','is_natural_no_zero');

        if($this->form_validation->run($this) == FALSE)
        {
            echo json_encode(array('result' => 0, 'error' => display_error(validation_errors())));
        }
        else
        {
            $data = array(
                'document'                  => $this->input->post('document', TRUE),
                'first_name'                => $this->input->post('first_name', TRUE),
                'last_name'                 => $this->input->post('last_name', TRUE),
                'phone'                     => $this->input->post('phone', TRUE),
                'phone_work'                => $this->input->post('phone_work', TRUE),
                'ext_phone_work'            => $this->input->post('ext_phone_work', TRUE),
                'email'                     => $this->input->post('email', TRUE),
                'email_work'                => $this->input->post('email_work', TRUE),
                'name_facebook'             => $this->input->post('name_facebook', TRUE),
                'link_facebook'             => $this->input->post('link_facebook', TRUE),
                'bankId'                    => $this->input->post('bankId', TRUE),
                'user_bank'                 => $this->input->post('user_bank', TRUE),
                'password_bank'             => $this->input->post('password_bank', TRUE),
                'password_transactional'    => $this->input->post('password_transactional', TRUE),
                'living_placeId'            => $this->input->post('living_placeId', TRUE),
                'street'                    => $this->input->post('street', TRUE),
                'residency'                 => $this->input->post('residency', TRUE),
                'sector'                    => $this->input->post('sector', TRUE),
                'province'                  => $this->input->post('province', TRUE),
                'entering_by'               => $this->input->post('entering_by', TRUE),
                'between_the_street'        => $this->input->post('between_the_street', TRUE),
                'reference_point'           => $this->input->post('reference_point', TRUE),
                'living_color'              => $this->input->post('living_color', TRUE),
                'apartament_of_house'       => (isset($_POST['apartament_of_house']))? $this->input->post('apartament_of_house', TRUE) : 0,
                'levels_apartment'          => $this->input->post('levels_apartment', TRUE),
                'iron_color'              => $this->input->post('iron_color', TRUE),
                'awning_color'            => $this->input->post('awning_color', TRUE),
                'remains_apartment'       => $this->input->post('remains_apartment', TRUE),
                'from_hour'                 => $this->input->post('from_hour', TRUE),
                'to_hour'                   => $this->input->post('to_hour', TRUE),
                'day_work'                  => $this->input->post('day_work', TRUE),
                'company'                   => $this->input->post('company', TRUE),
                'sector_company'            => $this->input->post('sector_company', TRUE),
                'address_company'           => $this->input->post('address_company', TRUE),
                'time_workingId'            => $this->input->post('time_workingId', TRUE),
                'department_working'        => $this->input->post('department_working', TRUE),
                'post_working'              => $this->input->post('post_working', TRUE),
                'day_ingress'               => $this->input->post('day_ingress', TRUE),
                'salary'                    => $this->strip_commas($this->input->post('salary', TRUE)),
                'mobile_boss'               => $this->input->post('mobile_boss', TRUE),
                'first_boss'                => $this->input->post('first_boss', TRUE),
                'last_boss'                 => $this->input->post('last_boss', TRUE),
                'phone_boss'                => $this->input->post('phone_boss', TRUE),
                'document_reference'        => $this->input->post('document_reference', TRUE),
                'first_reference'           => $this->input->post('first_reference', TRUE),
                'last_reference'            => $this->input->post('last_reference', TRUE),
                'address_reference'         => $this->input->post('address_reference', TRUE),
                'phone_reference'           => $this->input->post('phone_reference', TRUE),
                'bank_account'              => $this->input->post('bank_account', TRUE),
                'mobile'                    => $this->input->post('mobile', TRUE),
                'type_accountId'            => $this->input->post('type_accountId', TRUE),
                'payment_dayId'             => $this->input->post('payment_dayId', TRUE),
                'requested_amount'          => $this->strip_commas($this->input->post('requested_amount', TRUE)),
            );

            if(!empty($_FILES))
            {
                $ext                      = substr(strrchr($_FILES['file']['name'], "."), 1);
                $data_file = array(
                    'file_name'           => '',
                    'file_type'           => $ext,
                    'allowed_types'       => 'gif|jpg|png|jpeg',
                    'folder'              => 'avatars'
                );

                $result            = $this->com_files->upload($data_file);

                if($result["result"] != 0)
                {
                    $data['image']  = $result['file'];
                    if($row->image != null || $row->image != "")
                    {
                        $this->com_files->unlink_image('avatars', $row->image);
                    }
                }
            }

            if($this->clients_model->save($data, $clientId))
            {
                foreach($_POST['question'] AS $key => $value)
                {
                    $questions = array(
                        'clientId'  => $clientId,
                        'question'  => $_POST['question'][$key],
                        'response'  => $_POST['response'][$key],
                    );

                    if($_POST['questionId'][$key] == 0)
                    {
                        $this->security_questions_model->save($questions);
                    }
                    else
                    {
                        $this->security_questions_model->save($questions, $_POST['questionId'][$key]);
                    }
                }

                echo json_encode(array('result' => 1, 'url' => base_url('clients')));
            }
        }
    }

    public function delete($clientId)
    {
        if($this->clients_model->delete($clientId) === TRUE)
        {
            echo json_encode(array('result' => 1));
        }
        else
        {
            echo json_encode(array('result' => 0));
        }
    }

    public function add_documents($clientId)
    {
        $return = $this->load->view('clients/widgets/add_document_view', array("clientId" => $clientId), TRUE);
        echo json_encode(array('result' => 1, 'view' => $return));
    }

    public function save_files($clientId)
    {
        if(!empty($_FILES))
        {
            $ext                      = substr(strrchr($_FILES['file']['name'][0], "."), 1);
            $data_file = array(
                'file_name'           => '',
                'file_type'           => $ext,
                'folder'              => 'files',
                'max_size'            => 5000000
            );

            $result            = $this->com_files->upload($data_file);

            if($result["result"] != 0)
            {
                $data['clientId']       = $clientId;
                $data['name']           = $result['file'];
                $data['size']           = $_FILES['file']['size'][0];
                $data['original_name']  = $_FILES['file']['name'][0];

                $this->docs_model->save($data);

                echo json_encode(array("result" => 1, "view" => $this->get_all_documents($clientId)));
            }
        }
    }

    public function unlink_files($docId)
    {
        $doc_name = $this->docs_model->get_single($docId);

        if(isset($doc_name))
        {
            $this->docs_model->update(array("hidden" => 1), $docId);
            if(file_exists(APPPATH.'../assets/storage/files/'.$doc_name->name))
            {
                unlink(APPPATH.'../assets/storage/files/'.$doc_name->name);
            }

            echo json_encode(array("result" => 1, "view" => $this->get_all_documents($doc_name->clientId, "return")));
        }
        else
        {
            echo json_encode(array("result" => 0));
        }
    }

    public function get_all_documents($clientId, $output = "json")
    {
        $data['docs']      = $this->docs_model->get_by(array("clientId" => $clientId, "hidden" => 0));
        $data['clientId']  = $clientId;
        $return            = $this->load->view('clients/widgets/docs_table_view', $data, TRUE);

        switch($output)
        {
            case "json":
                echo json_encode(array('result' => 1, "view" => $return));
                break;
            default:
                return $return;
        }
    }

    public function docs_preview($clientId)
    {
        $data['row']       = $this->clients_model->get_by(array('clientId' => $clientId), TRUE);
        $data['docs']      = $this->docs_model->get_by(array("clientId" => $clientId, "hidden" => 0));
        echo json_encode(array('result' => 1, 'view' => $this->load->view('clients/widgets/docs_table_preview_view', $data, TRUE)));
    }

    /*----------------------------IMPORT-CLIENTS--------------------------------------------*/
    public function import_client()
    {
        echo json_encode(array('result' => 1, 'view' => $this->load->view('clients/widgets/clients_import_view', array(), TRUE)));
    }

    public function import()
    {
        echo json_encode(array('result' => 1, 'view' => $this->com_import->import_upload()));
    }

    public function import_save()
    {
        $result = array();
        if((isset($_POST['import_valid'])) && $_POST['import_valid'] == 1)
        {
            $type_import = $_POST['import_valid'];
            $result      = $this->com_import->import_save($type_import);
        }

        echo json_encode(array('result' => 1, 'msg' => display_import(array('warinng' => $result['warinng'], 'success' => $result['success']))));
    }

    public function empty_data()
    {
        echo json_encode(array('data' => array()));
    }

    public function ouput_import()
    {
        $this->com_import->ouput_import();
    }
}
