<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="breadcrumb-holder">
                        <h1 class="main-title float-left">Clientes</h1>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="<?php echo base_url()?>dashboard">Inicio</a></li>
                            <li class="breadcrumb-item active">Clientes</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card mb-3">
                        <div class="card-header">
                            <div id="import-msg"></div>
                            <div class="btn-group pull-right">
                                <a class="btn btn-primary" href="<?php echo base_url('clients/add');?>"><i class="fa fa-users"></i> Nuevo</a>
                                <div class="btn-group dropdown" role="group">
                                    <button type="button" id="btn-dropdown" class="btn border-radius-none btn-info dropdown-toggle" data-toggle="dropdown"></button>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="btn-dropdown">
                                        <a class="dropdown-item" href="<?php echo base_url('clients/add');?>"><i class="fa fa-users"></i> Nuevo</a>
                                        <a class="dropdown-item modal_trigger" data-url="<?php echo base_url('clients/import_client');?>" data-target="#import_clientes" data-toggle="modal" href="javascript:void(0);"><i class="fa fa-cloud-upload"></i> Importar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end card-header -->
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="list" class="table table-bordered display dataTable no-footer" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Cedula</th>
                                            <th>Nombre</th>
                                            <th>Apellido</th>
                                            <th class="text-right">Sueldo</th>
                                            <th>Banco</th>
                                            <th>Teléfono</th>
                                            <th></th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end card-body -->
                        <div class="card-body p-b-0 p-t-0"><hr></div>
                        <div class="card-body legend">
                            <div class="col-md-6">
                                <i class="fa fa-cloud-upload"></i>
                                <h5>Importar Clientes via Excel</h5>
                                <span><a class="modal_trigger" href="javascript:void(0);" data-url="<?php echo base_url('clients/import_client');?>" data-target="#import_clientes" data-toggle="modal">Importa tus Clientes desde Excel!</a> (<span>Usa ésta</span> <a href="<?php echo base_url('assets/storage/docs_import/clients.xlsx')?>">Plantilla de Importación</a>)</span>
                            </div>
                            <div class="col-md-6">
                                <i class="fa fa-cloud-download"></i>
                                <h5>Actualiza Clientes via Excel</h5>
                                <span>Descargar listado de clientes (<a id="export-all" href="<?php echo base_url('clients/ouput_import');?>">Lista de Clientes</a>) y <a class="modal_trigger" href="javascript:void(0);" data-url="<?php echo base_url('clients/import_client');?>" data-target="#import_clientes" data-toggle="modal">Subir listado actualizado.</a></span>
                            </div>
                        </div>
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- END container-fluid -->
    </div>
    <!-- END content -->
</div>
<!-- END content-page -->

<div id="docs_clients" class="modal fade custom-modal" tabindex="-1" role="dialog" aria-labelledby="modal_user" aria-hidden="true">
    <div class="modal-dialog"></div>
</div>

<div id="clients_preview" class="modal fade custom-modal" tabindex="-1" role="dialog" aria-labelledby="modal_user" aria-hidden="true">
    <div class="modal-dialog"></div>
</div>

<div id="import_clientes" class="modal fade custom-modal" tabindex="-1" role="dialog" aria-labelledby="modal_user" aria-hidden="true">
    <div class="modal-dialog"></div>
</div>

