<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="breadcrumb-holder">
                        <ol class="breadcrumb float-left">
                            <li class="breadcrumb-item"><a href="<?php echo base_url()?>dashboard">Inicio</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url()?>clients">Clientes</a></li>
                            <li class="breadcrumb-item active">Nuevo <span></span></li>
                        </ol>
                        <div class="col-md-3 float-right text-right">
                            <a href="javascript:void(0)" class="cancel" data-redirect="<?php echo base_url('clients')?>">
                                <i class="fa fa-times-circle-o bigfonts" aria-hidden="true" style="font-size: 30px!important; color: #f96262"></i>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <form id="form-clients" action="<?php echo base_url('clients/insert');?>" method="post" enctype="multipart/form-data">
                        <div class="card mb-3">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <nav>
                                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-person" role="tab" aria-controls="nav-home" aria-selected="true">
                                                    <h6><i class="fa fa-drivers-license-o"></i> Personal</h6>
                                                </a>
                                                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-internet-banking" role="tab" aria-controls="nav-profile" aria-selected="false">
                                                    <h6><i class="fa fa-bank"></i> Internet Banking</h6>
                                                </a>
                                                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-recidence" role="tab" aria-controls="nav-recidence" aria-selected="false">
                                                    <h6><i class="fa fa-drivers-license-o"></i> Residencia</h6>
                                                </a>
                                                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-work" role="tab" aria-controls="nav-work" aria-selected="false">
                                                    <h6><i class="fa fa-handshake-o"></i> Trabajo</h6>
                                                </a>
                                                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-reference" role="tab" aria-controls="nav-reference" aria-selected="false">
                                                    <h6><i class="fa fa-cloud-upload"></i> Referencia</h6>
                                                </a>
                                            </div>
                                        </nav>
                                        <div class="tab-content" id="nav-tabContent">
                                            <div class="tab-pane fade show active" id="nav-person" role="tabpanel" aria-labelledby="nav-person-tab">
                                                <div class="card mb-3">
                                                    <!-- end card-header-->
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-11 offset-1 alpha">
                                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                            <div class="fileinput-new img-thumbnail" style="width: 200px; height: 150px; text-align: center">
                                                                                <img src="<?php echo base_url()?>assets/images/avatars/user.png"  alt="...">
                                                                            </div>
                                                                            <div class="fileinput-preview fileinput-exists img-thumbnail" style="max-width: 200px; max-height: 150px; text-align: center"></div>
                                                                            <div class="file-buttons">
                                                                                <span class="btn btn-outline-secondary btn-file"><span class="fileinput-new"><i class="fa fa-cloud-upload"></i></span><span class="fileinput-exists">Cambiar</span><input type="file" name="file"></span>
                                                                                <a href="javascript:void();" class="btn btn-outline-secondary fileinput-exists" data-dismiss="fileinput">Remover</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            </div>

                                                            <div class="col-md-6 alpha omega">
                                                                <div class="row form-group">
                                                                    <label for="document" class="col-md-3 control-label text-right mg-top">Cédula :</label>
                                                                    <input type="text" name="document" class="form-control cedula col-md-5" id="document" placeholder="XXX-XXXXXXX-X" data-field="document">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="first_name" class="col-md-3 control-label text-right mg-top">Nombre:</label>
                                                                    <input type="text" name="first_name" class="form-control col-md-6" id="first_name" placeholder="Nombre" data-field="first_name">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="last_name" class="col-md-3 control-label text-right mg-top">Apellido:</label>
                                                                    <input type="text" name="last_name" class="form-control col-md-6" id="last_name" placeholder="Apellido" data-field="last_name">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="phone" class="col-md-3 control-label text-right mg-top">Teléfono:</label>
                                                                    <input type="text" name="phone" class="col-md-5 form-control phone"  id="phone" placeholder="Teléfono" data-field="phone">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="mobile" class="col-md-3 control-label text-right mg-top">Celular:</label>
                                                                    <input type="text" name="mobile" class="col-md-5 form-control phone" id="mobile" placeholder="Celular" data-field="mobile">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 alpha omega">
                                                                <div class="row form-group">
                                                                    <label for="email" class="col-md-3 control-label text-right mg-top">Correo:</label>
                                                                    <input type="text" name="email" class="col-md-6 form-control" id="email" placeholder="Correo" data-field="email">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="name_facebook" class="col-md-3 control-label text-right mg-top">Facebook:</label>
                                                                    <input type="text" name="name_facebook" class="col-md-6 form-control" id="name_facebook" placeholder="Facebook" data-field="name_facebook">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="link_facebook" class="col-md-3 control-label text-right mg-top">Perfil de Facebook:</label>
                                                                    <input type="text" name="link_facebook" class="col-md-6 form-control" id="link_facebook" placeholder="Perfil de facebook" data-field="link_facebook">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="requested_amount" class="col-md-3 control-label text-right mg-top">Monto Solicitado:</label>
                                                                    <input type="text" name="requested_amount" class="col-md-4 form-control numeric currency" id="requested_amount" data-field="requested_amount">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- end card-body -->
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="nav-internet-banking" role="tabpanel" aria-labelledby="nav-internet-banking-tab">
                                                <div class="card mb-3">
                                                    <!-- end card-header-->
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="row form-group">
                                                                    <label for="bankId" class="col-md-3 control-label text-right mg-top">Banco:</label>
                                                                    <div class="col-md-6 alpha">
                                                                        <?php echo form_dropdown('bankId', $this->bank, set_value('banId',0), "id='bankId' class='form-control chosen-select' data-placeholder='Seleccione una Opción' data-field='bankId'");?>
                                                                    </div>
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="payment_dayId" class="col-md-3 control-label text-right mg-top">Tipo de Cobro:</label>
                                                                    <div class="col-md-6 alpha">
                                                                        <?php echo form_dropdown('payment_dayId', $this->payment_day, set_value('payment_dayId',1), "id='payment_dayId' class='form-control chosen-select' data-placeholder='Seleccione una Opción' data-field='payment_dayId'");?>
                                                                    </div>
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="day_work" class="col-md-3 control-label text-right mg-top day_work">F. Prox. Cobro:</label>
                                                                    <div class="col-md-5 alpha">
                                                                        <input type="text" name="day_work" class="form-control date" readonly id="day_work">
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="type_accountId" class="col-md-3 control-label text-right mg-top">Tipo de Cuenta:</label>
                                                                    <div class="col-md-6 alpha">
                                                                        <?php echo form_dropdown('type_accountId', $this->type, set_value('type_accountId',0), "id='type_accountId' class='form-control chosen-select' data-placeholder='Seleccione una Opción' data-field='type_accountId'");?>
                                                                    </div>
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="bank_account" class="col-md-3 control-label text-right mg-top">Cuenta:</label>
                                                                    <input type="text" name="bank_account" class="col-md-6 form-control" id="bank_account" placeholder="Cuenta de Banco" data-field="bank_account">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row form-group">
                                                                    <label for="user_bank" class="col-md-4 control-label text-right mg-top">Usuario:</label>
                                                                    <input type="text" name="user_bank" class="col-md-5 form-control" id="user_bank" placeholder="Usuario NetBanking" data-field="user_bank">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="password_bank" class="col-md-4 control-label text-right mg-top">Clave:</label>
                                                                    <input type="text" name="password_bank" class="col-md-5 form-control" id="password_bank" placeholder="Clave NetBanking" data-field="password_bank">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="password_transactional" class="col-md-4 control-label text-right mg-top">Clave Transaccional:</label>
                                                                    <input type="text" name="password_transactional" class="col-md-5 form-control" id="password_transactional" placeholder="Clave Transaccional" data-field="password_transactional">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row form-group">
                                                            <label class="col-md-3 control-label text-right mg-top">Preguntas de Seguridad:</label>
                                                            <div class="col-md-9" id="security_questions">
                                                                <div class="row form-group">
                                                                    <label class="col-md-1 control-label text-right mg-top mg-left-19 count">1:</label>
                                                                    <div class="col-md-4"><input type="text" name="question[]" class="form-control" id="question" placeholder="Pregunta de Seguridad"></div>
                                                                    <div class="col-md-2 quetions"> <span>?</span> </div>
                                                                    <div class="col-md-4"><input type="text" name="response[]" class="form-control" id="response" placeholder="Respuesta de Seguridad"></div>
                                                                    <div class="col-md-1">
                                                                        <button type="button" class="close trigger_remove_row mg-8"><i class="fa fa-window-close-o close close-icon-windows"></i></button>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group p-t-10 last_row">
                                                                    <button type="button" class="btn btn-button btn-primary trigger_add_row"><i class="fa fa-plus"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- end card-body -->
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="nav-recidence" role="tabpanel" aria-labelledby="nav-recidence-tab">
                                                <div class="card mb-3">
                                                    <!--end card-header-->
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="row form-group">
                                                                    <label for="living_placeId" class="col-md-4 control-label text-right mg-top">Vivienda:</label>
                                                                    <div class="col-md-6 alpha"><?php echo form_dropdown('living_placeId', $this->living, set_value('living_placeId',0),"id='living_placeId' data-placeholder='-seleccione una opción-' class='form-control chosen-select'");?></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="street" class="col-md-4 control-label text-right mg-top">Calle:</label>
                                                                    <input type="text" name="street" class="col-md-5 form-control" id="street" placeholder="Calle" data-field="street">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="residency" class="col-md-4 control-label text-right mg-top">No. Recidencia:</label>
                                                                    <input type="text" name="residency" class="col-md-2 form-control numeric" id="residency" placeholder="No." data-field="residency">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="sector" class="col-md-4 control-label text-right mg-top">Sector:</label>
                                                                    <input type="text" name="sector" class="col-md-5 form-control" id="sector" placeholder="Sector" data-field="sector">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="province" class="col-md-4 control-label text-right mg-top">Provincia:</label>
                                                                    <input type="text" name="province" class="col-md-5 form-control" id="province" placeholder="Provincia" data-field="province">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="entering_by" class="col-md-4 control-label text-right mg-top ">Entrando Por:</label>
                                                                    <input type="text" name="entering_by" class="col-md-5 form-control" id="entering_by" placeholder="Entrando Por" data-field="entering_by">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="between_the_street" class="col-md-4 control-label text-right mg-top">Entre la Calle:</label>
                                                                    <input type="text" name="between_the_street" class="col-md-5 form-control" id="between_the_street" placeholder="Entre la calle" data-field="between_the_street">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="reference_point" class="col-md-4 control-label text-right mg-top">Punto de Referencia:</label>
                                                                    <input type="text" name="reference_point" class="col-md-5 form-control" id="reference_point" placeholder="Punto de referencia" data-field="reference_point">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="living_colorId" class="col-md-4 control-label text-right mg-top">Color de Vivienda:</label>
                                                                    <input type="text" name="living_color" class="col-md-5 form-control" id="living_color" placeholder="Color de la vivienda" data-field="living_color">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row form-group">
                                                                    <label for="house" class="col-md-4 control-label text-right mg-top">Es una casa?:</label>
                                                                    <div class="col-md-1">
                                                                        <label class="radio">
                                                                            <input type="radio" class="" name="apartament_of_house" value="1" id="house">
                                                                            <span class="check"></span>
                                                                        </label>
                                                                    </div>
                                                                    <label for="apartament" class="col-md-4 control-label text-right mg-top">Es un apartamento?:</label>
                                                                    <div class="col-md-1">
                                                                        <label class="radio">
                                                                            <input type="radio" class="" name="apartament_of_house" value="2" id="apartament">
                                                                            <span class="check"></span>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div id="is_house_hidden">
                                                                    <div class="row form-group">
                                                                        <label for="levels_apartment" class="col-md-4 control-label text-right mg-top">Niveles:</label>
                                                                        <input type="text" name="levels_apartment" class="col-md-6 form-control" id="levels_apartment" placeholder="Niveles" data-field="levels_apartment">
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <label for="iron_color" class="col-md-4 control-label text-right mg-top">Color Hierros:</label>
                                                                        <input type="text" name="iron_color" class="col-md-6 form-control" id="iron_color" placeholder="Color de hierros" data-field="iron_color">
                                                                    </div>
                                                                    <div class="row form-group">
                                                                        <label for="awning_color" class="col-md-4 control-label text-right mg-top">Color Toldos:</label>
                                                                        <input type="text" name="awning_color" class="col-md-6 form-control" id="awning_color" placeholder="Color de toldos" data-field="awning_color">
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="remains_apartment" class="col-md-4 control-label text-right mg-top">Vivienda queda:</label>
                                                                    <input type="text" name="remains_apartment" class="col-md-6 form-contr" id="remains_apartment" placeholder="Vivienda queda" data-field="remains_apartment">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- end card-body -->
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="nav-work" role="tabpanel" aria-labelledby="nav-work-tab">
                                                <div class="card mb-3">
                                                    <!-- end card-header-->
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="row form-group">
                                                                    <label for="phone_work" class="col-md-3 control-label text-right mg-top">Teléfono:</label>
                                                                    <input type="text" name="phone_work" class="col-md-4 form-control phone" id="phone_work" placeholder="Teléfono">
                                                                    <input type="text" name="ext_phone_work" class="col-md-2 m-l-1 form-control ext" id="ext_phone_work" placeholder="Ext:">
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="email_work" class="col-md-3 control-label text-right mg-top">Correo Laboral:</label>
                                                                    <input type="text" name="email_work" class="col-md-6 form-control" id="email_work" placeholder="Correo Laboral">
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label class="col-md-3 control-label text-right mg-top">Horario:</label>
                                                                    <div class="input-group alpha col-md-4" id="timepicker-AM" data-target-input="nearest">
                                                                        <div class="input-group" data-target="#timepicker-AM" data-toggle="datetimepicker">
                                                                            <input type="text" name="from_hour" class="form-control datetimepicker-input" data-target="#timepicker-AM">
                                                                            <div class="input-group-addon input-group-append"><i class="fa fa-clock-o input-group-text l-h-22"></i></div>
                                                                        </div>
                                                                    </div>
                                                                    <span class="l-h-35"> | </span>
                                                                    <div class="input-group col-md-4" id="timepicker-PM" data-target-input="nearest">
                                                                        <div class="input-group" data-target="#timepicker-PM" data-toggle="datetimepicker">
                                                                            <input type="text" name="to_hour" class="form-control datetimepicker-input" data-target="#timepicker-PM">
                                                                            <div class="input-group-addon input-group-append"><i class="fa fa-clock-o input-group-text l-h-22"></i></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="company" class="col-md-3 control-label text-right mg-top">Empresa:</label>
                                                                    <input type="text" name="company" class="col-md-6 form-control" id="company" placeholder="Nombre de la empresa">
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="sector_company" class="col-md-3 control-label text-right mg-top">Sector:</label>
                                                                    <input type="text" name="sector_company" class="col-md-6 form-control" id="sector_company" placeholder="Sector">
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="address_company" class="col-md-3 control-label text-right mg-top">Dirección:</label>
                                                                    <input type="text" name="address_company" class="col-md-6 form-control" id="address_company" placeholder="Dirección">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row form-group">
                                                                    <label for="time_workingId" class="col-md-4 control-label text-right mg-top">Tiempo Laborando:</label>
                                                                    <div class="col-md-5 alpha omega"><?php echo form_dropdown('time_workingId', $this->time_working, set_value('time_workingId',0),"id='time_workingId' data-placeholder='-seleccione una opción-'  class='form-control chosen-select'");?></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="department_working" class="col-md-4 control-label text-right mg-top">Departamento:</label>
                                                                    <input type="text" name="department_working" class="form-control col-md-5" id="department_working" placeholder="Departamento">
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="post_working" class="col-md-4 control-label text-right mg-top">Puesto:</label>
                                                                    <input type="text" name="post_working" class="form-control col-md-5" id="post_working" placeholder="Puesto">
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="day_ingress" class="col-md-4 control-label text-right mg-top">Fecha de Ingreso:</label>
                                                                    <input type="text" name="day_ingress" class="form-control col-md-5 date" id="day_ingress" readonly placeholder="Fecha de ingreso">
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="salary" class="col-md-4 control-label text-right mg-top">Salario:</label>
                                                                    <input type="text" name="salary" class="form-control col-md-5 numeric currency" id="salary" placeholder="Salario">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 p-t-30">
                                                                <h5>Datos del Supervisor / Jefe</h5>
                                                                <hr>
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="row form-group">
                                                                            <label for="first_boss" class="col-md-3 control-label text-right mg-top">Nombre:</label>
                                                                            <div class="col-md-6"><input type="text" name="first_boss" class="form-control" id="first_boss" placeholder="Nombre del Supervisor/Jefe"></div>
                                                                        </div>
                                                                        <div class="row form-group">
                                                                            <label for="last_boss" class="col-md-3 control-label text-right mg-top">Apellido:</label>
                                                                            <div class="col-md-6"><input type="text" name="last_boss" class="form-control" id="last_boss" placeholder="Apellido del Supervisor/Jefe"></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="row form-group">
                                                                            <label for="mobile_boss" class="col-md-3 control-label text-right mg-top">Celular:</label>
                                                                            <div class="col-md-6"><input type="text" name="mobile_boss" class="form-control phone" id="mobile_boss" placeholder="Celular de Supervisor/Jefe"></div>
                                                                        </div>
                                                                        <div class="row form-group">
                                                                            <label for="phone_boss" class="col-md-3 control-label text-right mg-top">Teléfono:</label>
                                                                            <div class="col-md-6"><input type="text" name="phone_boss" class="form-control phone" id="phone_boss" placeholder="Teléfono del Supervisor/Jefe"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- end card-body -->
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="nav-reference" role="tabpanel" aria-labelledby="nav-reference-tab">
                                                <div class="card mb-3">
                                                    <!-- end card-header-->
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="row form-group">
                                                                    <label for="first_reference" class="col-md-3 control-label text-right mg-top">Nombre:</label>
                                                                    <input type="text" name="first_reference" class="form-control col-md-5" id="first_reference" placeholder="Nombre de la referencia">
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="last_reference" class="col-md-3 control-label text-right mg-top">Apellido:</label>
                                                                    <input type="text" name="last_reference" class="form-control col-md-5" id="last_reference" placeholder="Apellido de la referencia">
                                                                </div>
                                                                <div for="address_reference" class="row form-group">
                                                                    <label class="col-md-3 control-label text-right mg-top">Dirección:</label>
                                                                    <input type="text" name="address_reference" class="form-control col-md-5" id="address_reference" placeholder="Dirección de la referencia">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="row form-group">
                                                                    <label for="document_reference" class="col-md-3 control-label text-right mg-top">Cédula :</label>
                                                                    <input type="text" name="document_reference" class="form-control col-md-5 cedula" id="document_reference" placeholder="Cédula de la referencia">
                                                                </div>
                                                                <div class="row form-group">
                                                                    <label for="phone_reference" class="col-md-3 control-label text-right mg-top">Teléfono:</label>
                                                                    <input type="text" name="phone_reference" class="form-control col-md-5 phone" id="phone_reference" placeholder="Teléfono de la referencia">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- end card-body -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-right p-b-10">
                                <button id="button-submit" type="submit" class="btn btn-primary ladda-button" data-style="expand-right">
                                    <span class="ladda-label">Guardar</span>
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- END container-fluid -->
    </div>
    <!-- END content -->
</div>
<!-- END content-page -->
<div class="hidden" id="questions_hidden">
    <div class="row form-group">
        <label class="col-md-1 control-label text-right mg-top mg-left-19 count">1:</label>
        <div class="col-md-4"><input type="text" name="question[]" class="form-control" id="question" placeholder="Pregunta de Seguridad"></div>
        <div class="col-md-2 quetions"> <span>?</span> </div>
        <div class="col-md-4"><input type="text" name="response[]" class="form-control" id="response" placeholder="Respuesta de Seguridad"></div>
        <div class="col-md-1"><button type="button" class="close trigger_remove_row mg-8"><i class="fa fa-window-close-o close close-icon-windows"></i></button></div>
    </div>
</div>

