<?php if(count($docs) > 0):?>
    <div class="col-md-12">
        <span class="pull-right"><a href="javascript:void(0)" class="btn btn-primary btn-sm btn-doc modal_trigger" data-url="<?php echo base_url('clients/add_documents/'.$clientId)?>" data-toggle="modal" data-target="#doc_modal"><i class="fa fa-file" aria-hidden="true"></i> Agregar</a></span>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table id="docs" class="table table-bordered display dataTable no-footer" style="width:100%">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Peso</th>
                        <th>Acci&oacute;n</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($docs AS $doc):?>
                    <tr>
                        <td><a href="<?php echo base_url('assets/storage/files/'.$doc->name);?>" target="_blank"><?php echo $doc->original_name?></a></td>
                        <td><?php echo number_format($doc->size/1024,0);?>KB</td>
                        <td><a role="button" href="javascript:void(0)" data-url="<?php echo base_url('clients/unlink_files/'.$doc->docId);?>" class="btn btn-danger unlink-file"><i class="fa fa-times"></i></a></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
<?php else:?>
    <div class="card-body alpha omega">
        <div class="col-md-12">
            <div class="new-documents">
                <a href="javascript:void(0)" class="btn btn-primary btn-doc modal_trigger" data-url="<?php echo base_url('clients/add_documents/'.$clientId);?>" data-toggle="modal" data-target="#doc_modal"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Buscar Archivo</a>
            </div>
        </div>
    </div>
<?php endif;?>