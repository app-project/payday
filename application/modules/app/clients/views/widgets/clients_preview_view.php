<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-fw fa-user-circle"></i> <?php echo $row->first_name.' '.$row->last_name;?></h5>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    </div>
    <div class="modal-body">
        <div class="card-body">
            <div class="row">
                <div class="col-md-2 solid-black">
                    <?php $image = ($row->image != "" || $row->image != null) ? "assets/storage/avatars/".$row->image : "assets/images/avatars/user.png"?>
                    <img src="<?php echo base_url().$image;?>" width="100px" height="100px" class="rounded float-left" alt="...">
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <label for="" class="control-label col-md-3 text-right mg-top">Cedula:</label>
                        <div class="col-md-6 mg-top"><span><?php echo $row->document;?></span></div>
                    </div>
                    <div class="row">
                        <label for="" class="control-label  col-md-3 text-right mg-top">Nombre:</label>
                        <div class="col-md-8 mg-top"><span><?php echo $row->first_name.' '.$row->last_name;?></span></div>
                    </div>
                    <div class="row">
                        <label for="" class="control-label  col-md-3 text-right mg-top">Telefono:</label>
                        <div class="col-md-6 mg-top"><span><?php echo $row->phone;?></span></div>
                    </div>
                    <div class="row">
                        <label for="" class="control-label  col-md-3 text-right mg-top">Celular:</label>
                        <div class="col-md-6 mg-top"><span><?php echo $row->mobile;?></span></div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <label for="" class="control-label  col-md-6  text-right mg-top">Salario:</label>
                        <div class="col-md-6 mg-top"><span>$<?php echo number_format($row->salary, 2);?></span></div>
                    </div>
                    <div class="row">
                        <label for="" class="control-label  col-md-6  text-right mg-top">Monto Solicitado:</label>
                        <div class="col-md-6 mg-top"><span>$<?php echo number_format($row->requested_amount, 2);?></span></div>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-internet-banking" role="tab" aria-controls="nav-home" aria-selected="true"><i class="fa fa-bank"></i> Internet Banking</a>
                <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-contact-tab" role="tab" aria-controls="nav-home" aria-selected="true"><i class="fa fa-drivers-license-o"></i> Residencia</a>
                <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-work-tab" role="tab" aria-controls="nav-home" aria-selected="true"><i class="fa fa-handshake-o"></i> Trabajo</a>
                <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-references-tab" role="tab" aria-controls="nav-home" aria-selected="true"><i class="fa fa-unlink"></i> Referencia</a>
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-internet-banking" role="tabpanel" aria-labelledby="nav-internet-banking-tab">
                <div class="card mb-3">
                    <!-- end card-header-->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Banco:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->bank_name;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Tipo de Cobro:</label>
                                    <div class="col-md-6 mg-top"><span><?php echo $row->payment_name;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">F. Prox. Cobro:</label>
                                    <div class="col-md-6 mg-top"><span><?php echo $row->day_work;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Tipo de Cuenta:</label>
                                    <div class="col-md-6 mg-top"><span><?php echo $row->account_type;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Cuenta:</label>
                                    <div class="col-md-6 mg-top"><span><?php echo $row->bank_account;?></span></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <label for="" class="control-label col-md-6 text-right mg-top">Usuario:</label>
                                    <div class="col-md-6 mg-top"><span><?php echo $row->user_bank;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-6 text-right mg-top">Clave:</label>
                                    <div class="col-md-6 mg-top"><span><?php echo $row->password_bank;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-6 text-right mg-top">Clave Transaccional:</label>
                                    <div class="col-md-6 mg-top"><span><?php echo $row->password_transactional;?></span></div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h5>Preguntas de Seguridad</h5>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <?php if(count($security_questions) > 0):
                                    $i = 1;
                                    foreach ($security_questions AS $question):?>
                                        <div class="row">
                                            <label for="" class="control-label col-md-1 text-right mg-top"><?php echo $i++;?>:</label>
                                            <div class="col-md-5 mg-top"><span><?php echo $question->question;?></span></div>
                                            <div class="col-md-1 quetions mg-r-0"> <span>?</span> </div>
                                            <div class="col-md-5 mg-top"><span><?php echo $question->response;?></span></div>
                                        </div>
                                    <?php endforeach;?>
                                <?php else:?>
                                    <div class="row">
                                        <div class="col-md-12 text-center not-docs-color"><label for=""> No tiene Pregutas</label></div>
                                    </div>
                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                    <!-- end card-body -->
                </div>
            </div>
            <div class="tab-pane fade" id="nav-contact-tab" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="card mb-3">
                    <!-- end card-header-->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Vivienda:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->living_name;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Calle:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->street;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">No. Recidencia:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->residency;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Sector:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->sector;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Provincia:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->province;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Entrando Por:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->entering_by;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Entre la Calle:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->between_the_street;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Punto de Referencia:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->reference_point;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Color de Vivienda:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->living_color;?></span></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Es una casa?:</label>
                                    <?php $apartament_of_house = ($row->apartament_of_house == 1)? 'Si' : 'No';?>
                                    <div class="col-md-7 mg-top"><span><?php echo $apartament_of_house;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Es un apartamento?:</label>
                                    <?php $apartament_of_house = ($row->apartament_of_house == 2)? 'Si' : 'No';?>
                                    <div class="col-md-7 mg-top"><span><?php echo $apartament_of_house;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Niveles:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->levels_apartment;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Color Hierros:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->iron_color;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Color Toldos:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->awning_color;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Vivienda queda:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->remains_apartment;?></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
            <div class="tab-pane fade" id="nav-work-tab" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="card mb-3">
                    <!-- end card-header-->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Teléfono:</label>
                                    <div class="col-md-4 mg-top"><span><?php echo $row->phone_work;?></span></div>
                                    <div class="col-md-3 mg-top"><span><?php echo $row->ext_phone_work;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Correo Laboral:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->email_work;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Horario:</label>
                                    <div class="col-md-4 mg-top"><span><?php echo $row->from_hour;?></span></div>
                                    <div class="col-md-3 mg-top"><span><?php echo $row->to_hour;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Empresa:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->company;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Sector:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->sector_company;?></span></div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Dirección:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->entering_by;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Tiempo Laborando:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->time_working;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Departamento:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->department_working;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Puesto:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->post_working;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Fecha de Ingreso:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->day_ingress;?></span></div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h5>Datos del Supervisor / Jefe</h5>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <label for="" class="control-label col-md-3 text-right mg-top">Nombre:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->first_boss;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-3 text-right mg-top">Apellido:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->last_boss;?></span></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <label for="" class="control-label col-md-3 text-right mg-top">Celular:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->mobile_boss;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-3 text-right mg-top">Celular:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->phone_boss;?></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
            <div class="tab-pane fade" id="nav-references-tab" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="card mb-3">
                    <!-- end card-header-->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Nombre:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->first_reference;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Apellido:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->last_reference;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Dirección:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->address_reference;?></span></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Cédula:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->document_reference;?></span></div>
                                </div>
                                <div class="row">
                                    <label for="" class="control-label col-md-5 text-right mg-top">Teléfono:</label>
                                    <div class="col-md-7 mg-top"><span><?php echo $row->phone_reference;?></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
</div>