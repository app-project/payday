<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-cloud-upload"></i> Importar Clientes<span class="title-input"></span> <span class="title-input-p"></span></h5>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    </div>
    <form action="<?php echo base_url('clients/import_save');?>" id="form-import" method="post" enctype="multipart/form-data" role="form">
        <div class="modal-body">
            <div class="response"></div>
             <div class="row">
                 <div class="col-md-12">
                     <div id="message">
                         <input type="file" name="file" id="import_filer">
                     </div>
                     <input type="hidden" id="import_valid" name="import_valid" value="0">
                     <div id="title-import" class="row p-t-15 hidden">
                         <div class="col-md-12 text-center"> <h5 class="modal-title"><i class="fa fa-question-circle-o "></i> Esta Informacion es Correcta?</h5></div>
                     </div>
                     <div id="import-table" class="table-responsive table-wrapper-scroll-y my-custom-scrollbar" style="display: none;">
                         <table id="my-table-import" class="table">
                             <thead>
                             <tr>
                                 <th>Cedula</th>
                                 <th>Nombre</th>
                                 <th>Apellido</th>
                                 <th>Telefono</th>
                                 <th>Celular</th>
                                 <th>Correo</th>
                                 <th>M-Solicitado</th>
                                 <th>F-Prox-Cobro</th>
                                 <th>Salario</th>
                             </tr>
                             </thead>
                             <tbody id="tbody-import">
                             </tbody>
                         </table>
                     </div>
                 </div>
            </div>
        </div>
        <div class="modal-footer text-right">
            <div class="row">
                <button id="save-b" type="submit" class="btn btn-primary ladda-button mg-r-10 hidden" data-style="expand-left"><span class="ladda-label"><i class="fa fa-floppy-o "></i> Agregar</span></button>
                <button type="button" class="btn btn-primary ladda-button mg-r-10" data-dismiss="modal" data-style="expand-left"><span class="ladda-label">Cerrar</span></button>
            </div>
        </div>
    </form>
</div>

<script>
    $(document).ready(function(){
        $("#import_filer").filer({
            limit: 1,
            changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Arrastrar y soltar archivos aquí</h3> <span style="display:inline-block; margin: 15px 0">O</span></div><a class="jFiler-input-choose-btn btn btn-custom">Subir Archivos</a></div></div>',
            theme: "dragdropbox",
            extensions: ['xls', 'xlsx'],
            uploadFile: {
                url: '<?php echo base_url('clients/import');?>',
                data: null,
                type: 'POST',
                enctype: 'multipart/form-data',
                beforeSend: function(){},
                success: function(data){
                    var response = JSON.parse(data);
                    if(response.result == 1){
                        $('#import-table').css('display', 'block');
                        $('#message').addClass('hidden');
                        $('#title-import').removeClass('hidden');
                        $('#save-b').removeClass('hidden');
                        $('#import_valid').val('1');
                        $('#tbody-import').html(response.view).promise().done(function () {
                            $('#import_clientes .modal-dialog').css('max-width', '1080px');
                        });

                    }else{

                        $('#message').html(response.error);
                    }
                }
            },
            captions: {
                errors: {
                    filesType: 'Debe seleccionar un documento de Excel tipo xls|xlsx.'
                }
            }
        });
        $('#title-import').addClass('hidden');
        $('#message').removeClass('hidden');
        $('#import_clientes .modal-dialog').css('max-width', '720px');
    });
</script>