<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_analysis_deposit_items_model extends MY_Model
{
    public function __construct()
    {
        $this->table_name   = "ai_loan_analysis_deposit_items";
        $this->view_name    = "ai_loan_analysis_deposit_items";
        $this->primary_key  = "itemId";
        $this->order_by     = "itemId";
    }
}