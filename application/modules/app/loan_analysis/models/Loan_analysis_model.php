<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_analysis_model extends MY_Model
{
    public function __construct()
    {
        $this->table_name   = "ai_loan_analysis";
        $this->view_name    = "ai_loan_analysis_view";
        $this->primary_key  = "loan_analysiId";
        $this->order_by     = "loan_analysiId";
    }

    public function get_loan_client_data($loan_analysiId)
    {
        return $this->db->query("SELECT a.loan_analysiId,a.notes,a.hidden, b.requested_amount, CONCAT(b.first_name,' ', b.last_name) AS full_name
                                 FROM ai_loan_analysis AS a
                                 LEFT JOIN ai_clients AS b ON a.clientId = b.clientId  
                                 WHERE a.hidden = 0 AND a.statusId != 2 AND a.loan_analysiId = $loan_analysiId")->row();
    }

    public function get_loan_data($loan_analysiId)
    {
        /*
         * TODO: con mas tiempo agregar los campos que iran en el preview pendiente.....
         */

        return $this->db->query("SELECT a.*,
                                 b.*,
                                 c.name AS bank_name,
                                 d.name AS type_account_name
                                 FROM ai_loan_analysis AS a
                                 LEFT JOIN ai_clients AS b ON a.clientId = b.clientId 
	                             LEFT JOIN ai_institution_banks AS c ON a.bankId = c.bankId 
	                             LEFT JOIN ai_clients_type_accounts AS d ON a.type_accountId = d.typeId 
                                 WHERE a.hidden = 0 AND a.loan_analysiId = $loan_analysiId")->row();
    }
}