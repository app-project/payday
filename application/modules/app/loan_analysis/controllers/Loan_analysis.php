<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Loan_analysis extends MY_Controller
{
    public $title;
    public $status_analysis;
    public $columns;
    public $request_code;
    public $bank;
    public $type;
    public $payment_day;

    public function __construct()
    {
        parent::__construct();
        $this->title       = 'Análisis de Prestamos';
        $this->securities->is_logged_in($this->session->userdata('is_logged_in'));

        //Load Model
        $this->load->model('loan_analysis/loan_analysis_model');
        $this->load->model('loan_analysis/loan_analysis_status_model');
        $this->load->model('loan_analysis/loan_analysis_deposit_items_model');
        $this->load->model('institution_banks/institution_bank_model');
        $this->load->model('clients/clients_type_accounts_model');
        $this->load->model('clients/payment_days_model');
        $this->load->model('clients/payment_days_model');
        $this->load->model('clients/clients_model');
        $this->load->model('company/settings_model');

        //Load Module
        $this->load->module('com_clients/controller/com_clients');

        $this->payment_day      = $this->payment_days_model->get_assoc_list('payment_dayId AS id, name', array('hidden' => 0));
        $this->bank             = $this->institution_bank_model->get_assoc_list('bankId AS id, name', array('hidden' => 0));
        $this->status_analysis  = $this->loan_analysis_status_model->get_assoc_list('statusId AS id, name', FALSE, TRUE, array(1,2,3));
        $this->request_code     = $this->loan_analysis_model->count_by(array('hidden' => 0));
        $this->type             = $this->clients_type_accounts_model->get_assoc_list('typeId AS id, CONCAT(code, "-", name) AS name', array('hidden' => 0));

        $this->columns          = "loan_analysiId,date_issue,request_code,total_deposit,full_name,quota_monthly,biweekly_quota,weekly_quota,status,class,statusId,sent_mail,clientId";
    }

    public function index()
    {
        $data            = array();
        $data['content'] = 'loan_analysis/loan_analysis_view';
        $this->load->view('include/template', $data);
    }

    public function datatable()
    {
       $all_loan_analysis = $this->loan_analysis_model->datatable($this->columns, FALSE, TRUE);
        echo json_encode(array('data' => $all_loan_analysis));
    }

    public function add()
    {
        $data = array(
            'userId'        => $this->session->userdata('userId'),
            'date_issue'    => timestamp_to_date(gmt_to_local(now(), 'UTC', FALSE), "Y-m-d"),
            'date_creation' => timestamp_to_date(gmt_to_local(now(), 'UTC', FALSE), "Y-m-d H:i:s"),
            'hidden'        => 1
        );

        if($loan_analysiId = $this->loan_analysis_model->save($data))
        {
            echo json_encode(array('result' => 1 , 'url' => base_url('loan_analysis/edit/'.$loan_analysiId)));
        }
    }

    public function preview($loan_analysiId)
    {
        $data               = array();
        $data['row']        = $this->loan_analysis_model->get_loan_data($loan_analysiId);
        $data['loan_items'] = $this->loan_analysis_deposit_items_model->get_by(array('loan_analysiId' => $loan_analysiId, 'hidden' => 0));
        echo json_encode(array('result'=> 1, 'view' => $this->load->view('loan_analysis/widgets/load_analysis_preview_view', $data, TRUE)));
    }

    public function receipt($loan_analysiId)
    {
        $data            = array();
        $data['row']     = $this->loan_analysis_model->get_loan_client_data($loan_analysiId);
        $data['content'] = 'loan_analysis/widgets/load_analysis_receipt_view';
        $this->load->view('include/template', $data);
    }

    public function edit($loan_analysiId)
    {
        $data = array(
            'row'        => $this->loan_analysis_model->get_by(array('loan_analysiId' => $loan_analysiId), TRUE),
            'clients'    => $this->com_clients->get_clients(),
            'loan_items' => $this->loan_analysis_deposit_items_model->get_by(array('loan_analysiId' => $loan_analysiId, 'hidden' => 0)),
            'content'    => 'loan_analysis/loan_analysis_edit_view',
        );

        $this->load->view('include/template', $data);
    }

    public function update($loan_analysiId)
    {
        $this->form_validation->set_rules('clientId','<strong>Cliente</strong>','is_natural_no_zero');

        if(isset($_POST['deposit_day']))
        {
            $i = 1;
            foreach ($_POST['deposit_day'] AS $key => $value)
            {
                $this->form_validation->set_rules("deposit_day[$key]","<strong>Depositos Fecha</strong> en la linea $i",'trim|required');
                $this->form_validation->set_rules("deposit_salary[$key]","<strong>Depositos Monto</strong> en la linea $i",'trim|required');

                $i++;
            }
        }

        if($this->form_validation->run($this) == FALSE)
        {
            echo json_encode(array('result' => 0, 'error' => display_error(validation_errors())));
        }
        else
        {
            $data = array(
                'clientId'               => $this->input->post('clientId_h', TRUE),
                'request_code'           => $this->input->post('request_code', TRUE),
                'date_issue'             => $this->input->post('date_issue', TRUE),
                'statusId'               => $this->input->post('statusId', TRUE),
                'notes'                  => $this->input->post('notes', TRUE),
                'total_deposit'          => $this->input->post('total_deposit', TRUE),
                'average_deposit'        => $this->input->post('average_deposit', TRUE),
                'average_salary'         => $this->input->post('average_salary', TRUE),
                'no_deposit_percent'     => $this->input->post('no_deposit_percent', TRUE),
                'payment_day_name'       => $this->input->post('payment_day_name', TRUE),
                'total_fact'             => $this->input->post('total_fact', TRUE),
                'average_salary_fat'     => $this->input->post('average_salary_fat', TRUE),
                'fact_percent'           => $this->input->post('fact_percent', TRUE),
                'fact_percent_total'     => $this->input->post('fact_percent_total', TRUE),
                'approved_fact'          => $this->input->post('approved_fact', TRUE),
                'monthly_fee_percent'    => $this->input->post('monthly_fee_percent', TRUE),
                'monthly_fee'            => $this->input->post('monthly_fee', TRUE),
                'quota_monthly'          => $this->input->post('quota_monthly', TRUE),
                'biweekly_quota'         => $this->input->post('biweekly_quota', TRUE),
                'weekly_quota'           => $this->input->post('weekly_quota', TRUE),
                'document'               => $this->input->post('document', TRUE),
                'first_name'             => $this->input->post('first_name', TRUE),
                'last_name'              => $this->input->post('last_name', TRUE),
                'company'                => $this->input->post('company', TRUE),
                'sector_company'         => $this->input->post('sector_company', TRUE),
                'apartament_of_house'    => $this->input->post('apartament_of_house', TRUE),
                'bankId'                 => $this->input->post('bankId', TRUE),
                'type_accountId'         => $this->input->post('type_accountId', TRUE),
                'bank_account'           => $this->input->post('bank_account', TRUE),
                'phone'                  => $this->input->post('phone', TRUE),
                'mobile'                 => $this->input->post('mobile', TRUE),
                'salary'                 => $this->input->post('salary', TRUE),
                'post_working'           => $this->input->post('post_working', TRUE),
                'department_working'     => $this->input->post('department_working', TRUE),
                'user_bank'              => $this->input->post('user_bank', TRUE),
                'password_bank'          => $this->input->post('password_bank', TRUE),
                'image'                  => $this->input->post('image', TRUE),
                'hidden'                 => 0
            );

            if($this->loan_analysis_model->save($data, $loan_analysiId))
            {
                if(isset($_POST['deposit_day']))
                {
                    foreach ($_POST['deposit_day'] AS $key => $value)
                    {
                        $data_items = array(
                          'loan_analysiId'  => $loan_analysiId,
                          'deposit_day'     => $_POST['deposit_day'][$key],
                          'deposit_salary'  => $this->strip_commas($_POST['deposit_salary'][$key]),
                        );

                        if($_POST['itemId'][$key] == 0)
                        {
                            $this->loan_analysis_deposit_items_model->save($data_items);
                        }
                        else
                        {
                            $this->loan_analysis_deposit_items_model->save($data_items, $_POST['itemId'][$key]);
                        }
                    }
                }

                echo json_encode(array('result' => 1, 'url' => base_url('loan_analysis')));
            }
        }
    }

    public function delete($loan_analysiId)
    {
        if($this->loan_analysis_model->delete($loan_analysiId) === TRUE)
        {
            echo json_encode(array('result' => 1));
        }
        else
        {
            echo json_encode(array('result' => 0));
        }
    }

    public function hide_items($itemId)
    {
        if($this->loan_analysis_deposit_items_model->delete($itemId) === TRUE)
        {
            echo json_encode(array('result' => 1));
        }
        else
        {
            echo json_encode(array('result' => 0));
        }
    }

    public function cancel_document($loan_analysiId)
    {
        if($this->loan_analysis_model->save(array('statusId' => 2), $loan_analysiId) == TRUE)
        {
            echo json_encode(array('result' => 1));
        }
        else
        {
            echo json_encode(array('result' => 0));
        }
    }

    public function send_by_mail($loan_analysiId, $clientId)
    {
        $data                   = array();
        $data['loan_analysiId'] = $loan_analysiId;
        $data['row_client']     = $this->clients_model->get_by(array('clientId' => $clientId, 'hidden' => 0), TRUE);;
        $data['row']            = $this->settings_model->get_by(array('companyId' => 1, 'hidden' => 0), TRUE);
        echo json_encode(array('result' => 1, 'view' => $this->load->view('loan_analysis/send_by_mail_view', $data, TRUE)));
    }

    public function send_mail($loan_analysiId)
    {
        $data                    = array();
        $data['row']             = $this->loan_analysis_model->get_loan_client_data($loan_analysiId);
        $data['file_name']       = 'Recibo de Prestamo';
        $data['pdf_view']        = $this->load->view('loan_analysis/widgets/receipt_view.php', $data, TRUE);
        $document_pdf            = $this->_Output($data);

        $data = array('to'       => $_POST['to'],
                      'to_name'  => $_POST['to_name'],
                      'cc'       => $_POST['cc'],
                      'title'    => $_POST['title'],
                      'body'     => $_POST['message'],
                      'pdf'      => $document_pdf,
        );

        if($this->sendmaild->send_mail($data, TRUE))
        {
            $this->loan_analysis_model->save(array('sent_mail' => 1), $loan_analysiId);
            echo json_encode(array('result' => 1));
        }
    }
}
