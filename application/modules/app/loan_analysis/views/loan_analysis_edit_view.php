<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="breadcrumb-holder">
                        <ol class="breadcrumb float-left">
                            <li class="breadcrumb-item"><a href="<?php echo base_url()?>dashboard">Inicio</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url()?>clients"> Análisis de Prestamos</a></li>
                            <li class="breadcrumb-item active title-h"> <span></span></li>
                        </ol>
                        <div class="col-md-3 float-right text-right">
                            <a href="javascript:void(0)" class="cancel" data-redirect="<?php echo base_url('loan_analysis')?>">
                                <i class="fa fa-times-circle-o bigfonts" aria-hidden="true" style="font-size: 30px!important; color: #f96262"></i>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <form id="form" action="<?php echo base_url('loan_analysis/update/'.$row->loan_analysiId); ?>" method="post">
                        <div class="card mb-3">
                            <div>
                                <input type="hidden"    name="clientId_h"           id="clientId-h"                     value="<?php echo $row->clientId;?>">
                                <input type="hidden"    name="total_deposit"        id="total_deposit-h"                value="<?php echo $row->total_deposit;?>">
                                <input type="hidden"    name="average_deposit"      id="average_deposit-h"              value="<?php echo $row->average_deposit;?>">
                                <input type="hidden"    name="average_salary"       id="average_salary"                 value="<?php echo $row->average_salary;?>">
                                <input type="hidden"    name="no_deposit_percent"   id="no-deposit-in-h"                value="<?php echo $row->no_deposit_percent;?>">
                                <input type="hidden"    name="payment_day_name"     id="payment_day_name-h"             value="<?php echo $row->payment_day_name;?>">
                                <input type="hidden"    name="total_fact"           id="total_fact-h"                   value="<?php echo $row->total_fact;?>">
                                <input type="hidden"    name="average_salary_fat"   id="average_salary_fat-h"           value="<?php echo $row->average_salary_fat;?>">
                                <input type="hidden"    name="fact_percent"         id="fact_percent-h"                 value="<?php echo $row->fact_percent;?>">
                                <input type="hidden"    name="fact_percent_total"   id="fact_percent_cal-h"             value="<?php echo $row->fact_percent_total;?>">
                                <input type="hidden"    name="approved_fact"        id="approved_fact-h"                value="<?php echo $row->approved_fact;?>">
                                <input type="hidden"    name="monthly_fee_percent"  id="monthly_fee_percent-h"          value="<?php echo $row->monthly_fee_percent;?>">
                                <input type="hidden"    name="monthly_fee"          id="monthly_fee-h"                  value="<?php echo $row->monthly_fee;?>">
                                <input type="hidden"    name="quota_monthly"        id="quota_monthly-h"                value="<?php echo $row->quota_monthly;?>">
                                <input type="hidden"    name="biweekly_quota"       id="biweekly_quota-h"               value="<?php echo $row->biweekly_quota;?>">
                                <input type="hidden"    name="weekly_quota"         id="weekly_quota-h"                 value="<?php echo $row->weekly_quota;?>">
                                <input type="hidden"    name="apartament_of_house"  id="apartament_of_house-h"          value="<?php echo $row->apartament_of_house;?>">
                                <input type="hidden"    name="bankId"               id="bankId-h"                       value="<?php echo $row->bankId;?>">
                                <input type="hidden"    name="type_accountId"       id="type_accountId-h"               value="<?php echo $row->type_accountId;?>">
                                <input type="hidden"    name="image"                id="image-h"                        value="<?php echo $row->image;?>">
                            </div>
                            <div class="card-body">
                                <div class="response"></div>
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="form-group row">
                                            <?php $ch_valid =(!empty($row->clientId))? 'disabled' : ''; ?>
                                            <div class="col-2 text-right"><label for="" class="mg-top control-label">Cliente:</label></div>
                                            <div class="col-md-6"><?php echo form_dropdown_data('clientId', $clients, set_value('clientId', $row->clientId),"id='clientId' ".$ch_valid." data-placeholder='Seleccione una Opción ' class='form-control chosen-select'");?></div>
                                            <div class="col-md-3 valid-message"></div>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group row">
                                            <div class="col-md-5 text-right"><label for="" class="mg-top control-label">Solicitud:</label></div>
                                            <div class="col-md-6 mg-top">
                                                <strong><span>#00<?php echo (!empty($row->request_code))? $row->request_code : $this->request_code+1;?></span></strong>
                                                <input type="hidden" name="request_code" value="<?php echo (!empty($row->request_code))? $row->request_code : $this->request_code+1;?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-5 text-right"><label for="" class="mg-top control-label">Fecha Emisión:</label></div>
                                            <div class="col-md-6"><input type="text" class="form-control date" readonly name="date_issue" id="date-issue" value="<?php echo $row->date_issue; ?>"></div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-5 text-right"><label for="" class="mg-top control-label">Estado:</label></div>
                                            <div class="col-md-6"><?php echo form_dropdown('statusId', $this->status_analysis, set_value('statusId', 3),"id='statusId' data-placeholder='Seleccione una Opción' class='form-control chosen-select'");?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row text-right">
                                    <div class="col-md-2">
                                        <?php $image = ($row->image != "" || $row->image != null) ? "assets/storage/avatars/".$row->image : "assets/images/avatars/user.png"?>
                                        <img id="add-image" class="card-img" src="<?php echo base_url().$image;?>" alt="">
                                    </div>
                                </div>
                                <hr class="mg-b-30">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row form-group">
                                            <label class="col-md-4 control-label text-right mg-top ">Cedula de Identidad:</label>
                                            <div class="col-md-7">
                                                <input type="text" name="document" class="form-control cedula" id="document" readonly value="<?php echo $row->document;?>">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-4 control-label text-right mg-top">Nombre:</label>
                                            <div class="col-md-7">
                                                <input type="text" name="first_name" class="form-control" id="first_name" readonly value="<?php echo $row->first_name;?>">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-4 control-label text-right mg-top">Apellido:</label>
                                            <div class="col-md-7">
                                                <input type="text" name="last_name" class="form-control" id="last_name" readonly value="<?php echo $row->last_name;?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 control-label text-right mg-top">Empresa:</label>
                                            <div class="col-md-7"><input type="text" name="company" class="form-control" id="company" readonly value="<?php echo $row->company;?>"></div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-4 control-label text-right mg-top">Sector:</label>
                                            <div class="col-md-7"><input type="text" name="sector_company" class="form-control" id="sector_company" readonly value="<?php echo $row->sector_company;?>"></div>
                                        </div>
                                        <?php $chk_checked_1 = ($row->apartament_of_house == 1) ? 'checked' : '';?>
                                        <?php $chk_checked_2 = ($row->apartament_of_house == 2) ? 'checked' : '';?>
                                        <div class="row form-group">
                                            <label class="col-md-4 control-label text-right mg-top">Es una casa?:</label>
                                            <div class="col-md-1">
                                                <label class="radio">
                                                    <input type="radio" class="" <?php echo $chk_checked_1; ?> name="apartament_of_house" value="1" id="apartament_of_house_1" disabled>
                                                    <span class="check"></span>
                                                </label>
                                            </div>
                                            <label class="col-md-5 control-label text-right mg-top">Es un apartamento?:</label>
                                            <div class="col-md-1">
                                                <label class="radio">
                                                    <input type="radio" class="" <?php echo $chk_checked_2; ?> name="apartament_of_house" value="2" id="apartament_of_house_2" disabled>
                                                    <span class="check"></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-4 control-label text-right mg-top">Banco:</label>
                                            <div class="col-md-7"><?php echo form_dropdown('bankId', $this->bank, set_value('banId', $row->bankId),"id='bankId' data-placeholder='-seleccione una opción-' disabled class='form-control select2'");?></div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-4 control-label text-right mg-top">Tipo de Cuenta:</label>
                                            <div class="col-md-7"><?php echo form_dropdown('type_accountId', $this->type, set_value('type_accountId', $row->type_accountId),"id='type_accountId' disabled data-placeholder='-seleccione una opción-' class='form-control select2'");?></div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-4 control-label text-right mg-top">Cuenta:</label>
                                            <div class="col-md-7">
                                                <input type="text" name="bank_account" class="form-control" id="bank_account" readonly value="<?php echo $row->bank_account;?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row form-group">
                                            <label class="col-md-3 control-label text-right mg-top">Telefono:</label>
                                            <div class="col-md-5">
                                                <input type="text" name="phone" class="form-control phone" id="phone" readonly value="<?php echo $row->phone;?>">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-3 control-label text-right mg-top ">Celular:</label>
                                            <div class="col-md-5">
                                                <input type="text" name="mobile" class="form-control phone" id="mobile" readonly value="<?php echo $row->mobile;?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 control-label text-right mg-top">Salario:</label>
                                            <div class="col-md-4"><input type="text" name="salary" class="form-control numeric" id="salary" readonly value="<?php echo $row->salary;?>"></div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-3 control-label text-right mg-top">Puesto:</label>
                                            <div class="col-md-5"><input type="text" name="post_working" class="form-control" id="post_working" readonly value="<?php echo $row->post_working;?>"></div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-3 control-label text-right mg-top">Departamento:</label>
                                            <div class="col-md-5"><input type="text" name="department_working" class="form-control" id="department_working" readonly value="<?php echo $row->department_working;?>"></div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-3 control-label text-right mg-top">Usuario:</label>
                                            <div class="col-md-5">
                                                <input type="text" name="user_bank" class="form-control" id="user_bank" readonly value="<?php echo $row->user_bank;?>">
                                            </div>
                                        </div>
                                        <div class="row form-group">
                                            <label class="col-md-3 control-label text-right mg-top">Clave:</label>
                                            <div class="col-md-5">
                                                <input type="text" name="password_bank" class="form-control"  id="password_bank" readonly value="<?php echo $row->password_bank;?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr class="mg-b-30">
                                <div class="row">
                                    <div class="col-md-5"><h4>Depósitos en los próximos 3 Meses</h4></div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="table-responsive">
                                            <table id="deposit" class="table">
                                                <thead>
                                                    <tr>
                                                        <th width="5%">NO</th>
                                                        <th width="40%">Fecha</th>
                                                        <th width="30%">Monto</th>
                                                        <th width="2%"></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php if(!empty($loan_items)):?>
                                                    <?php $count = 1; foreach($loan_items AS $key => $item):?>
                                                        <tr class="row-item inline">
                                                            <input type="hidden" name="itemId[]" value="<?php echo $item->itemId; ?>">
                                                            <td class="number-count"><?php echo $count++;?></td>
                                                            <td class="width-50 form-group"><input type="text" name="deposit_day[]" value="<?php echo $item->deposit_day ?>" class="form-control date deposit-day" id="deposit_day" readonly></td>
                                                            <td class="width-15"><input type="text" name="deposit_salary[]" value="<?php echo number_format($item->deposit_salary,2); ?>" class="form-control numeric-decimal deposit-salary currency" id="deposit_salary"></td>
                                                            <td class="master-closet"><button type="button" class="close trigger_remove_row mg-8" data-id="<?php echo $item->itemId; ?>"><i class="fa fa-window-close-o close close-icon-windows"></i></button></td>
                                                        </tr>
                                                    <?php endforeach;?>
                                                <?php else:?>
                                                        <tr class="row-item inline">
                                                            <input type="hidden" name="itemId[]" value="0">
                                                            <td class="number-count">1</td>
                                                            <td class="width-50 form-group"><input type="text" name="deposit_day[]"  class="form-control date deposit-day" id="deposit_day" readonly></td>
                                                            <td class="width-15"><input type="text" name="deposit_salary[]" value="0.00" class="form-control numeric-decimal deposit-salary currency" id="deposit_salary"></td>
                                                            <td class="master-closet"><button type="button" class="close trigger_remove_row mg-8" data-id="0"><i class="fa fa-window-close-o close close-icon-windows"></i></button></td>
                                                        </tr>
                                                <?php endif;?>
                                                <tr class="last_row"><td colspan="8"><button type="button" class="btn btn-info btn-rounded trigger_add_row border-button btn-sm"><i class="fa fa-plus"></i> <span>Depósitos</span></button></td></tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="notes-textarea">Notas</label>
                                            <textarea name="notes" class="form-control" id="notes-textarea" rows="3"><?php echo $row->notes; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mg-t-27">
                                        <div class="card card-border">
                                            <h6 class="card-header">Depósitos</h6>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-4 text-right"><label for="" class="mg-top control-label">Total Depósitos:</label></div>
                                                    <div class="col-md-8 mg-top text-right">
                                                       <strong> <span id="total_deposit">$0</span></strong>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 text-right"><label for="" class="mg-top control-label curson-pointer" id="deposit-click">NO. Depósitos:</label></div>
                                                    <div class="col-md-8 mg-top text-right">
                                                        <input type="text" name="no_deposit" id="no-deposit-in" class="numeric-decimal hidden" value="3.5">
                                                        <span id="no_deposit_span" style="color: blue;">3.5</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 text-right"><label for="" class="mg-top control-label">Depósitos Promedio:</label></div>
                                                    <div class="col-md-8 mg-top text-right">
                                                        <strong><span id="average_deposit_span" style="color: red;">$0</span></strong>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 text-right"><label for="" class="mg-top control-label">Dias de Cobro:</label></div>
                                                    <div class="col-md-8 mg-top text-right">
                                                        <strong><span id="payment_day_name" style="color: blue;"><?php echo (!empty($row->payment_day_name))? $row->payment_day_name : 'N/A';?></span></strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card card-border">
                                            <h6 class="card-header">Factibilidad</h6>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-4 text-right"><label for="" class="mg-top control-label">Totales:</label></div>
                                                    <div class="col-md-8 mg-top text-right">
                                                       <strong> <span id="totales">$0</span></strong>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 text-right"><label for="" class="mg-top control-label">Sueldo Promedio:</label></div>
                                                    <div class="col-md-8 mg-top text-right">
                                                        <span id="average_salary_fat">$0</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 text-right"><label for="" class="mg-top control-label curson-pointer" id="fact_percent_click">50%:</label></div>
                                                    <div class="col-md-8 mg-top text-right">
                                                        <input type="text" name="fact_percent" id="fact_percent" class="numeric-decimal hidden" value="50">
                                                        <strong><span id="fact_percent_span" style="color: red;">$0</span></strong>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 text-right"><label for="" class="mg-top control-label">Aprobado:</label></div>
                                                    <div class="col-md-8 mg-top text-right">
                                                        <strong><span id="approved_fact" style="color:green;">$0</span></strong>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 text-right"><label for="" class="mg-top control-label curson-pointer" id="monthly_fee_click">Cuota 45% Mensual:</label></div>
                                                    <div class="col-md-8 mg-top text-right">
                                                        <input type="text" name="monthly_fee" id="monthly_fee" class="numeric-decimal hidden" value="45">
                                                        <strong> <span id="monthly_fee_span" style="color: red;">$0</span></strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card card-border">
                                            <h6 class="card-header">Cuotas</h6>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-4 text-right"><label for="" class="mg-top control-label">Mensual:</label></div>
                                                    <div class="col-md-8 mg-top text-right">
                                                        <strong><span id="quota_monthly" style="color: green;">$0</span></strong>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 text-right"><label for="" class="mg-top control-label">Quincenal:</label></div>
                                                    <div class="col-md-8 mg-top text-right">
                                                        <strong> <span id="biweekly_quota" style="color: green;">$0</span></strong>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 text-right"><label for="" class="mg-top control-label">Semanal:</label></div>
                                                    <div class="col-md-8 mg-top text-right">
                                                        <strong> <span id="weekly_quota" style="color: green;">$0</span></strong>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr class="mg-b-30">
                            <div class="col-md-12 text-right p-b-10">
                                <button id="button-submit" type="submit" class="btn btn-primary ladda-button" data-style="expand-right">
                                    <span class="ladda-label">Agregar</span>
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- END container-fluid -->
    </div>
    <!-- END content -->
</div>
<!-- END content-page -->

<table class="hidden">
    <tbody id="row-table-hidden">
        <tr class="row-item inline">
            <input type="hidden" name="itemId[]" value="0">
            <td class="number-count"></td>
            <td><input type="text" name="deposit_day[]" class="form-control date deposit-day" id="deposit_day" readonly></td>
            <th><input type="text" name="deposit_salary[]" value="0.00" class="form-control numeric-decimal deposit-salary currency" id="deposit_salary"></th>
            <td class="master-closet"><button type="button" class="close trigger_remove_row mg-8" data-id="0"><i class="fa fa-window-close-o close close-icon-windows"></i></button></td>
        </tr>
    </tbody>
</table>