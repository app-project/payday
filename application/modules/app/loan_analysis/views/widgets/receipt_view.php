<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Recibo</title>
</head>
<body>
    <table class="table2excel" width="100%">
        <tbody>
        <tr>
            <td colspan="2" style="width: 50%; height: 94px; text-align: center;">
                <img src="<?php echo base_url();?>assets/images/logo3.png" width="90px" height="90px" alt="">
            </td>
        </tr>
        </tbody>
    </table>
    <table class="table2excel" width="100%">
        <tbody>
        <tr>
            <td style="width: 100%; height: 94px; text-align: center;">
                <h2>RECIBO DE PRESTAMO</h2>
                <strong>Fecha:</strong> <?php echo date('d-m-Y');?>
            </td>
        </tr>
        </tbody>
    </table>
    <hr>
    <table class="table2excel" style="width: 100%">
        <tbody>
        <tr style="height: 50px;">
            <td><h5 class="text-right">YO:&nbsp;</h5></td>
            <td style="padding-left: 15px;"> <p style="margin-bottom: 5px;"> <?php echo strtoupper($row->full_name);?>.</p></td>
        </tr>
        <tr style="height: 50px;">
            <td><h5 class="text-right">RECIBI:&nbsp;</h5></td>
            <td style="padding-left: 15px;"> <p style="margin-bottom: 5px;"> RD$<?php $amount = floatval($row->requested_amount); echo number_format($amount,2);?> (<?php echo NumberToLetter::convertir($amount,'PESO','CENTAVOS',TRUE)?>).</p></td>
        </tr>
        <tr style="height: 50px;">
            <td><h5 class="text-right">DE:</h5></td>
            <td style="padding-left: 15px;"> <p style="margin-bottom: 5px;"> PRESTAME DINERO S.R.L.</p></td>
        </tr>
        <tr style="height: 50px;">
            <td><h5 class="text-right">MOTIVO:</h5></td>
            <td style="padding-left: 15px;"><p style="margin-bottom: 5px;"><?php echo strtoupper($row->notes);?>.</p></td>
        </tr>
        </tbody>
    </table>
    <hr>
    <table class="table2excel" style="width: 100%; margin-left: auto; margin-right: auto;" cellspacing="10">
        <tbody>
        <tr style="height: 94px;">
            <td style="width: 50%; height: 94px; text-align: center;">
                <p><span style="text-align: center;"><br>_____________________________________</span><br style="text-align: center;"><span style="text-align: center;">FIRMA<br></span></p>
            </td>
        </tr>
        </tbody>
    </table>
</body>
</html>