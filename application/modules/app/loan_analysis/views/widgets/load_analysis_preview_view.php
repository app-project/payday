<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-newspaper-o"></i> <?php echo '#00'.$row->request_code.'-'.$row->first_name.' '.$row->last_name;?></h5>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    </div>
    <div class="modal-body">
        <div class="card-body">
            <div class="row">
                <div class="col-md-2 solid-black">
                    <?php $image = ($row->image != "" || $row->image != null) ? "assets/storage/avatars/".$row->image : "assets/images/avatars/user.png"?>
                    <img src="<?php echo base_url().$image;?>" width="100px" height="100px" class="rounded float-left" alt="...">
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <label for="" class="control-label col-md-3 text-right mg-top">Cedula:</label>
                        <div class="col-md-6 mg-top"><span><?php echo $row->document;?></span></div>
                    </div>
                    <div class="row">
                        <label for="" class="control-label  col-md-3 text-right mg-top">Nombre:</label>
                        <div class="col-md-6 mg-top"><span><?php echo $row->first_name.' '.$row->last_name;?></span></div>
                    </div>
                    <div class="row">
                        <label for="" class="control-label  col-md-3 text-right mg-top">Telefono:</label>
                        <div class="col-md-6 mg-top"><span><?php echo $row->phone;?></span></div>
                    </div>
                    <div class="row">
                        <label for="" class="control-label  col-md-3 text-right mg-top">Celular:</label>
                        <div class="col-md-6 mg-top"><span><?php echo $row->mobile;?></span></div>
                    </div>
                    <div class="row">
                        <label for="" class="control-label  col-md-3 text-right mg-top">Banco:</label>
                        <div class="col-md-9 mg-top"><span><?php echo $row->bank_name;?></span></div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <label for="" class="control-label  col-md-5  text-right mg-top">Salario:</label>
                        <div class="col-md-6 mg-top"><span>$<?php echo number_format($row->salary, 2);?></span></div>
                    </div>

                    <div class="row">
                        <label for="" class="control-label  col-md-5  text-right mg-top">Tipo de Cuenta:</label>
                        <div class="col-md-6 mg-top"><span><?php echo $row->type_account_name;?></span></div>
                    </div>
                    <div class="row">
                        <label for="" class="control-label  col-md-5  text-right mg-top">Cuenta Bancaria:</label>
                        <div class="col-md-6 mg-top"><span><?php echo $row->bank_account;?></span></div>
                    </div>
                    <div class="row">
                        <label for="" class="control-label  col-md-5  text-right mg-top">Estado:</label>
                        <?php $chk_load = '';
                        switch ($row->statusId):
                            case 1:
                                $chk_load = '<div class="col-md-4 mg-top label p-t-6" style="background-color:#FFC000;">DESEMBOLSADO</div>';
                                break;
                            case 2:
                                $chk_load = '<div class="col-md-4 mg-top label p-t-6" style="background-color:#FF0000;">CANCELADO</div>';
                                break;
                            case 3:
                                $chk_load = '<div class="col-md-4 mg-top label p-t-6" style="background-color:#9e9c9c;">ARCHIVADO</div>';
                                break;
                        endswitch;
                        echo $chk_load; ?>
                    </div>
                </div>
            </div>
        </div>
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-deposit" role="tab" aria-controls="nav-home" aria-selected="true"><i class="fa fa-delicious"></i> Depósitos</a>
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-deposit" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="card mb-3">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table id="deposit" class="table" width="100%">
                                        <thead>
                                        <tr>
                                            <th width="20%">#NO</th>
                                            <th width="30%">Fecha</th>
                                            <th width="10%">Monto</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(!empty($loan_items)):?>
                                            <?php $count = 1; foreach($loan_items AS $key => $item):?>
                                                <tr>
                                                    <td><?php echo $count++;?></td>
                                                    <td><?php echo $item->deposit_day; ?></td>
                                                    <td>$<?php echo number_format($item->deposit_salary,2); ?></td>
                                                </tr>
                                            <?php endforeach;?>
                                        <?php else:?>
                                            <tr class="row-item inline"><td class="number-count">No hay Registros..</td></tr>
                                        <?php endif;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <div class="card card-border">
                    <h6 class="card-header">Cuotas</h6>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4 text-right"><label for="" class="mg-top control-label">Total:</label></div>
                            <div class="col-md-7 mg-top text-right"><span>$<?php echo number_format($row->total_deposit,2);?></span></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 text-right"><label for="" class="mg-top control-label">Mensual:</label></div>
                            <div class="col-md-7 mg-top text-right"><span>$<?php echo number_format($row->quota_monthly,2);?></span></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 text-right"><label for="" class="mg-top control-label">Quincenal:</label></div>
                            <div class="col-md-7 mg-top text-right"><span>$<?php echo number_format($row->biweekly_quota,2);?></span></div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 text-right"><label for="" class="mg-top control-label">Semanal:</label></div>
                            <div class="col-md-7 mg-top text-right"><span>$<?php echo number_format($row->weekly_quota,2);?></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>