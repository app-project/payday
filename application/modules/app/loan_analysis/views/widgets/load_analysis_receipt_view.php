<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="breadcrumb-holder">
                        <h1 class="main-title float-left"> Recibo de Prestamo </h1>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="<?php echo base_url()?>dashboard">Inicio</a></li>
                            <li class="breadcrumb-item active"> Recibo de Prestamo</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10 col-xl-10 mg-l-80" id="print_content">
                    <div class="card mb-3">
                        <div class="card-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table2excel" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td colspan="2" style="width: 50%; height: 94px; text-align: center;">
                                                        <img src="<?php echo base_url();?>assets/images/logo3.png" width="90px" height="90px" alt="">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="table2excel" width="100%">
                                           <tbody>
                                               <tr>
                                                   <td style="width: 100%; height: 94px; text-align: center;">
                                                       <h2>RECIBO DE PRESTAMO</h2>
                                                       <strong>Fecha:</strong> <?php echo date('d-m-Y');?>
                                                   </td>
                                               </tr>
                                           </tbody>
                                        </table>
                                        <hr>
                                        <table class="table2excel" style="width: 100%">
                                           <tbody>
                                               <tr style="height: 50px;">
                                                   <td><h5 class="text-right">YO:&nbsp;</h5></td>
                                                   <td style="padding-left: 15px;"> <p style="margin-bottom: 5px;"> <?php echo strtoupper($row->full_name);?>.</p></td>
                                               </tr>
                                               <tr style="height: 50px;">
                                                   <td><h5 class="text-right">RECIBI:&nbsp;</h5></td>
                                                   <td style="padding-left: 15px;"> <p style="margin-bottom: 5px;"> RD$<?php $amount = floatval($row->requested_amount); echo number_format($amount,2);?> (<?php echo NumberToLetter::convertir($amount,'PESO','CENTAVOS',TRUE)?>).</p></td>
                                               </tr>
                                               <tr style="height: 50px;">
                                                   <td><h5 class="text-right">DE:</h5></td>
                                                   <td style="padding-left: 15px;"> <p style="margin-bottom: 5px;"> PRESTAME DINERO S.R.L.</p></td>
                                               </tr>
                                               <tr style="height: 50px;">
                                                   <td><h5 class="text-right">MOTIVO:</h5></td>
                                                   <td style="padding-left: 15px;"><p style="margin-bottom: 5px;"><?php echo strtoupper($row->notes);?>.</p></td>
                                               </tr>
                                           </tbody>
                                        </table>
                                        <hr>
                                        <table class="table2excel" style="width: 100%; margin-left: auto; margin-right: auto;" cellspacing="10">
                                            <tbody>
                                                <tr style="height: 94px;">
                                                    <td style="width: 50%; height: 94px; text-align: center;">
                                                        <p><span style="text-align: center;"><br>_____________________________________</span><br style="text-align: center;"><span style="text-align: center;">FIRMA<br></span></p>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end card body -->
                    </div>
                    <!-- end card -->
                </div>
                <div class="col-md-1 print-options">
                    <ul>
                        <li><button type="button" title="Original Cliente" class="btn btn-primary trigger_print"><i class="fa fa-print"></i></button></li>
                        <li>
                            <div class="dropdown">
                                <button class="btn" type="button" data-toggle="dropdown"><i class="fa fa-cloud-download"></i></button>
                                <div class="dropdown-menu">
                                    <h6 class="dropdown-header">Exportar en</h6>
                                    <a href="javascript:void(0)" class="dropdown-item export_word"><i class="fa fa-file-word-o"></i> Word</a>
                                    <a href="javascript:void(0)" class="dropdown-item export_excel"><i class="fa fa-file-excel-o"></i> Excel</a>
                                </div>
                            </div>
                        </li>
                        <li><button type="button" class="btn modal_trigger" role="menuitem" tabindex="-1" data-toggle="modal" data-target="#send-email" data-url="send_by_mail"><i class="fa fa-envelope"></i></button></li>
                    </ul>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- END container-fluid -->
    </div>
    <!-- END content -->
</div>

<!-- END content-page -->