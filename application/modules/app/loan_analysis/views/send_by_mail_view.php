<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-envelope-o"></i> Enviar Correo / <?php echo $row_client->first_name.' '.$row_client->last_name;?></h5>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    </div>
    <form action="<?php echo base_url()?>loan_analysis/send_mail/<?php echo $loan_analysiId;?>" id="form-mail" method="post">
        <div class="modal-body">
             <div class="modal-response"></div>
            <div class="row">
                <input type="hidden" name="from" id="from" value="<?php echo $row->email;?>">
                <input type="hidden" name="by" id="by" value="<?php echo $row->company;?>">
                <input type="hidden" name="to_name" id="to_name" value="<?php echo $row_client->first_name;?>">
                <div class="col-md-12">
                    <div class="row form-group">
                        <div class="col-md-3 text-right"><label for="of">De:</label></div>
                        <div class="col-md-7"><label class="color-mail" for=""><?php echo $row->company.' ('.$row->email.')';?></label></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3 text-right"><label class="mg-t-5" for="for">Para:</label></div>
                        <div class="col-md-7"><input type="text" name="to" value="<?php echo $row_client->email;?>" id="to" class="form-control"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3 text-right"><label class="mg-t-5" for="cc">Cc:</label></div>
                        <div class="col-md-7"><input type="text" name="cc" id="cc" class="form-control"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3 text-right"><label class="mg-t-5" for="affair">Asunto:</label></div>
                        <div class="col-md-7"><input type="text" name="title" id="title" class="form-control"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3 text-right"><label class="mg-t-5" for="message">Mensaje:</label></div>
                        <div class="col-md-7">
                            <textarea type="text" name="message" id="message" rows="8" class="form-control">
Estimado (a) <?php echo $row_client->first_name;?>
<br>
adjunto el recibo de prestamo cualquiero pregunta puede comunicarse por via de mi correo
<br>
<p><?php echo $row->email;?></p>
<br>
Prestame Dinero SRL.
                            </textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button id="button-submit-email" type="submit" class="btn btn-primary ladda-button" data-style="expand-right">
                <span class="ladda-label">Enviar</span>
            </button>
        </div>
    </form>
</div>