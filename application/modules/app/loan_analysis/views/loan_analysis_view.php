<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="breadcrumb-holder">
                        <h1 class="main-title float-left"> Análisis de Prestamos </h1>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="<?php echo base_url()?>dashboard">Inicio</a></li>
                            <li class="breadcrumb-item active"> Análisis de Prestamos</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card mb-3">
                        <div class="card-header">
                            <span class="pull-right"><a class="btn btn-primary btn-sm redirect ladda-button"  data-style="expand-right" href="javascript:void(0)" data-url="<?php echo base_url('loan_analysis/add');?>"><i class="fa fa-user-plus"></i> Nuevo</a></span>
                        </div>
                        <!-- end card-header -->
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="list" class="table table-bordered table-hover display dataTable no-footer" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Código</th>
                                            <th><i class="mail-f-s-20 fa fa-envelope-o"></i></th>
                                            <th>Cliente</th>
                                            <th>Fecha Creación</th>
                                            <th>Total Deposito</th>
                                            <th>Cuota Mensual</th>
                                            <th>Cuota Quincenal</th>
                                            <th>Cuota Semanal</th>
                                            <th>Estado</th>
                                            <th>Acci&oacute;n</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end card-body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- END container-fluid -->
    </div>
    <!-- END content -->
</div>
<!-- END content-page -->

<div id="loand_preview" class="modal fade custom-modal" tabindex="-1" role="dialog" aria-labelledby="modal_user" aria-hidden="true">
    <div class="modal-dialog"></div>
</div>

<div id="sent_mail" class="modal fade custom-modal" tabindex="-1" role="dialog" aria-labelledby="modal_user" aria-hidden="true">
    <div class="modal-dialog"></div>
</div>