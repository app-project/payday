<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Compulsive_payments_items_model extends MY_Model
{
    public function __construct()
    {
        $this->table_name   = "ai_compulsive_payments_items";
        $this->view_name    = "ai_compulsive_payments_items";
        $this->primary_key  = "compulsive_itemId";
        $this->order_by     = "compulsive_itemId";
    }
}