<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Compulsive_payments_model extends MY_Model
{
    public function __construct()
    {
        $this->table_name   = "ai_loan_analysis";
        $this->view_name    = "ai_compulsive_payments_view";
        $this->primary_key  = "";
        $this->order_by     = "";
    }

    public function get_load_analysis_data($loan_analysiId = FALSE, $options = FALSE, $status = FALSE)
    {
        $loan_analys  = ($loan_analysiId != FALSE)? "AND a.loan_analysiId = $loan_analysiId" : "";
        $object       = ($loan_analysiId != FALSE)? 'row': 'result';
        $options      = (in_array($options, array(1,2,3)))? "AND b.payment_dayId = $options": "AND b.payment_dayId != 0";
        $status      = ($status != FALSE)? "": "AND a.statusId = 1";

        $result = $this->db->query("SELECT a.loan_analysiId,CONCAT(b.first_name,' ',b.last_name) AS full_name,b.requested_amount,a.clientId,
                                    a.statusId,
                                    a.request_code,
                                    b.image,
                                    b.email,b.email_work,b.user_bank,b.password_bank,b.phone,b.mobile,b.name_facebook,b.link_facebook,b.payment_dayId,
                                    b.day_work,
                                    CASE 
                                        WHEN b.payment_dayId = 1 THEN a.quota_monthly
                                        WHEN b.payment_dayId = 2 THEN a.biweekly_quota
                                        WHEN b.payment_dayId = 3 THEN a.weekly_quota
                                    END AS payment_amount,
                                    c.`name` AS payment_name,
                                    e.name AS bank
                                    FROM ai_loan_analysis  AS a
                                         LEFT JOIN ai_clients AS b ON a.clientId = b.clientId 
                                    	 LEFT JOIN ai_payment_days AS c ON b.payment_dayId = c.payment_dayId
                                    	 LEFT JOIN ai_institution_banks AS e ON b.bankId = e.bankId
                                    WHERE b.hidden = 0 $status $loan_analys $options")->$object();

        return $result;
    }

    public function compulsive_count()
    {
        return $this->db->query("SELECT COUNT(*) AS `numrows` FROM ai_loan_analysis WHERE `hidden` = 0 AND statusId IN (1,4)")->row()->numrows;
    }
}