<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Compulsive_payments extends MY_Controller
{
    public $columns;
    public $payment_day;
    public $title;

    public function __construct()
    {
        parent::__construct();

        $this->title       = 'Cobros Compulsivos';
        $this->securities->is_logged_in($this->session->userdata('is_logged_in'));

        //Load Model
        $this->load->model('compulsive_payments/compulsive_payments_model');
        $this->load->model('compulsive_payments/compulsive_payments_items_model');
        $this->load->model('loan_analysis/loan_analysis_model');
        $this->load->model('clients/payment_days_model');
        $this->load->model('clients/clients_model');

        $this->columns          = "loan_analysiId,full_name,payment_day_name,document,requested_amount,payment_day_total,bank,status,class,statusId";
    }

    public function index()
    {
        $data['content']        = 'compulsive_payments/compulsive_payments_view';
        $this->load->view('include/template', $data);
    }

    public function datatable()
    {
        $all_clients             = $this->compulsive_payments_model->datatable($this->columns, FALSE, TRUE);
        echo json_encode(array('data' => $all_clients));
    }

    public function clients_compulsive()
    {
        $data                    = array();
        $data['result']          = $this->compulsive_items();
        $data['content']         = 'compulsive_payments/compulsive_payments_view_view';
        $this->load->view('include/template', $data);
    }

    public function preview($loan_analysiId)
    {
        $data                     = array();
        $data['row']              = $this->compulsive_payments_model->get_load_analysis_data($loan_analysiId, FALSE, TRUE);
        $data['compulsive_items'] = $this->compulsive_payments_items_model->get_by(array('loan_analysiId' => $loan_analysiId, 'hidden' => 0));
        echo json_encode(array('result' => 1, 'view' => $this->load->view('compulsive_payments/widgets/compulsive_payments_preview_view', $data, TRUE)));
    }

    public function payments_purchase($loan_analysiId)
    {
        $data                     = array();
        $data['row_client']       = $this->compulsive_payments_model->get_load_analysis_data($loan_analysiId);
        $data['compulsive_items'] = $this->compulsive_payments_items_model->get_by(array('loan_analysiId' => $loan_analysiId, 'hidden' => 0));
        echo json_encode(array('result' => 1, 'view' => $this->load->view('compulsive_payments/widgets/compulsive_payments_purchase_view', $data, TRUE)));
    }

    public function payment_insert()
    {
        if(isset($_POST['date']))
        {
            foreach($_POST['date'] AS $key => $value)
            {
                $data = array(
                    'clientId'       => $_POST['clientId'][$key],
                    'loan_analysiId' => $_POST['loan_analysiId'][$key],
                    'date'           => $_POST['date'][$key],
                    'capital'        => $this->strip_commas($_POST['capital'][$key]),
                    'interest'       => $this->strip_commas($_POST['interest'][$key]),
                    'late_payment'   => $this->strip_commas($_POST['late_payment'][$key]),
                );

                if($_POST['compulsive_itemId'][$key] == 0)
                {
                    $this->compulsive_payments_items_model->save($data);
                }
                else
                {
                    $this->compulsive_payments_items_model->save($data, $_POST['compulsive_itemId'][$key]);
                }
            }

            echo json_encode(array('result' => 1, 'view' => $this->compulsive_items()));
        }
    }

    public function hide_items($compulsive_itemId)
    {
        if($this->compulsive_payments_items_model->delete($compulsive_itemId) === TRUE)
        {
            echo json_encode(array('result' => 1));
        }
        else
        {
            echo json_encode(array('result' => 0));
        }
    }

    public function update_status_compulsive($compulsive_itemId)
    {
        if($this->loan_analysis_model->update(array('statusId' => 4), $compulsive_itemId) === TRUE)
        {
            echo json_encode(array('result' => 1));
        }
        else
        {
            echo json_encode(array('result' => 0));
        }
    }

    public function compulsive_items()
    {
        $data['data_result'] = $this->compulsive_payments_model->get_load_analysis_data();
      return $this->load->view('compulsive_payments/widgets/compulsive_payments_items_view', $data, TRUE);
    }

    public function compulsive_status($option)
    {
        $data['data_result'] = $this->compulsive_payments_model->get_load_analysis_data(FALSE, $option, FALSE);
        echo json_encode(array('result' => 1, 'view' => $this->load->view('compulsive_payments/widgets/compulsive_payments_items_view', $data, TRUE)));
    }
}
