<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="breadcrumb-holder">
                        <ol class="breadcrumb float-left">
                            <li class="breadcrumb-item"><a href="<?php echo base_url()?>dashboard">Inicio</a></li>
                            <li class="breadcrumb-item"><a href="<?php echo base_url()?>clients">Cobros Compulsivos</a></li>
                            <li class="breadcrumb-item active">Historial <span></span></li>
                        </ol>
                        <div class="col-md-3 float-right text-right">
                            <a href="javascript:void(0)" class="cancel" data-redirect="<?php echo base_url('compulsive_payments')?>">
                                <i class="fa fa-times-circle-o bigfonts" aria-hidden="true" style="font-size: 30px!important; color: #f96262"></i>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card mb-3">
                        <div class="card-body">
                            <!-- end card-header-->
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-4 text-right"><label for="" class="mg-top control-label">Filtrar Por:</label></div>
                                            <div class="col-md-4">
                                                <div class="row">
                                                    <label for="option-filter" class="col-md-5 control-label text-right mg-top">Mensual:</label>
                                                    <div class="col-md-1">
                                                        <label class="radio">
                                                            <input type="radio" class="" name="filter" value="1" id="option-filter">
                                                            <span class="check"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label for="option-filter" class="col-md-5 control-label text-right mg-top">Quincenal:</label>
                                                    <div class="col-md-1">
                                                        <label class="radio">
                                                            <input type="radio" class="" name="filter" value="2" id="option-filter">
                                                            <span class="check"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="row">
                                                    <label for="option-filter" class="col-md-7 control-label text-right mg-top">Semanal:</label>
                                                    <div class="col-md-1">
                                                        <label class="radio">
                                                            <input type="radio" class="" name="filter" value="3" id="option-filter">
                                                            <span class="check"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <label for="option-filter" class="col-md-7 control-label text-right mg-top">Mostrar Todos:</label>
                                                    <div class="col-md-1">
                                                        <label class="radio">
                                                            <input type="radio" class="" name="filter" checked value="4" id="option-filter">
                                                            <span class="check"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div id="container-payments">
                                    <?php echo $result;?>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="card border-card"><div class="card-header"><h6><i class="fa fa-fw fa-user-secret"></i> Legenda</h6></div>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row form-group">
                                                            <div class="col-md-2 text-right">Label:</div>
                                                            <div class="col-md-3 badge badge-success" style="color: white; padding-top: 6px;">No Cobrar</div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-md-2 text-right">Label:</div>
                                                            <div class="col-md-3 badge badge-warning" style="color: white; padding-top: 6px;">Acercandose</div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-md-2 text-right">Label:</div>
                                                            <div class="col-md-3 badge badge-danger" style="color: white; padding-top: 6px;">Cobrar</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end card-body -->
                        </div>
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- END container-fluid -->
    </div>
    <!-- END content -->
</div>
<!-- END content-page -->

<div id="compulsive_payments_view" class="modal fade custom-modal" tabindex="-1" role="dialog" aria-labelledby="modal_user" aria-hidden="true">
    <div class="modal-dialog"></div>
</div>

