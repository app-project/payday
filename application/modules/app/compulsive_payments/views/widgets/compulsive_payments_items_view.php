<?php if(!empty($data_result)):?>
    <?php foreach ($data_result AS $key => $row):
        $next_payment_day = next_payment_day($row->payment_dayId, $row->day_work);
        $next_payment_day_text = ($next_payment_day[0] == 0)? "Proximo cobro Hoy" : "Proximo cobro en ".$next_payment_day[1];?>
        <div class="row">
            <div class="col-md-12">
                <div class="card border-card">
                    <h5 class="card-header p-t-10-color" style="background-color: #<?php echo getColor($next_payment_day[0])?>;"> <?php echo $next_payment_day_text;?></h5>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-2  solid-black">
                                <?php $image = ($row->image != "" || $row->image != null) ? "assets/storage/avatars/".$row->image : "assets/images/avatars/user.png"?>
                                <img src="<?php echo base_url().$image;?>" width="100px" height="100px" class="rounded float-left" style="margin: 25px;" alt="...">
                            </div>
                            <div class="col-md-5">
                                <div class="row">
                                    <label class="col-md-3 text-right">Nombre:</label>
                                    <div class="col-md-8"><span><?php echo $row->full_name;?></span></div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 text-right">Capital:</label>
                                    <div class="col-md-8"><span><?php echo '$'.number_format($row->requested_amount,2);?></span></div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 text-right">Interes:</label>
                                    <div class="col-md-8"><span><?php echo '$'.number_format($row->payment_amount, 2);?></span></div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 text-right">Banco:</label>
                                    <div class="col-md-8"><span><?php echo $row->bank;?></span></div>
                                </div>
                                <div class="row">
                                    <label class="col-md-3 text-right">Correo:</label>
                                    <div class="col-md-8"><span><?php echo $row->email;?></span></div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="row">
                                    <label class="col-md-4 text-right">Usuario:</label>
                                    <div class="col-md-8"><span><?php echo $row->user_bank;?></span></div>
                                </div>
                                <div class="row">
                                    <label class="col-md-4 text-right">Clave:</label>
                                    <div class="col-md-8"><span><?php echo $row->password_bank;?></span></div>
                                </div>
                                <div class="row">
                                    <label class="col-md-4 text-right">Telefono:</label>
                                    <div class="col-md-8"><span><?php echo $row->phone;?></span></div>
                                </div>
                                <div class="row">
                                    <label class="col-md-4 text-right">Celular:</label>
                                    <div class="col-md-8"><span><?php echo $row->mobile;?></span></div>
                                </div>
                                <div class="row">
                                    <label class="col-md-4 text-right">Correo Trabajo:</label>
                                    <div class="col-md-8"><span><?php echo $row->email_work;?></span></div>
                                </div>
                                <div class="row">
                                    <label class="col-md-4 text-right">Facebook:</label>
                                    <div class="col-md-8"><a href="<?php echo $row->link_facebook;?>" target="_blank"><?php echo $row->name_facebook;?></a></div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <a href="javascript:void(0);" class="btn btn-info modal_trigger" data-toggle="modal" data-target="#compulsive_payments_view" data-url="<?php echo base_url('compulsive_payments/payments_purchase/'.$row->loan_analysiId);?>"><i class="fa fa-money"></i> Restar Capital</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-header p-t-10-text" style="background-color: <?php //echo $day_status;?>/*;">
                        <h6 style="color: white;"><?php //echo $day_label;?></h6>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach;?>
<?php else:;?>

<?php endif;?>