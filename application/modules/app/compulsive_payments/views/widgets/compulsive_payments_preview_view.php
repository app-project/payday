<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-newspaper-o"></i> <?php echo '#00'.$row->request_code.'-'.$row->full_name;?></h5>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    </div>
    <div class="modal-body">
        <div class="card-body">
            <div class="row">
                <div class="col-md-2  solid-black">
                    <?php $image = ($row->image != "" || $row->image != null) ? "assets/storage/avatars/".$row->image : "assets/images/avatars/user.png"?>
                    <img src="<?php echo base_url().$image;?>" width="100px" height="100px" class="rounded float-left" alt="...">
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <label class="control-label col-md-3 text-right mg-top">Nombre:</label>
                        <div class="col-md-7 mg-top"><span><?php echo $row->full_name;?></span></div>
                    </div>
                    <div class="row">
                        <label class="control-label col-md-3 text-right mg-top">Correo:</label>
                        <div class="col-md-7 mg-top"><span><?php echo $row->email;?></span></div>
                    </div>
                    <div class="row">
                        <label class="control-label col-md-3 text-right mg-top">Telefono:</label>
                        <div class="col-md-7 mg-top"><span><?php echo $row->phone;?></span></div>
                    </div>
                    <div class="row">
                        <label class="control-label col-md-3 text-right mg-top">Celular:</label>
                        <div class="col-md-7 mg-top"><span><?php echo $row->mobile;?></span></div>
                    </div>
                    <div class="row">
                        <label class="control-label col-md-3 text-right mg-top">Facebook:</label>
                        <div class="col-md-7 mg-top"><a href="<?php echo $row->link_facebook;?>" target="_blank"><?php echo $row->name_facebook;?></a></div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="row">
                        <label class="control-label col-md-5 text-right mg-top">Capital:</label>
                        <div class="col-md-7 mg-top"><span><?php echo '$'.number_format($row->requested_amount,2);?></span></div>
                    </div>
                    <div class="row">
                        <label class="control-label col-md-5 text-right mg-top">Interes:</label>
                        <div class="col-md-7 mg-top"><span><?php echo '$'.number_format($row->payment_amount, 2);?></span></div>
                    </div>
                    <div class="row">
                        <label class="control-label col-md-5 text-right mg-top">Banco:</label>
                        <div class="col-md-7 mg-top"><span><?php echo $row->bank;?></span></div>
                    </div>
                    <div class="row">
                        <label class="control-label col-md-5 text-right mg-top">Usuario:</label>
                        <div class="col-md-7 mg-top"><span><?php echo $row->user_bank;?></span></div>
                    </div>
                    <div class="row">
                        <label class="control-label col-md-5 text-right mg-top">Clave:</label>
                        <div class="col-md-7 mg-top"><span><?php echo $row->password_bank;?></span></div>
                    </div>
                    <div class="row">
                        <label class="control-label col-md-5 text-right mg-top">Correo Laboral:</label>
                        <div class="col-md-7 mg-top"><span><?php echo $row->email_work;?></span></div>
                    </div>
                    <div class="row">
                        <label for="" class="control-label  col-md-5  text-right mg-top">Estado:</label>
                        <?php $chk_load = '';
                        switch ($row->statusId):
                            case 1:
                                $chk_load = '<div class="col-md-4 mg-top label p-t-6" style="background-color:#FFC000;">DESEMBOLSADO</div>';
                                break;
                            case 2:
                                $chk_load = '<div class="col-md-4 mg-top label p-t-6" style="background-color:#FF0000;">CANCELADO</div>';
                                break;
                            case 3:
                                $chk_load = '<div class="col-md-4 mg-top label p-t-6" style="background-color:#9e9c9c;">ARCHIVADO</div>';
                                break;
                            case 4:
                                $chk_load = '<div class="col-md-4 mg-top label p-t-6" style="background-color:#53c766;">SALDADO</div>';
                                break;
                        endswitch;
                        echo $chk_load; ?>
                    </div>
                </div>
            </div>
        </div>
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-deposit" role="tab" aria-controls="nav-home" aria-selected="true"><i class="fa fa-fire"></i> Cobros</a>
            </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-deposit" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="card mb-3">
                    <!-- end card-header-->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table id="compulsive-payments" class="table">
                                        <thead>
                                        <tr>
                                            <th width="25%">Fecha</th>
                                            <th width="20%">Capital</th>
                                            <th width="20%">Interes</th>
                                            <th width="20%">Mora</th>
                                            <th width="2%"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if(!empty($compulsive_items)):?>
                                            <?php foreach($compulsive_items AS $key => $row):?>
                                                <tr>
                                                    <td class="width-50 form-group"><?php echo $row->date ?></td>
                                                    <td class="width-15"><?php echo number_format($row->capital,2); ?></td>
                                                    <td class="width-15"><?php echo number_format($row->interest,2); ?></td>
                                                    <td class="width-15"><?php echo number_format($row->late_payment,2); ?></td>
                                                </tr>
                                            <?php endforeach;?>
                                        <?php else:?>
                                            <tr>
                                                <td colspan="4" class="text-center"><span class='text-muted'>Ningún dato disponible en esta tabla.</span></td>
                                            </tr>
                                        <?php endif;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
</div>