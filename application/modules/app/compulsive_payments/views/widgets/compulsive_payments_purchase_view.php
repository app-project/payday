<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title"><i class="fa fa-fire"></i> Cobros / <span class="title-input"><?php echo $row_client->full_name;?></span> <span class="title-input-p"></span></h5>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    </div>
    <div class="modal-body">
        <form action="<?php echo base_url()?>compulsive_payments/payment_insert" id="form" method="post" role="form">
            <div class="modal-response"></div>
            <input type="hidden" id="capital-result" value="<?php echo $row_client->requested_amount;?>">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="compulsive-payments" class="table">
                            <thead>
                            <tr>
                                <th width="25%">Fecha</th>
                                <th width="20%">Capital</th>
                                <th width="20%">Interes</th>
                                <th width="20%">Mora</th>
                                <th width="2%"></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($compulsive_items)):?>
                                <?php foreach($compulsive_items AS $key => $row):?>
                                    <tr class="row-item inline">
                                        <input type="hidden" name="compulsive_itemId[]" value="<?php echo $row->compulsive_itemId; ?>">
                                        <input type="hidden" name="loan_analysiId[]" value="<?php echo $row->loan_analysiId; ?>">
                                        <input type="hidden" name="clientId[]" value="<?php echo $row->clientId; ?>">
                                        <td class="width-50 form-group"><input type="text" name="date[]" value="<?php echo $row->date ?>" class="form-control date date-compulsive" id="date_compulsive" readonly></td>
                                        <td class="width-15"><input type="text" name="capital[]" value="<?php echo number_format($row->capital,2); ?>" class="form-control numeric-decimal capital currency" id="capital" readonly></td>
                                        <td class="width-15"><input type="text" name="interest[]" value="<?php echo number_format($row->interest,2); ?>" class="form-control numeric-decimal interest currency" id="interest" readonly></td>
                                        <td class="width-15"><input type="text" name="late_payment[]" value="<?php echo number_format($row->late_payment,2); ?>" class="form-control numeric-decimal late-payment currency" id="late_payment"></td>
                                        <td class="master-closet"><button type="button" class="close trigger_remove_row mg-8" data-id="<?php echo $row->compulsive_itemId; ?>"><i class="fa fa-window-close-o close close-icon-windows"></i></button></td>
                                    </tr>
                                <?php endforeach;?>
                            <?php else:?>
                                <tr class="row-item inline">
                                    <input type="hidden" name="compulsive_itemId[]" value="0">
                                    <input type="hidden" name="loan_analysiId[]" value="<?php echo $row_client->loan_analysiId; ?>">
                                    <input type="hidden" name="clientId[]" value="<?php echo $row_client->clientId;?>">
                                    <td class="width-50 form-group"><input type="text" name="date[]" class="form-control date date-compulsive" id="date-compulsive" readonly></td>
                                    <td class="width-15"><input type="text" name="capital[]" value="<?php echo number_format($row_client->requested_amount,2);?>" class="form-control numeric-decimal capital currency" id="capital" readonly></td>
                                    <td class="width-15"><input type="text" name="interest[]" value="<?php echo number_format($row_client->payment_amount,2);?>" class="form-control numeric-decimal interest currency" id="interest" readonly></td>
                                    <td class="width-15"><input type="text" name="late_payment[]" class="form-control numeric-decimal late-payment currency" value="0.00" id="late_payment"></td>
                                    <td class="master-closet"><button type="button" class="close trigger_remove_row mg-8" data-id="0"><i class="fa fa-window-close-o close close-icon-windows"></i></button></td>
                                </tr>
                            <?php endif;?>
                            <tr class="last_row"><td colspan="8"><button type="button" class="btn btn-info btn-rounded trigger_add_row border-button btn-sm"><i class="fa fa-money"></i></button></td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button id="button-submit" type="submit" class="btn btn-primary ladda-button" data-style="expand-right" data-load_id="<?php echo $row_client->loan_analysiId; ?>">
            <span class="ladda-label">Guardar</span>
        </button>
    </div>
</div>

<table class="hidden">
    <tbody id="row-table-hidden">
    <tr class="row-item inline">
        <input type="hidden" name="compulsive_itemId[]" value="0">
        <input type="hidden" name="loan_analysiId[]" value="<?php echo $row_client->loan_analysiId; ?>">
        <input type="hidden" name="clientId[]" value="<?php echo $row_client->clientId;?>">
        <td class="width-50 form-group"><input type="text" name="date[]" class="form-control date date-compulsive" id="date-compulsive" readonly></td>
        <td class="width-15"><input type="text" name="capital[]" value="<?php echo number_format($row_client->requested_amount,2);?>" class="form-control numeric-decimal capital currency" id="capital" readonly></td>
        <td class="width-15"><input type="text" name="interest[]" value="<?php echo number_format($row_client->payment_amount,2);?>" class="form-control numeric-decimal interest currency" id="interest" readonly></td>
        <td class="width-15"><input type="text" name="late_payment[]" class="form-control numeric-decimal late-payment currency" value="0.00" id="late_payment"></td>
        <td class="master-closet"><button type="button" class="close trigger_remove_row mg-8" data-id="0"><i class="fa fa-window-close-o close close-icon-windows"></i></button></td>
    </tr>
    </tbody>
</table>