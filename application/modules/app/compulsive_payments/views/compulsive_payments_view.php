<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="breadcrumb-holder">
                        <h1 class="main-title float-left"> Cobros Compulsivos</h1>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="<?php echo base_url()?>compulsive_payments">Inicio</a></li>
                            <li class="breadcrumb-item active"> Cobro Compulsivo</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card mb-3">
                        <div class="card-header">
                            <span class="pull-right"><a class="btn btn-primary btn-sm ladda-button" data-style="expand-right" href="<?php echo base_url('compulsive_payments/clients_compulsive');?>"><i class="fa fa-sticky-note-o"></i> Vista Previa</a></span>
                        </div>
                        <!-- end card-header -->
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="list" class="table table-bordered table-hover display dataTable no-footer" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Cliente</th>
                                            <th>Tipo de Cobro</th>
                                            <th>Capital</th>
                                            <th>Cuota</th>
                                            <th>Banco</th>
                                            <th>Estado</th>
                                            <th>Acción</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end card-body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- END container-fluid -->
    </div>
    <!-- END content -->
</div>
<!-- END content-page -->

<div id="preview_compulsive" class="modal fade custom-modal" tabindex="-1" role="dialog" aria-labelledby="preview_compulsive" aria-hidden="true">
    <div class="modal-dialog"></div>
</div>