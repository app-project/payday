<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->title       = 'Reportes';
        $this->securities->is_logged_in($this->session->userdata('is_logged_in'));

        $this->load->model('reports/reports_model');
    }

    public function index()
    {
        $data = array();
        $data['content'] = 'reports/reports_view';
        $this->load->view('include/template',$data);
    }
}
