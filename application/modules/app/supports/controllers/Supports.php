<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Supports extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->title       = 'Soporte';
        $this->load->model('supports/supports_model');
    }

    public function index()
    {
        $data = array();
        $this->load->view('supports/supports_view',$data);
    }
}
