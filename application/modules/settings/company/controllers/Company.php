<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Company extends MY_Controller
{
    public $title;

    public function __construct()
    {
        parent::__construct();
        $this->title        = 'Empresa';
        $this->securities->is_logged_in($this->session->userdata('is_logged_in'));

        //Load Model
        $this->load->model('company/settings_model');

        //Load Module
        $this->load->module('com_files/controller/com_files');
    }

    public function index()
    {
        $data                   = array();
        $data['data_settings']  = $this->settings_model->get_by(array('hidden' => 0, 'companyId' => 1), TRUE);
        $data['content']        = 'company/company_view';
        $this->load->view('include/template',$data);
    }

    public function insert()
    {
        $this->form_validation->set_rules('company', '<strong>Empresa</strong>', 'trim|required');
        $this->form_validation->set_rules('email', '<strong>Correo</strong>', 'trim|required|valid_email');


        if($this->form_validation->run($this) == FALSE)
        {
            echo json_encode(array('result' => 0, 'error' => display_error(validation_errors())));
        }
        else
        {
            $data = array(
                "company"       => $this->input->post("company"),
                "email"         => $this->input->post("email"),
                "phone"         => $this->input->post("phone"),
                "mobil"         => $this->input->post("mobil"),
                "politics"      => $this->input->post("politics"),
                "companyId"     => 1,
            );

            if(!empty($_FILES))
            {
                $ext                      = substr(strrchr($_FILES['file']['name'], "."), 1);
                $data_file = array(
                    'file_name'           => '',
                    'file_type'           => $ext,
                    'allowed_types'       => 'gif|jpg|png|jpeg',
                    'folder'              => 'company'
                );

                $result                   = $this->com_files->upload($data_file);

                if($result["result"] != 0)
                {
                    $data['image']  = $result['file'];
                }
            }

            if($_POST['settingId'] == 0)
            {
                $this->settings_model->save($data);
            }
            else
            {
                $this->settings_model->save($data, $_POST['settingId']);
            }

            $data_settings                                      = $this->session->userdata('settings');
            $data_settings['user_data']['settings']['company']  = $data['company'];
            $data_settings['user_data']['settings']['email']    = $data['email'];
            $data_settings['user_data']['settings']['phone']    = $data['phone'];
            $data_settings['user_data']['settings']['mobil']    = $data['mobil'];
            $data_settings['user_data']['settings']['politics'] = $data['politics'];
            $data_settings['user_data']['settings']['image']    = $data['image'];

            echo json_encode(array('result' => 1));
        }
    }
}
