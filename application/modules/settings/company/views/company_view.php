<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="breadcrumb-holder">
                        <h1 class="main-title float-left">Empresa</h1>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="<?php echo base_url()?>dashboard">Inicio</a></li>
                            <li class="breadcrumb-item active">Empresa</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <form id="form" action="<?php echo base_url('company/insert');?>" method="post" enctype="multipart/form-data">
                        <?php $settingId = (isset($data_settings) && !empty($data_settings))? $data_settings->settingId : 0;?>
                        <input type="hidden" name="settingId" value="<?php echo $settingId;?>">
                        <div class="card mb-3">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <nav>
                                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-company" role="tab" aria-controls="nav-home" aria-selected="true">
                                                    <h6><i class="fa fa-newspaper-o"></i> Información de la Empresa</h6>
                                                </a>
                                            </div>
                                        </nav>
                                        <div class="tab-content" id="nav-tabContent">
                                            <div class="tab-pane fade show active" id="nav-company" role="tabpanel" aria-labelledby="nav-person-tab">
                                                <div class="card mb-3">
                                                    <!-- end card-header-->
                                                    <div class="card-body">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="row">
                                                                    <div class="col-md-11 offset-1 alpha">
                                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                            <div class="fileinput-new img-thumbnail" style="width: 200px; height: 150px; text-align: center">
                                                                                <?php $image = (isset($data_settings) && !empty($data_settings) &&  $data_settings->image != '')? $data_settings->image : base_url()."assets/images/avatars/user.png";?>
                                                                                <img src="<?php echo $image;?>"  alt="...">
                                                                            </div>
                                                                            <div class="fileinput-preview fileinput-exists img-thumbnail" style="max-width: 200px; max-height: 150px; text-align: center"></div>
                                                                            <div class="file-buttons">
                                                                                <span class="btn btn-outline-secondary btn-file"><span class="fileinput-new"><i class="fa fa-cloud-upload"></i></span><span class="fileinput-exists">Cambiar</span><input type="file" name="file"></span>
                                                                                <a href="javascript:void();" class="btn btn-outline-secondary fileinput-exists" data-dismiss="fileinput">Remover</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                            </div>

                                                            <div class="col-md-6 alpha omega">
                                                                <div class="row form-group">
                                                                    <?php $company = (isset($data_settings) && !empty($data_settings))? $data_settings->company : '';?>
                                                                    <label for="company" class="col-md-3 control-label text-right mg-top">Empresa:</label>
                                                                    <input type="text" name="company" class="form-control col-md-5" id="company" value="<?php echo $company;?>" data-field="document">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <?php $email = (isset($data_settings) && !empty($data_settings))? $data_settings->email : '';?>
                                                                    <label for="email" class="col-md-3 control-label text-right mg-top">Correo:</label>
                                                                    <input type="email" name="email" class="form-control col-md-6" id="email" placeholder="Email" value="<?php echo $email;?>" data-field="first_name">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 alpha omega">
                                                                <div class="row form-group">
                                                                    <?php $phone = (isset($data_settings) && !empty($data_settings))? $data_settings->phone : '';?>
                                                                    <label for="phone" class="col-md-3 control-label text-right mg-top">Teléfono:</label>
                                                                    <input type="text" name="phone" class="form-control phone col-md-6" id="phone" placeholder="Teléfono" value="<?php echo $phone;?>" data-field="last_name">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <?php $mobil = (isset($data_settings) && !empty($data_settings))? $data_settings->mobil : '';?>
                                                                    <label for="mobile" class="col-md-3 control-label text-right mg-top">Celular:</label>
                                                                    <input type="text" name="mobile" class="col-md-5 form-control phone" id="mobile" placeholder="Celular"  value="<?php echo $mobil;?>" data-field="mobile">
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <hr>
                                                                <div class="row form-group">
                                                                    <?php $politics = (isset($data_settings) && !empty($data_settings))? $data_settings->politics : '';?>
                                                                    <label for="politics" class="col-md-2 control-label text-right mg-top">Politicas:</label>
                                                                    <textarea name="politics" class="col-md-9 form-control" id="politics" cols="70" rows="10"><?php echo $politics;?></textarea>
                                                                    <div class="col-md-3 valid-message"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- end card-body -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 text-right p-b-10">
                                <button id="button-submit" type="submit" class="btn btn-primary ladda-button" data-style="expand-right">
                                    <span class="ladda-label">Guardar</span>
                                </button>
                            </div>
                        </div>
                    </form>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- END container-fluid -->
    </div>
    <!-- END content -->
</div>
<!-- END content-page -->