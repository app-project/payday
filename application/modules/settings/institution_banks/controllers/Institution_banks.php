<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Institution_banks extends MY_Controller
{
    public $title;

    public function __construct()
    {
        parent::__construct();
        $this->title        = 'Instituciones Bancarias';
        $this->securities->is_logged_in($this->session->userdata('is_logged_in'));

        //Load Model
        $this->load->model('institution_banks/institution_bank_model');

        //Load Module
        $this->load->module('com_files/controller/com_files');
    }

    public function index()
    {
        $data = array();
        $data['content'] = 'institution_banks/institution_bank_view';
        $this->load->view('include/template',$data);
    }

    public function datatable()
    {
        $columns    = "bankId,code,name,description,image,exclude,status";
        $all_users  = $this->institution_bank_model->datatable($columns, FALSE, TRUE);

        echo json_encode(array('data' => $all_users));
    }

    public function add()
    {
        echo json_encode(array('result' => 1, 'view' =>  $this->load->view('institution_banks/institution_bank_new_view', array(), TRUE)));
    }

    public function edit($bankId)
    {
        $data['row'] = $this->institution_bank_model->get_by(array('bankId' => $bankId), TRUE);
        echo json_encode(array('result' => 1, 'view' => $this->load->view('institution_banks/institution_bank_edit_view', $data, TRUE)));
    }

    public function insert()
    {
        $this->form_validation->set_rules('name_bank', '<strong>Nombre</strong>', 'trim|required|callback_bank_name_check');
        $this->form_validation->set_rules('code', '<strong>Codigo</strong>', 'trim|required|callback_bank_code_check');


        if($this->form_validation->run($this) == FALSE)
        {
            echo json_encode(array('result' => 0, 'error' => display_error(validation_errors())));
        }
        else
        {
            $data = array(
                "code"           => $this->input->post("code"),
                "name"           => $this->input->post("name_bank"),
                "description"    => $this->input->post("description"),
                "exclude"        => 1,
                "status"         => (isset($_POST['status']))? $this->input->post("status") : 0,
            );

            if(!empty($_FILES))
            {
                $ext                      = substr(strrchr($_FILES['file']['name'], "."), 1);
                $data_file = array(
                    'file_name'           => '',
                    'file_type'           => $ext,
                    'allowed_types'       => 'gif|jpg|png|jpeg',
                    'folder'              => 'banks'
                );

                $result            = $this->com_files->upload($data_file);

                if($result["result"] != 0)
                {
                    $data['image']  = $result['file'];
                }
            }

            $this->institution_bank_model->save($data);

            echo json_encode(array('result' => 1));
        }
    }

    public function update($bankId)
    {
        $row = $this->institution_bank_model->get_by(array('bankId' => $bankId), TRUE);

        $this->form_validation->set_rules('code', '<strong>Codigo</strong>', 'trim|required|callback_bank_code_check');
        $this->form_validation->set_rules('name_bank', '<strong>Nombre</strong>', 'trim|required|callback_bank_name_check');

        if($this->form_validation->run($this) == FALSE)
        {
            echo json_encode(array('result' => 0, 'error' => display_error(validation_errors())));
        }
        else
        {
            $data = array(
                "code"           => $this->input->post("code"),
                "name"           => $this->input->post("name_bank"),
                "description"    => $this->input->post("description"),
                "status"         => (isset($_POST['status']))? $this->input->post("status") : 0,
            );

            if(!empty($_FILES))
            {
                $ext                      = substr(strrchr($_FILES['file']['name'], "."), 1);
                $data_file = array(
                    'file_name'           => '',
                    'file_type'           => $ext,
                    'allowed_types'       => 'gif|jpg|png|jpeg',
                    'folder'              => 'banks'
                );

                $result            = $this->com_files->upload($data_file);

                if($result["result"] != 0)
                {
                    $data['image']  = $result['file'];
                    if($row->image != null || $row->image != "")
                    {
                        $this->com_files->unlink_image('banks', $row->image);
                    }
                }
            }

            $this->institution_bank_model->save($data, $bankId);

            echo json_encode(array('result' => 1));
        }
    }

    public function delete($bankId)
    {
        if($this->institution_bank_model->delete($bankId) === TRUE)
        {
            echo json_encode(array('result' => 1));
        }
        else
        {
            echo json_encode(array('result' => 0));
        }
    }

    public function bank_code_check()
    {
        $where = array(
            'LOWER(REPLACE(code," ",""))=' => clear_space($this->input->post('code')),
            'hidden'                       => 0,
            'bankId !='                    => (isset($_POST['bankId']) && $_POST['bankId'] != 0)? $_POST['bankId'] : 0
        );

        return ($this->institution_bank_model->in_table_by($where) > 0)? FALSE : TRUE;
    }

    public function bank_name_check()
    {
        $where = array(
            'LOWER(REPLACE(name," ",""))=' => clear_space($this->input->post('name')),
            'hidden'                       => 0,
            'bankId !='                    => (isset($_POST['bankId']) && $_POST['bankId'] != 0)?  $_POST['bankId'] : 0
        );

        return ($this->institution_bank_model->in_table_by($where) > 0)? FALSE : TRUE;
    }
}
