<div class="modal-content">
    <div class="modal-header">
        <h5 class="modal-title">Editar Instituciones Bancarias / <span class="title-input"><?php echo $row->name;?></span></h5>
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    </div>
    <form action="<?php echo base_url('institution_banks/update/'.$row->bankId)?>" id="form" method="post" enctype="multipart/form-data" role="form">
        <div class="modal-body">
            <div class="modal-response"></div>
            <div class="row">
                <div class="col-md-8 form-border">
                    <div class="row form-group">
                        <input type="hidden" name="bankId" value="<?php echo $row->bankId;?>">
                        <div class="col-md-3 text-right"><label for="code">Codigo:</label></div>
                        <div class="col-md-3"><input type="text" name="code" id="code" class="form-control numeric"  value="<?php echo $row->code;?>"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3 text-right"><label for="name_bank">Nombre:</label></div>
                        <div class="col-md-8"><input type="text" name="name_bank" id="name_bank" class="form-control" value="<?php echo $row->name;?>"></div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-3 text-right"><label for="description">Descripción:</label></div>
                        <div class="col-md-8">
                            <textarea type="text" name="description" id="description" class="form-control"><?php echo $row->description;?></textarea>
                        </div>
                    </div>
                    <hr>
                    <div class="row form-group">
                        <?php $chk_status = ($row->status == 1)? 'checked': '';?>
                        <div class="col-md-3 text-right"><label for="">Estado:</label></div>
                        <div class="col-md-5">
                            <label class="checkbox">
                                <input type="checkbox" name="status" id="status" <?php echo $chk_status;?> value="1">
                                <span class="check"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row form-group">
                        <div class="col-md-8 offset-1">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <?php $image = ($row->image != "" || $row->image != null) ? "assets/uploads/bank/".$row->image : "assets/images/avatars/bank.png"?>
                                <div class="fileinput-new img-thumbnail" style="width: 200px; height: 150px; text-align: center">
                                    <img src="<?php echo base_url().$image?>"  alt="...">
                                </div>
                                <div class="fileinput-preview fileinput-exists img-thumbnail" style="max-width: 200px; max-height: 150px; text-align: center"></div>
                                <div class="file-buttons">
                                    <span class="btn btn-outline-secondary btn-file"><span class="fileinput-new"><i class="fa fa-cloud-upload"></i></span><span class="fileinput-exists">Cambiar</span><input type="file" name="file"></span>
                                    <a href="#" class="btn btn-outline-secondary fileinput-exists" data-dismiss="fileinput">Remover</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button id="button-submit" type="submit" class="btn btn-primary ladda-button" data-style="expand-right">
                <span class="ladda-label">Guardar</span>
            </button>
        </div>
    </form>
</div>

<script>
    $(document).ready(function(){
        if($('.numeric').length > 0){$('.numeric').numeric({negative:false});}
        Ladda.bind('.ladda-button');
    });
</script>