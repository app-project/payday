<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="breadcrumb-holder">
                        <h1 class="main-title float-left">Instituciones Bancarias</h1>
                        <ol class="breadcrumb float-right">
                            <li class="breadcrumb-item"><a href="<?php echo base_url()?>dashboard">Inicio</a></li>
                            <li class="breadcrumb-item active">Instituciones Bancarias</li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="card mb-3">
                        <div class="card-header">
                            <span class="pull-right"><a href="javascript:void(0)" class="btn btn-primary btn-sm modal_trigger" data-url="<?php echo base_url()?>institution_banks/add" data-toggle="modal" data-target="#modal_bank"><i class="fa fa-plus" aria-hidden="true"></i> Nuevo</a></span>
                        </div>
                        <!-- end card-header -->
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="list" class="table table-bordered table-hover display dataTable no-footer" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th></th>
                                        <th>Codigó</th>
                                        <th>Nombre</th>
                                        <th>Descripción</th>
                                        <th>Foto</th>
                                        <th>Estado</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- end card-body -->
                    </div>
                    <!-- end card -->
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- END container-fluid -->
    </div>
    <!-- END content -->
</div>
<!-- END content-page -->

<div class="modal fade custom-modal" tabindex="-1" role="dialog" aria-labelledby="modal_bank" aria-hidden="true" id="modal_bank">
    <div class="modal-dialog"></div>
</div>