<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Institution_bank_model extends MY_Model
{
    public function __construct()
    {
        $this->table_name   = "ai_institution_banks";
        $this->view_name    = "ai_institution_banks_view";
        $this->primary_key  = "bankId";
        $this->order_by     = "bankId DESC";
    }
}