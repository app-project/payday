var Calculate;

/**
 *
 * @type {{setSingleAlert: setSingleAlert, setRequestAlert: setRequestAlert, submit: submit, submitData: submitData, getValue: getValue, getAppend: getAppend, actionsAfterProccess: actionsAfterProccess, ajaxRequestEnctype: ajaxRequestEnctype}}
 */
// DOM = {
//     setSingleAlert: function (data) {
//         swal({
//             type: data.type,
//             title: data.title,
//             text: data.text
//         });
//     },
//     setRequestAlert: function (data, callBack) {
//     var title = (typeof data.title === "undefined") ? 'Esta seguro de eliminar este registro?' : data.title,
//         text = (typeof data.text === "undefined") ? 'Una vez eliminado no podra recuperarlo.' : data.text,
//         type = (typeof data.type === "undefined") ? 'warning' : data.type,
//         confirmButtonColor = (typeof data.confirmButtonColor === "undefined") ? '#3085d6' : data.confirmButtonColor,
//         cancelButtonColor = (typeof data.cancelButtonColor === "undefined") ? '#d33' : data.cancelButtonColor,
//         confirmButtonText = (typeof data.confirmButtonText === "undefined") ? "Si, eliminar!" : data.confirmButtonText;
//
//     swal({
//         title: title,
//         text: text,
//         type: type,
//         showCancelButton: true,
//         confirmButtonColor: confirmButtonColor,
//         cancelButtonColor: cancelButtonColor,
//         confirmButtonText: confirmButtonText,
//     }).then((result) => {
//         if(result.value)
//     {
//         request(data).done(function (r) {
//             var response = JSON.parse(r);
//
//             if (response.result !== 0) {
//                 DOM.actionsAfterProccess(data, response, callBack);
//             }
//             else {
//                 var alert = {
//                     type: "error",
//                     title: "No se puede ejecutar la operacion",
//                     text: "Por el momento no se puede ejecutar esta operacion, si el problema continua contacte con su proveedor"
//                 };
//                 this.setSingleAlert(alert);
//             }
//         })
//     }
// })
// },
//     submit: function (data, callBack) {
//         request(data).done(function (r) {
//             var response = JSON.parse(r);
//
//             if (response.result !== 0) {
//                 DOM.actionsAfterProccess(data, response, callBack);
//             } else {
//                 var messageError = (typeof data.messageError === "undefined") ? '.response' : data.messageError;
//                 Ladda.stopAll();
//                 $(messageError).html(response.error);
//                 $('html, body').animate({scrollTop: 0}, 100, 'linear');
//             }
//         });
//
//         // Handle any callback function that was sent
//         // if(typeof callBack !== "undefined") {
//         //     callBack();
//         // }
//     },
//     submitData: function (data, callBack) {
//         DOM.ajaxRequestEnctype(data, function (r) {
//             var response = JSON.parse(r);
//
//             if (response.result !== 0) {
//                 DOM.actionsAfterProccess(data, response);
//             } else {
//                 var messageError = (typeof data.messageError === "undefined") ? '.response' : data.messageError;
//                 Ladda.stopAll();
//                 $(messageError).html(response.error);
//                 $('html, body').animate({scrollTop: 0}, 100, 'linear');
//             }
//         });
//         // Handle any callback function that was sent
//         if (typeof callBack !== "undefined") {
//             callBack();
//         }
//     },
//     getValue: function(data){
//         var response = ajaxRequest(data);
//         if(response.result !== 0) {
//             if(data.action === 'undefined') data.action = '';
//             switch (data.action){
//                 case 'return':
//                     return response;
//                     break;
//                 default:
//                     (typeof data.selector === 'undefined')? data.selector2.val(response.result) : $(data.selector).val(response.result);
//                     break;
//             }
//         }
//     },
//     getAppend: function (data, callBack) {
//         request(data).done(function (r) {
//             var response = JSON.parse(r);
//             if (response.result !== 0) {
//                 if (typeof data.modal !== "undefined") {
//                     $(data.modal).modal('toggle');
//                 }
//                 switch (data.action) {
//                     case 'html':
//                         $(data.selector).html(response.view);
//                         break;
//
//                     case 'append':
//                         $(data.selector).append(response.view);
//                         break;
//
//                     case 'prepend':
//                         $(data.selector).prepend(response.view);
//                         break;
//
//                     case 'before':
//                         $(data.selector).before(response.result);
//                         break;
//
//                     case 'insertAfter':
//                         data.selector.after(response.result);
//                         break;
//
//                     case 'redirect':
//                         window.location = data.returnUrl;
//                         break;
//
//                     default:
//                         $(data.selector).html(response.view);
//                 }
//             }
//         });
//
//         // Handle any callback function that was sent
//         if (typeof callBack !== "undefined") {
//             callBack();
//         }
//     },
//     actionsAfterProccess: function (data, response, callBack) {
//         switch (data.doAfter) {
//             case "redirect":
//                 var url = (typeof response.url === "undefined") ? data.returnUrl : response.url;
//                 if (typeof data.showAlert !== "undefined" && data.showAlert == true) {
//                     this.setSingleAlert({type: "success", title: data.titleResponse, text: data.textResponse});
//                 }
//                 window.location = url;
//                 break;
//
//             case "datatable":
//                 if (data.showAlert == true) {
//                     this.setSingleAlert({type: "success", title: data.titleResponse, text: data.textResponse});
//                     data.table.ajax.reload();
//                     $(data.modal).modal('toggle');
//                 }
//                 break;
//
//             case "html":
//                 $(data.selector).html(response.view);
//                 break;
//
//             case "remove":
//                 $(data.selector).remove();
//                 break;
//
//             case "calendar":
//                 data.selector.fullCalendar('refetchEvents');
//                 data.selector.fullCalendar('rerenderEvents');
//                 data.selector.fullCalendar('refetchResources');
//                 $('.popover').popover('hide');
//                 $(data.modal).modal('toggle');
//                 break;
//
//             case "moda_hidden":
//                 $(data.modal).modal('hide');
//         }
//
//         // Handle any callback function that was sent
//         if (typeof callBack !== "undefined") {
//             callBack();
//         }
//     },
//     isLoggedIn: function(response) {
//         var skip = ["login", "session"],
//             path = getUrl.urlPath().split("/");
//
//         if($.inArray(path[0], skip) == false) {
//
//             if (response.is_logged_in == false) {
//                 swal({
//                     title: "Atención",
//                     text: 'Has sido desconectado. Por favor Iniciar sesión.',
//                     type: "warning",
//                     confirmButtonColor: "#DD6B55",
//                     confirmButtonText: "Login",
//                     confirmButtonClass: "ladda-button",
//                     closeOnConfirm: false
//                 }, function () {
//                     window.location = getUrl.baseUrl() + 'login/' + getUrl.urlPath();
//                 });
//             }
//         }
//     },
//     ajaxRequestEnctype: function (data, success) {
//         $(data.form).ajaxSubmit({success: success});
//     }
// };

var DOM = {
    setSingleAlert: function (data) {
        swal({
            type: data.type,
            title: data.title,
            text: data.text
        });
    },
    getAppend: function(data, callBack) {
        DOM.ajaxRequest(data).done(function(r) {
            var response = JSON.parse(r);
            DOM.resultHandler(data, response, callBack);
        });
    },
    submit: function (data, callBack, errorCallBack) {
        DOM.ajaxRequest(data).done(function(r) {
            var response = JSON.parse(r);
            DOM.resultHandler(data, response, callBack, errorCallBack);
        });
    },
    setRequestAlert: function (data, callBack) {
        var title   = (typeof data.title === "undefined") ? 'Atención' : data.title,
            text    = (typeof data.text === "undefined") ? 'Esta Seguro que Desea Eliminar este Registro?' : data.text,
            type    = (typeof data.type === "undefined") ? 'warning' : data.type,
            confirmButtonColor = (typeof data.confirmButtonColor === "undefined") ? '#3085d6' : data.confirmButtonColor,
            cancelButtonColor = (typeof data.cancelButtonColor === "undefined") ? '#d33' : data.cancelButtonColor,
            confirmButtonText = (typeof data.confirmButtonText === "undefined") ? "<i class='fa fa-recycle'> Si, eliminar! </i>" : data.confirmButtonText;

        swal({
            title: title,
            text: text,
            type: type,
            showCancelButton: true,
            confirmButtonColor: confirmButtonColor,
            cancelButtonColor: cancelButtonColor,
            confirmButtonText: confirmButtonText,
        }).then((result) => {
            if(result.value)
            {
                request(data).done(function (r) {
                    var response = JSON.parse(r);

                    if (response.result !== 0) {
                        DOM.domHandler(data, response, callBack);
                    }
                    else {
                        var alert = {
                            type: "error",
                            title: "No se puede ejecutar la operacion",
                            text: "Por el momento no se puede ejecutar esta operacion, si el problema continua contacte con su proveedor"
                        };
                        this.setSingleAlert(alert);
                    }
                });
            }
        });
    },
    getValue: function(data){
        var response = DOM.ajaxRequestSync(data);

        isLoggedIn(response);

        if(response.result !== 0) {
            if(data.action === 'undefined') data.action = '';
            switch (data.action){
                case 'return':
                    return response;
                    break;
                default:
                    (typeof data.selector === 'undefined')? data.selector2.val(response.result) : $(data.selector).val(response.result);
                    break;
            }
        }
    },
    ajaxLoad: function(r) {
        var params = false,
            response = 0;

        if(r.type == "post") {
            params = $(r.form).serialize();
        }

        $.ajax({
            async:  false,
            type:   r.type,
            url:    r.url,
            data:   params,
            success:function(data){
                response = data;
            },
            error:function(){
                response = {
                    result: false,
                    error: '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="fa fa-exclamation-triangle"></i><h3>Advertencia:</h3><p>Hubo problemas en el administrador</p></div>'
                };
            }
        });

        return JSON.parse(response);
    },
    submitData: function (data, callBack) {

        DOM.ajaxRequestEnctype(data, function (r) {
            var response = JSON.parse(r);
            DOM.resultHandler(data, response, callBack);
        });

    },
    ajaxRequest: function(r) {
        var params = (r.type == "post")? $(r.form).serialize() : false;
        return $.ajax({type: r.type, url: r.url, data: params});
    },
    getAppendMultiple: function(data, callBack, callBack2) {
        DOM.ajaxRequest(data).done(function(r) {
            var response = (typeof data.type === "undefined")? data : JSON.parse(r);
            isLoggedIn(response);

            if(response.result !== 0) {
                for(var element in response.result) {
                    $(element).html(response.result[element]);
                }

                if(callBack) {
                    callBack();
                }

                if(callBack2){
                    callBack2();
                }
            }
        });
    },
    resultHandler: function(data, response, callBack, errorCallBack){

        isLoggedIn(response);

        if (response.result !== 0) {
            DOM.domHandler(data, response, callBack);
        } else {

            if(typeof handler !== "undefined") {
                $(document).on('click', '#submit', handler);
                count = 2;
            }

            var messageError = (typeof data.messageError === 'undefined')? '.response' : data.messageError;
            $(messageError).html(response.error);
            $('html, body').animate({scrollTop:0}, 100, 'linear');

            Ladda.stopAll();

            setTimeout(function(){$('.saving').css('background-color', '#d9534f').fadeOut(1500);}, 1000);

            if(errorCallBack) {
                errorCallBack(response);
            }
        }
    },
    domHandler: function (data, response, callBack) {

        var selector = $(data.selector);

        switch (data.doAfter) {

            case "login":
                window.location = getUrl.baseUrl()+response.controller;
                break;

            case "redirect":
                var url = (typeof response.url === "undefined") ? data.returnUrl : response.url;
                if (typeof data.showAlert !== "undefined" && data.showAlert == true) {
                    this.setSingleAlert({type: "success", title: data.titleResponse, text: data.textResponse});
                }
                window.location = url;
                break;

            case "append":
                selector.append(response.view);
                break;

            case "prepend":
                selector.prepend(response.view);
                break;

            case 'before':
                selector.before(response.result);
                break;

            case 'insertAfter':
                selector.after(response.result);
                break;

            case "remove":
                selector.remove();
                break;

            case "moda_hidden":
                $(data.modal).modal('hide');
                break;

            case "inline":
                setTimeout(function(){$('.saving').css('background-color', '#71B842').fadeOut(1500);}, 1000);
                break;

            case "html":
                selector.html(response.view);

                if(typeof response.mod !== "undefined"){
                    var dataMod = {result : response.mod};
                    DOM.getAppendMultiple(dataMod);
                }

                break;

            case "calendar":
                selector.fullCalendar('refetchEvents');
                selector.fullCalendar('rerenderEvents');
                selector.fullCalendar('refetchResources');
                $('.popover').popover('hide');
                $(data.modal).modal('toggle');
                break;

            case "datatable":
                if (data.showAlert == true) {
                    this.setSingleAlert({type: "success", title: data.titleResponse, text: data.textResponse});
                    data.table.ajax.reload();
                    $(data.modal).modal('hide');
                    Ladda.stopAll();
                }
                break;

            case "datatable-reload":
                data.table.ajax.reload();
                $(data.modal).modal('toggle');
                break;

            case "import":
                data.table.ajax.reload();
                $('#import-msg').html(response.msg);
                $(data.modal).modal('toggle');
                break;

            case "showAlert":
                if (data.showAlert == true) {
                    this.setSingleAlert({type: "success", title: data.titleResponse, text: data.textResponse});
                    $(data.modal).modal('toggle');
                    Ladda.stopAll();
                }
                break;

            case "changePassword":
                swal({
                    title: "",
                    text: 'Contraseña reestablecida correctamente. Sera reedirigido a la pantalla inicial en unos momentos.',
                    type: "success",
                    showConfirmButton: false,
                    timer: 4000
                }).then(function () {
                    window.location = getUrl.baseUrl() + 'login/';
                });
                break;

            default:
                selector.html(response.result);
        }

        Ladda.stopAll();
        $(data.modal).modal('toggle');
        // Handle any callback function that was sent
        if(typeof callBack !== "undefined") {
            callBack();
        }

    },
    ajaxRequestSync: function (r) {

        var params = false,
            response = 0;

        if(r.type == "post") {
            params = $(r.form).serialize();
        }

        $.ajax({
            async:  false,
            type:   r.type,
            url:    r.url,
            data:   params,
            success:function(data){
                response = data;
            },
            error:function(){
                response = {
                    result: false,
                    error: '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="fa fa-exclamation-triangle"></i><h3>Advertencia:</h3><p>Hubo problemas en el administrador</p></div>'
                };
            }
        });

        return JSON.parse(response);
    },
    ajaxRequestEnctype: function (data, success) {
        $(data.form).ajaxSubmit({success: success});
    }
};

var isLoggedIn = function(response) {
    var skip = ["login", "session"],
        path = getUrl.urlPath().split("/");
    if(inArray(path[0], skip) == false) {
        if (response.is_logged_in == false) {
            swal({
                title: "Atención",
                text: 'Has sido desconectado. Por favor Iniciar sesión.',
                type: "warning",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Login",
                confirmButtonClass: "ladda-button",
                closeOnConfirm: false
            }).then(result =>{
                if(result.value){
                window.location = getUrl.baseUrl() + 'login/' + getUrl.urlPath();
                }
            });
        }
    }
},
/**
 *
 * @type {{itemCalculate: itemCalculate}}
 */

Calculate = {
    itemCalculate :function (data) {
        var input = 0,
            total = 0;

        $(data.selector).each(function (index, row) {
            var input  = parseFloat(clearAmount($(row).find(data.input).val()));
                total += (isNaN(input))? 0 : parseFloat(input);
        });

        return {
            totalResult: parseFloat(total).toFixed(2)
        };
    }
};

/**
 *
 * @type {{baseUrl: getUrl.baseUrl, urlPath: getUrl.urlPath}}
 */

var getUrl = {
    baseUrl: function(){
        var host = location.hostname + '/',
            path = location.protocol + '//' +location.hostname + '/';

        if(host == 'localhost/') {
            path += "payday/";
        }

        return path;
    },
    urlPath: function(){
        var path = window.location.pathname;

        if(window.location.host == 'localhost') {
            path = path.replace('/payday/', '');
        }

        return path;
    }
};

var inArray = function(value, array) {
    return array.indexOf(parseInt(value)) > -1;
};