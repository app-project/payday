
$(document).ready(function(){

    var oTable = $('#list').DataTable({
        "ajax": getUrl.baseUrl()+'compulsive_payments/datatable',
        "order": [[ 0, "desc" ]],
        "language": {
            "url": getUrl.baseUrl()+"assets/plugins/datatables/language/spanish.json"
        },
        "columns": [
            { "data": "loan_analysiId",     "sClass": "dt-loan_analysiId col-hidden",  "width": "0",       "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "full_name",          "sClass": "dt-full_name",                  "width": "20%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "payment_day_name",   "sClass": "dt-payment_day_name",           "width": "10%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "requested_amount",   "sClass": "dt-requested_amount",           "width": "15%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "payment_day_total",  "sClass": "dt-payment_day_total",          "width": "15%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "bank",               "sClass": "dt-bank",                       "width": "20%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "status",             "sClass": "dt-status",                     "width": "10%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "option",             "sClass": "dt-option",                     "width": "10%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
        ],
        "createdRow": function(row, data){
            $('.dt-full_name', row).html(clientData(data));
            // $('.dt-requested_amount', row).html(formatCurrency(data.requested_amount));
            $('.dt-payment_day_total', row).html('$'+formatCurrency(parseFloat(data.payment_day_total)));
            $('.dt-status', row).html(status(data));
            $('.dt-option', row).html(options(data));
        }
    });

    $(document).on('click', '.trigger_add_row', function () {
        var rowTable    = $('#row-table-hidden .row-item').clone(),
            capital     = parseFloat($('#capital-result').val());

        if(capital > 0){
            $('.last_row').before(rowTable).promise().done(function () {
                if($('.numeric-decimal').length > 0){$('.numeric-decimal').numeric({negative : false, decimalPlaces: 2 });}
                if($('.date').length > 0){$('.date').daterangepicker({singleDatePicker: true, locale: {format: 'YYYY-MM-DD'}});}
                compulsivePayment();
            });
        }else{
            swal('Atención!', 'El Cliente a Finalizado el Prestamo.', 'warning');
        }
    });

    $(document).on('click', '.trigger_remove_row', function () {
        var item        = $(this).closest('.row-item'),
            id          = parseInt($(this).data('id')),
            count       = $('.table tr.row-item').length;

        if(count > 1) {
            if(id === 'undefined' || id == 0){
                item.remove();
                compulsivePayment();
            }else{
                swal({
                    title: 'Atención',
                    text: 'Esta seguro que desea borrar este item?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Borrar',
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'Cancelar',
                }).then((result) => {
                    if (result.value == true) {
                        DOM.submit({type:'get', url:getUrl.baseUrl()+'compulsive_payments/hide_items/'+id, doAfter:'remove', selector: item},function () {
                            compulsivePayment();
                        });
                    }
                });
            }
        } else {

            swal('Atención', 'No puedes borrar todos los registros', 'warning');
        }
    });

    $(document).on("click", '#button-submit', function(e) {
        e.preventDefault();
        var capital     = parseFloat($('#capital-result').val()),
            optionData  = $(this).data(),
            data_upadte = {type:'get', url:getUrl.baseUrl()+'compulsive_payments/update_status_compulsive/'+ parseInt(optionData.load_id)},
            data        = {type: "post", url: $('#form').attr('action'), form: "#form", doAfter: "html", selector: '#container-payments', table: oTable, modal:"#compulsive_payments_view", messageError: ".modal-response", showAlert: true, titleResponse: "Exito!", textResponse: "Datos Registrados Correctamente"};

            DOM.submit(data);

        if(capital == 0){
            DOM.submit(data_upadte);
        }

    });

    $(document).on('click', '#option-filter',function () {
       var $thiSelector = $(this),
           option       = parseInt($thiSelector.val()),
           data         = {};

       switch (option)
       {
           case 1:
                 data  = {type: "get", url: getUrl.baseUrl()+'compulsive_payments/compulsive_status/'+option, doAfter: "html", selector: '#container-payments'};
               break;
           case 2:
                 data  = {type: "get", url: getUrl.baseUrl()+'compulsive_payments/compulsive_status/'+option, doAfter: "html", selector: '#container-payments'};
               break;
           case 3:
                 data  = {type: "get", url: getUrl.baseUrl()+'compulsive_payments/compulsive_status/'+option, doAfter: "html", selector: '#container-payments'};
               break;
           case 4:
                 data  = {type: "get", url: getUrl.baseUrl()+'compulsive_payments/compulsive_status/'+option, doAfter: "html", selector: '#container-payments'};
               break;
       }

        DOM.submit(data);
    });

    /*-----------------------------------------------------------------------------*/
    var compulsivePayment = function(){
        var $this          = $(this),
            capital         = 0,
            interest        = 0,
            latePayment     = 0,
            latePaymentR    = 0,
            result_res      = 0,
            capital_result  = 0,
            result          = 0;

        $('#compulsive-payments .row-item').each(function (index, row) {

            capital         = parseFloat(clearAmount($(row).find('.capital').val()));
            interest        = $(row).find('.interest').val();
            latePayment     = $(row).find('.late-payment').val();
            latePayment     = clearAmount(latePayment);
            latePayment     = parseFloat(latePayment);
            interest        = clearAmount(interest);
            interest        = parseFloat(interest);
            latePaymentR    = (latePayment == '0.00')? interest :  latePayment + interest;

            result          =  capital - latePaymentR;
            result_res      = (result.toFixed(2) > 0)? result.toFixed(2) : 0;
            capital_result  = (capital.toFixed(2) > 0)? capital.toFixed(2) : 0;

            $(row).next().find('.capital').val(formatCurrency(result_res));
            $(row).next().find('.interest').val(formatCurrency(latePaymentR));
            $('#capital-result').val(capital_result);

        });
    };

    /**
     * FUNCTIONS
     *
     */
    var options = function(data){
        var id          = data.loan_analysiId,
            preview     = getUrl.baseUrl()+"compulsive_payments/preview/"+id,
            html        = '';

        html += '<div class="btn-group" role="group">\n' +
            '    <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n' +
            '      Opciones' +
            '    </button>\n' +
            '    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">\n' +
            '      <a class="dropdown-item modal_trigger" href="javascript:void(0)" data-url="'+preview+'" data-toggle="modal" data-target="#preview_compulsive"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Vista Previa</a>\n' +
            '    </div>\n' +
            '  </div>';

        return html;
    };

    var clientData = function (data) {
        var html = '<span><b>'+data.full_name+'</b></span>'+
            '<span class="document-description" style="display: block">'+data.document+'</span>';

        return html;
    };

    var status = function (data) {
        return '<label class="label label-status border active-status" style="background-color: '+data.class+'">'+data.status+'</label>';
    };
});

