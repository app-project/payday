$(document).ready(function(){

    $(document).on("submit", '#form', function(e) {
        e.preventDefault();
        var data = {type: "post", url: $('#form').attr('action'), form: this, doAfter: "showAlert", messageError: ".modal-response", showAlert: true, titleResponse: "Exito!", textResponse: "Registrado Correctamente."};
        DOM.submitData(data);
    });

});
