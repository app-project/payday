$(document).ready(function(){

    var oTable = $('#list').DataTable({
        "ajax": getUrl.baseUrl()+'institution_banks/datatable',
        "order": [[ 0, "desc" ]],
        "language": {
            "url": getUrl.baseUrl()+"assets/plugins/datatables/language/spanish.json"
        },
        "columns": [
            { "data": "bankId",         "sClass": "dt-bankId col-hidden", "width": "0",       "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "code",           "sClass": "dt-code col-hidden",   "width": "0",       "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "name",           "sClass": "dt-name",              "width": "30%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "description",    "sClass": "dt-description",       "width": "35%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "image",          "sClass": "dt-image col-hidden",  "width": "0",       "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "status",         "sClass": "dt-status",            "width": "10%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "option",         "sClass": "dt-option",            "width": "15%",     "defaultContent": "<span class='text-muted'>N/A</span>"}
        ],
        "createdRow": function(row, data){
            $('.dt-name', row).html(bankDetails(data));
            $('.dt-status', row).html(status(data));
            $('.dt-option', row).html(options(data));
        }
    });

    /**
     * Event
     */

    $(document).on('keyup', '#name_bank',function () {
        $('.title-input').html($(this).val());
    });

    $(document).on("submit", '#form', function(e) {
        e.preventDefault();
        var data = {type: "post", url: $('#form').attr('action'), form: this, doAfter: "datatable", table: oTable, modal:"#modal_bank", messageError: ".modal-response", showAlert: true, titleResponse: "Exito!", textResponse: "Instituciones Bancarias Registrado Correctamente"};
        DOM.submitData(data);
    });

    $(document).on("click", '#button-submit-update', function(e) {
        e.preventDefault();
        var data = {type: "post", url: $('#form').attr('action'), form: "#form", doAfter: "datatable", table: oTable, modal:"#modal_bank", messageError: ".modal-response", showAlert: true, titleResponse: "Exito!", textResponse: "Datos Actualizados Correctamente"};
        DOM.submitData(data);
    });
    /*-----------------------------------------------------------------------------*/

    /**
     * FUNCTIONS
     *
     */

    var status = function (data) {
        return (data.status == 1)? '<label class="label label-status border active-status">Activo</label>' : '<label class="label label-status border active-status" style="background-color: #bb0000;">Inactivo</label>';
    };

    var options = function(data){
        var id          = data.bankId,
            url_edit    = getUrl.baseUrl()+"institution_banks/edit/"+id,
            url_delete  = getUrl.baseUrl()+"institution_banks/delete/"+id,
            hasModifier = (data.exclude == 0)? 'aria-disabled="true" disabled' : '',
            html        = '';

        html += '<div class="btn-group" role="group">\n' +
            '    <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" '+hasModifier+' data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n' +
            '      Opciones' +
            '    </button>\n' +
            '    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">\n' +
            '      <a class="dropdown-item modal_trigger" href="javascript:void(0)" data-url="'+url_edit+'" data-toggle="modal" data-target="#modal_bank"><i class="fa fa-edit" aria-hidden="true"></i> Editar</a>\n' +
            '      <a class="dropdown-item delete_dt" href="javascript:void(0)" data-url="'+url_delete+'" data-id="'+id+'"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</a>\n' +
            '    </div>\n' +
            '  </div>';

        return html;
    };

    var bankDetails = function(data){
        var name  = data.name,
            code  = data.code,
            img   = ($.isEmptyObject(data.image) != true) ? 'assets/storage/banks/'+data.image : 'assets/images/avatars/bank.png';

        return '<span style="float: left; margin-right:10px;"><img alt="image" style="max-width:40px; height:auto;" src="'+getUrl.baseUrl()+img+'" /></span>' +
               '<strong>'+name+'</strong>' +
               '<span class="document-description">Codigo: '+code+'</span>';
    };
});
