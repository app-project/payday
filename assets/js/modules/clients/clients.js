
$(document).ready(function(){

    var oTable = $('#list').DataTable({
        "ajax": getUrl.baseUrl()+'clients/datatable',
        "order": [[ 0, "desc" ]],
        "language": {
            "url": getUrl.baseUrl()+"assets/plugins/datatables/language/spanish.json"
        },
        "columns": [
            { "data": "clientId",       "sClass": "dt-clientId col-hidden",     "width": "0",           "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "document",       "sClass": "dt-document col-hidden",     "width": "0",           "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "first_name",     "sClass": "dt-full_name",               "width": "30%",         "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "last_name",      "sClass": "dt-last_name col-hidden",    "width": "10%",         "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "salary",         "sClass": "dt-salary text-right",       "width": "12%",         "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "bank_name",      "sClass": "dt-bank_name",               "width": "25%",         "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "phone",          "sClass": "dt-phone",                   "width": "13%",         "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "image",          "sClass": "dt-image col-hidden",        "width": "0",           "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "option",         "sClass": "dt-option",                  "width": "10%",         "defaultContent": "<span class='text-muted'>N/A</span>"},
        ],
        "createdRow": function(row, data){
            $('.dt-full_name', row).html(clientData(data));
            $('.dt-salary', row).html('$'+formatCurrency(data.salary));
            $('.dt-option', row).html(options(data));
        }
    });

    $('#timepicker-AM, #timepicker-PM').datetimepicker({
        format: 'LT'
    });

    $(document).on('click', '.trigger_add_row', function () {
        var rowTable = $('#questions_hidden .form-group').clone();
        $('.last_row').before(rowTable).promise().done(function () {
            countRow();
        });
    });

    $(document).on('keyup', '#first_name', function() {
        $('.breadcrumb-item span').html('/ ' + $('#first_name').val() + " " + $('#last_name').val());
    });

    $(document).on('keyup', '#last_name', function() {
        $('.breadcrumb-item span').html('/ ' + $('#first_name').val() + " " + $('#last_name').val());
    });

    $(document).on('click', '.trigger_remove_row', function () {
        var item        = $(this).closest('.form-group'),
            count       = $('#security_questions .form-group').length;
        if(count > 1) {
            item.remove();
            countRow();
        } else {
            swal('Atención', 'No puedes borrar todos los registros', 'warning');
        }
    });

    $(document).on('change', '#requested_amount', function () {
        if((parseInt($(this).val()) == 0) || (parseFloat($(this).val()) == 0.00)){
            swal('Atención', 'el monto no puede ser cero (0).', 'warning');
            $(this).val('');
        }
    });

    $(document).on('click', '.unlink-file', function(){

        var selector    = $(this),
            url         = selector.data('url'),
            data        = {method : 'get', url : url, selector : '#client-documents', doAfter : 'html'};

        DOM.setRequestAlert(data);
    });

    var form = $('#form').formValid({
        fields: {
            "document": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            },
            "first_name": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            },
            "last_name": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            },
            "email": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    },
                    {
                        "type": "email",
                        "message": "Formato de email incorrecto"
                    }
                ]
            },
            "phone": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            },
            "day_payment": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            },"bank_account": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            },"user_bank": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            },"password_bank": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            },"password_transactional": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            },"street": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            },"residency": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            },"sector": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            },"province": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            },"entering_by": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            },"between_the_street": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            },"reference_point": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            },"requested_amount": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            }
        }
    });

    form.keypress(300);

    $(document).on("click", '#button-submit', function(e) {
        form.test();
        e.preventDefault();

        if (form.errors() != 0) {
            Ladda.stopAll();
        }else{
            var data = {type: "post", url: $('#form-clients').attr('action'), form: "#form-clients", doAfter: "redirect", messageError: ".response"};
            DOM.submitData(data);
        }
    });

    $(document).on("click", '#save-b', function(e) {
        e.preventDefault();
        var data = {type: "post", url: getUrl.baseUrl()+'clients/import_save', form: "#form-import", doAfter: "import", modal: '#import_clientes', table: oTable, messageError: ".response"};
        DOM.submit(data);
    });

    $(document).on('change', '#document', function () {
        if(isValidCedula($(this).val()) == 0){
            $(this).val('');
            swal('Error!', 'La Cedula ingresada no es valida', 'error');
        }
    });

    $(document).on('change', '#payment_dayId', function () {
        var thisSelector = $(this);
        if(parseInt(thisSelector.val()) == 1){
            $('#day_work_2').addClass('hidden');
            $('#day_work_3').addClass('hidden');
        }

        if(parseInt(thisSelector.val()) == 2){
            $('.day_work').removeClass('hidden');
            $('#day_work_1').removeClass('hidden');
            $('#day_work_2').removeClass('hidden');

        }else{
            $('#day_work_3').addClass('hidden');
        }

        if(parseInt(thisSelector.val()) == 3){
            $('.day_work').removeClass('hidden');
            $('#day_work_1').removeClass('hidden');
            $('#day_work_2').removeClass('hidden');
            $('#day_work_3').removeClass('hidden');

        }else{
            $('#day_work_3').addClass('hidden');
        }
    });

    var options = function(data){
        var id              = data.clientId,
            url_edit        = getUrl.baseUrl()+"clients/edit/"+id,
            url_delete      = getUrl.baseUrl()+"clients/delete/"+id,
            preview         = getUrl.baseUrl()+"clients/preview/"+id,
            docs            = getUrl.baseUrl()+"clients/docs_preview/"+id,
            html            = '';

        html += '<div class="btn-group" role="group">\n' +
            '    <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n' +
            '      Opciones' +
            '    </button>\n' +
            '    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">\n' +
            '      <a class="dropdown-item" href="'+url_edit+'" ><i class="fa fa-edit" aria-hidden="true"></i> Editar</a>\n' +
            '      <a class="dropdown-item modal_trigger" href="javascript:void(0)" data-url="'+preview+'" data-toggle="modal" data-target="#clients_preview"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Vista Previa</a>\n' +
            '      <a class="dropdown-item modal_trigger" href="javascript:void(0)" data-url="'+docs+'" data-toggle="modal" data-target="#docs_clients"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Vista Documentos</a>\n' +
            '      <a class="dropdown-item delete_dt" href="javascript:void(0)" data-url="'+url_delete+'" data-id="'+id+'"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</a>\n' +
            '    </div>\n' +
            '  </div>';

        return html;
    };

    var countRow = function () {
        $('#security_questions .form-group label.count').each(function (i) {
            $(this).text(i + 1);
        });
    };

    // var clientData = function (data) {
    //     var html = '<span><b>'+data.first_name+ ' ' +data.last_name+'</b></span>'+
    //         '<span class="document-description" style="display: block">'+data.document+'</span>';
    //
    //     return html;
    // };

    var clientData = function(data){

        var fullname    = data.first_name + ' ' + data.last_name,
            img         = ($.isEmptyObject(data.image) != true) ? 'assets/storage/avatars/'+data.image : 'assets/images/avatars/user.png';

        return '<span style="float: left; margin-right:10px;"><img alt="image" style="max-width:40px; height:auto;" src="'+getUrl.baseUrl()+img+'" /></span>\n' +
            '                                                <span><strong>'+fullname+'</strong>\t\t\t\t\t\t\t<br /></span>\n' +
            '                                                <small><span class="document-description" style="display:block">'+data.document+'</span></small>';
    };
});
