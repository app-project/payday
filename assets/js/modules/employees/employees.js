$(document).ready(function(){
    
    var oTable = $('#list').DataTable({
        "ajax": getUrl.baseUrl()+'employees/datatable',
        "order": [[ 0, "desc" ]],
        "language": {
            "url": getUrl.baseUrl()+"assets/plugins/datatables/language/spanish.json"
        },
        "columns": [
            { "data": "userId",         "sClass": "dt-userId col-hidden",       "width": "0",       "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "first_name",     "sClass": "dt-first_name",              "width": "60",      "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "last_name",      "sClass": "dt-last_name col-hidden",    "width": "0",       "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "user_type",      "sClass": "dt-user_type",               "width": "20%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "email",          "sClass": "dt-email col-hidden",        "width": "0",       "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "option",         "sClass": "dt-option",                  "width": "20%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
        ],
        "createdRow": function(row, data){
            $('.dt-first_name', row).html(usersDetails(data));
            $('.dt-user_type', row).html(userType(data));
            $('.dt-option', row).html(options(data));
        }
    });

    /**
     * Validations
     *
     */
    var form = $('#form').formValid({
        fields: {
            "first_name": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            },
            "last_name": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            },
            "email": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    },
                    {
                        "type": "email",
                        "message": "Formato de email incorrecto"
                    }
                ]
            },
            "password": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            }
        }
    });

    form.keypress(300);

    /**
     * Event
     */
    $(document).on("click", '#button-submit-user', function(e) {
        form.test();
        e.preventDefault();

        if (form.errors() != 0) {
            Ladda.stopAll();
        }else{
            var data = {type: "post", url: $('#form').attr('action'), form: "#form", doAfter: "datatable", table: oTable, modal:"#modal_user", messageError: ".modal-response", showAlert: true, titleResponse: "Exito!", textResponse: "Empleado Registrado Correctamente"};
            DOM.submitData(data);
        }
    });

    $(document).on("click", '#button-submit-update-users', function(e) {
        form.test();
        e.preventDefault();

        if (form.errors() != 0) {
            Ladda.stopAll();
        }else{
            var data = {type: "post", url: $('#form').attr('action'), form: "#form", doAfter: "datatable", table: oTable, modal:"#modal_user", messageError: ".modal-response", showAlert: true, titleResponse: "Exito!", textResponse: "Datos Actualizados Correctamente"};
            DOM.submitData(data);
        }
    });

    $(document).on('keyup', '#first_name',function () {
        $('.title-input').html($(this).val());
    });

    $(document).on('keyup', '#last_name',function () {
        $('.title-input-p').html($(this).val());
    });

     /*-----------------------------------------------------------------------------*/

    /**
     * FUNCTIONS
     *
     */

    var userType = function (data) {
        return (data.user_type == 1)? 'Administrador' : 'Empleado';
    };

    var usersDetails = function(data){

        var fullname    = data.first_name + ' ' + data.last_name,
            preview     = getUrl.baseUrl()+"employees/employee_view/"+data.userId,
            email       = data.email,
            img         = ($.isEmptyObject(data.image) != true)  ? 'assets/storage/avatars/'+data.image : 'assets/images/avatars/user.png';

        return '<span style="float: left; margin-right:10px;"><img alt="image" style="max-width:40px; height:auto;" src="'+getUrl.baseUrl()+img+'" /></span>\n' +
            '                                                <a href="javascript:void(0)" class="modal_trigger" data-url="'+preview+'" data-toggle="modal" data-target="#preview_users_empployes"><strong>'+fullname+'</strong>\t\t\t\t\t\t\t<br /></a>\n' +
            '                                                <small><a href="mailto:'+email+'" style="color:#545b62">'+email+'</a></small>';
    };

    var options = function(data){
        var id          = data.userId,
            url_edit    = getUrl.baseUrl()+"employees/edit/"+id,
            preview     = getUrl.baseUrl()+"employees/employee_view/"+id,
            url_delete  = getUrl.baseUrl()+"employees/delete/"+id,
            html        = '';

        html += '<div class="btn-group" role="group">\n' +
                '    <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n' +
                '      Opciones' +
                '    </button>\n' +
                '    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">\n' +
                '      <a class="dropdown-item modal_trigger" href="javascript:void(0)" data-url="'+url_edit+'" data-toggle="modal" data-target="#modal_user"><i class="fa fa-edit" aria-hidden="true"></i> Editar</a>\n' +
                '      <a class="dropdown-item modal_trigger" href="javascript:void(0)" data-url="'+preview+'" data-toggle="modal" data-target="#preview_users"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Preview</a>\n' +
                '      <a class="dropdown-item delete_dt" href="javascript:void(0)" data-url="'+url_delete+'" data-id="'+id+'"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</a>\n' +
                '    </div>\n' +
                '  </div>';

        return html;

    };
});
