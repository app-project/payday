$(document).ready(function() {

    /* initialize the external events
    -----------------------------------------------------------------*/
    $('#external-events .fc-event').each(function() {
        // store data so the calendar knows to render an event upon drop
        $(this).data('event', {
            title: $.trim($(this).text()), // use the element's text as the event title
            stick: true // maintain when user navigates (see docs on the renderEvent method)
        });
    });

    /* initialize the calendar
    -----------------------------------------------------------------*/
    loadCalendar();

    $(document).on('keyup', '#name', function() {
        $('.list-title-calendar').html($('#name').val());
    });

    $(document).on('keyup', '#description', function() {
        $('.list-title-event').html($('#description').val());
    });

    $(document).on('click', '.frecuency-week label', function () {
        if($(this).prev('input').is(':checked')) {
            $(this).prev('input').prop('checked', false);
        }else{
            $(this).prev('input').prop('checked', true);
        }
    });

    $(document).on("click", "#all_day", function () {
        if($(this).prop('checked') == true) {
            $('.hour').addClass('hidden');
        }else {
            $('.hour').removeClass('hidden');
        }
    });

    $(document).on("change", "#frequencyId", function () {
        var id      = $(this).val(),
            text    = $('#frequencyId option:selected').data('text');

        if(id != 0) {
            $('.period-label').html(text);
            $('.frecuency-data').removeClass('hidden');
        } else {
            $('.frecuency-data').addClass('hidden');
        }

        if(id == 'WEEKLY') {
            var d = new Date();
            $('.frecuency-week').removeClass('hidden');
            $('#weekday_' + d.getDay()).prop('checked', true);
            $('#repeat_every option[value=0]').text('Todas las').trigger("chosen:updated");
        }else {
            $('.frecuency-week').addClass('hidden');
            $('.weekday').prop('checked', false);
            $('#repeat_every option[value=0]').text('Todos los').trigger("chosen:updated");
        }
    });

    $(document).on('click', function (e) {
        if(!$(e.target).closest('.popover').length) {
            $('.popover').each(function () {
                $(this).popover('dispose');
            });
        }
    });

    $(document).on('click', '.popover-close, .popover-hide', function (e) {
        $('.event-item').popover('dispose');
        e.stopPropagation();
    });

    $(document).on('click', '.cal-edit-event', function (e) {
        $('.event-item').popover('dispose');
        var url     = $(this).data('url'),
            data    = {type: 'get', url: url, selector: '#calendar-event .modal-dialog', doAfter: 'html'};

        $('#calendar-event .modal-dialog').html('');

        DOM.getAppend(data);

        $('#calendar-event').modal();
    });

    var form = $('#form-type').formValid({
        fields: {
            "name": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            },
            "color": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            }
        }
    });

    form.keypress(300);

    $(document).on("click", '#button-submit', function(e) {
        form.test();
        e.preventDefault();

        if (form.errors() != 0) {
            Ladda.stopAll();
        }else{
            var data = {type: "post", url: $('#form-type').attr('action'), form: "#form-type", doAfter: "html", modal:"#calendar-type", messageError: ".modal-response", selector: "#calendar-menu"};
            DOM.getAppend(data);
            loadCalendar();
        }
    });

    var form_event = $('#form-event').formValid({
        fields: {
            "description": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            },
            "calendarId": {
                "required": true,
                "tests": [
                    {
                        "type": "null",
                        "message": "Este campo es requerido"
                    }
                ]
            }
        }
    });

    form_event.keypress(300);

    $(document).on("click", '#button-submit-event', function(e) {
        form_event.test();
        e.preventDefault();

        if (form_event.errors() != 0) {
            Ladda.stopAll();
        }else{
            var data = {type: "post", url: $('#form-event').attr('action'), form: "#form-event", doAfter: "calendar", modal:"#calendar-event", messageError: ".modal-response", selector: $('#calendar')};
            DOM.submit(data);
        }
    });

    $(document).on('click', '.cal-del-event', function (e) {
        var url = $(this).data('url');
        DOM.submit({type: 'get', url: url, selector: $('#calendar'), doAfter: 'calendar', btn: $(this)});
    });

    $(document).on('click', '.delete_calendar', function (e) {
        e.preventDefault();

        var selector    = $(this),
            id          = selector.data('id'),
            url         = selector.data('route'),
            data        = {url : baseUrl+url+'/'+id, doAfter : "html", selector: "#calendar-menu"};

        DOM.setRequestAlert(data, loadCalendar);
    });
});

var loadCalendar = function () {

    var default_date    = $('#defaultDate').val();

    $('#calendar').fullCalendar('destroy');
    $('#calendar').fullCalendar({
        header: {left: 'prev,next today', center: 'title', right: 'month,agendaWeek,agendaDay,listMonth'},
        columnFormat: 'dddd',
        firstDay: 0,
        cache: true,
        defaultDate: default_date,
        locale: 'es',
        buttonIcons: true,
        navLinks: false,
        eventLimit: true,
        events: function (start, end, timezone, callback) {
            var events = [];
            $.ajax({
                url: baseUrl + 'calendar/events/json',
                dataType: 'json',
                data: {
                    start: start.format(),
                    end: end.format()
                },
                type: 'POST',
                success: function (doc) {
                    $(doc).each(function () {
                        events.push({
                            id: $(this).attr('id'),
                            title: $(this).attr('title'),
                            date: $(this).attr('date'),
                            calendar: $(this).attr('calendar'),
                            amount: $(this).attr('amount'),
                            start: $(this).attr('start'),
                            color: $(this).attr('color'),
                            className: $(this).attr('className'),
                            url: $(this).attr('url')
                        });
                    });
                    callback(events);
                },
                error: function () {
                    loadCalendar();
                }
            });
            return events;
        },
        dayClick: function (date) {
            var date    = ($.fullCalendar.formatDate(date, "YYYY-MM-DD")),
                url     = baseUrl +'calendar/add_event/'+ date,
                data    = {type: 'get', url: url, selector: '#calendar-event .modal-dialog', doAfter: 'html'};

            DOM.getAppend(data);
            $('#calendar-event').modal();
        },
        eventClick: function (event) {
            var selector = this;
            $('.event-item').popover('dispose');

            $.get(event.url, function (r) {
                var data = $.parseJSON(r);
                $(selector).popover({
                    html: true,
                    //title: data.title,
                    content: data.content,
                    placement: 'right'
                }).popover('show');
                $('.popover').addClass('more_width');
            });
            return false;
        }
    });
};