
$(document).ready(function(){

    var oTable = $('#list').DataTable({
        "ajax": getUrl.baseUrl()+'loan_analysis/datatable',
        "order": [[ 0, "desc" ]],
        "language": {
            "url": getUrl.baseUrl()+"assets/plugins/datatables/language/spanish.json"
        },
        "columns": [
            { "data": "loan_analysiId",   "sClass": "dt-loan_analysiId col-hidden",   "width": "0",       "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "request_code",     "sClass": "dt-request_code",                "width": "10%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "sent_mail",        "sClass": "dt-sent_mail",                   "width": "20%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "full_name",        "sClass": "dt-full_name",                   "width": "20%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "date_issue",       "sClass": "dt-date_issue",                  "width": "10%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "total_deposit",    "sClass": "dt-total_deposit",               "width": "10%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "quota_monthly",    "sClass": "dt-quota_monthly",               "width": "10%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "biweekly_quota",   "sClass": "dt-biweekly_quota",              "width": "10%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "weekly_quota",     "sClass": "dt-weekly_quota",                "width": "10%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "status",           "sClass": "dt-status",                      "width": "10%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
            { "data": "option",           "sClass": "dt-option",                      "width": "10%",     "defaultContent": "<span class='text-muted'>N/A</span>"},
        ],
        "createdRow": function(row, data){
            $('.dt-request_code', row).html(requestCode(data));
            $('.dt-total_deposit', row).html('$'+formatCurrency(data.total_deposit));
            $('.dt-quota_monthly', row).html('$'+formatCurrency(data.quota_monthly));
            $('.dt-biweekly_quota', row).html('$'+formatCurrency(data.biweekly_quota));
            $('.dt-weekly_quota', row).html('$'+formatCurrency(data.weekly_quota));
            $('.dt-status', row).html(status(data));
            $('.dt-sent_mail', row).html(sentMail(data));
            $('.dt-option', row).html(options(data));
        }
    });

    $(function () {
        itemCalculate();
    });

    $(document).on('change', '#clientId',function () {
        var $thiSelector  = $('option:selected', this),
            clientData    = $thiSelector.data(),
            image         = (clientData.image == '')? '' : clientData.image,
            getImage      = (clientData.image == '')? getUrl.baseUrl()+'assets/storage/avatars/user.png': getUrl.baseUrl()+'assets/storage/avatars/'+clientData.image;

        /**
         * hidden
         */

        $('#clientId-h').val(clientData.clientid);
        $('#apartament_of_house-h').val(clientData.apartament_of_house);
        $('#bankId-h').val(clientData.bankid);
        $('#type_accountId-h').val(clientData.type_accountid);
        $('.title-h').html(clientData.first_name+' '+clientData.last_name+' ('+ clientData.document+')');

        /**
         * disabled
         */

        $('#document').val(clientData.document);
        $('#first_name').val(clientData.first_name);
        $('#last_name').val(clientData.last_name);
        $('#company').val(clientData.company);
        $('#sector_company').val(clientData.sector_company);
        $('#apartament_of_house_1').prop('checked', (clientData.apartament_of_house == 1)? true: false);
        $('#apartament_of_house_2').prop('checked', (clientData.apartament_of_house == 2)? true: false);
        $('#bankId').val(clientData.bankid).trigger("chosen:updated");
        $('#type_accountId').val(clientData.type_accountid).trigger("chosen:updated");
        $('#payment_day_name').html(clientData.payment_name);
        $('#bank_account').val(clientData.bank_account);
        $('#phone').val(clientData.phone);
        $('#mobile').val(clientData.mobile);
        $('#salary').val(clientData.salary);
        $('#post_working').val(clientData.post_working);
        $('#department_working').val(clientData.department_working);
        $('#user_bank').val(clientData.user_bank);
        $('#password_bank').val(clientData.password_bank);
        $('#bank_name-h').val(clientData.bank_name);
        $('#image-h').val(image);
        $('#add-image').attr('src', getImage);


    });

    $(document).on("click", '#button-submit', function(e) {
        e.preventDefault();
        var data = {type: "post", url: $('#form').attr('action'), form: "#form", doAfter: "redirect", messageError: ".response"};
        DOM.submit(data);
    });

    $(document).on('change', '#document', function () {
        if(isValidCedula($(this).val()) == 0){
            $(this).val('');
            swal('Error!', 'La Cedula ingresada no es valida', 'error');
        }
    });

    $(document).on('click', '.trigger_add_row', function () {
        var rowTable = $('#row-table-hidden .row-item').clone();
        $('.last_row').before(rowTable).promise().done(function () {
            countRow();
            if($('.numeric-decimal').length > 0){$('.numeric-decimal').numeric({negative : false, decimalPlaces: 2 });}
            if($('.date').length > 0){$('.date').daterangepicker({singleDatePicker: true, locale: {format: 'YYYY-MM-DD'}});}
        });
    });

    $(document).on('click', '.trigger_remove_row', function () {
        var item        = $(this).closest('.row-item'),
            id          = parseInt($(this).data('id')),
            count       = $('.table tr.row-item').length;

        if(count > 1) {
            if(id === 'undefined' || id == 0){
                item.remove();
                itemCalculate();
            }else{
                swal({
                    title: 'Atención',
                    text: 'Esta seguro que desea borrar este item?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Borrar',
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'Cancelar',
                }).then((result) => {
                    if (result.value == true) {
                        DOM.submit({type:'get', url:getUrl.baseUrl()+'loan_analysis/hide_items/'+id, doAfter:'remove', selector: item});
                        itemCalculate();
                    }
                });
            }

            countRow();

        } else {
            swal('Atención', 'No puedes borrar todos los registros', 'warning');
        }
    });

    $(document).on('change', '.deposit-salary', function () {
        itemCalculate();
    });

    $(document).on('click','#deposit-click',function () {
        if($('#no-deposit-in').hasClass('hidden')){
            $('#no-deposit-in').removeClass('hidden');
            $('#no_deposit_span').addClass('hidden');
        }else{
            $('#no-deposit-in').addClass('hidden');
            $('#no_deposit_span').removeClass('hidden');
        }
    });

    $(document).on('change','#no-deposit-in',function () {

        if($(this).val().length == 0){
            $(this).val(3.5);
        }

        $('#no_deposit_span').html($(this).val());
        itemCalculate();
    });

    $(document).on('change','#fact_percent',function () {

        if($(this).val().length == 0){
            $(this).val(50);
        }
        itemCalculate();
    });

    $(document).on('change','#monthly_fee',function () {
        if($(this).val().length == 0){
            $(this).val(45);
        }
        itemCalculate();
    });

    $(document).on('click','#fact_percent_click',function () {
        if($('#fact_percent').hasClass('hidden')){
            $('#fact_percent').removeClass('hidden');
            $('#fact_percent_span').addClass('hidden');
        }else{
            $('#fact_percent').addClass('hidden');
            $('#fact_percent_span').removeClass('hidden');
        }
    });

    $(document).on('click','#monthly_fee_click',function () {
        if($('#monthly_fee').hasClass('hidden')){
            $('#monthly_fee').removeClass('hidden');
            $('#monthly_fee_span').addClass('hidden');
        }else{
            $('#monthly_fee').addClass('hidden');
            $('#monthly_fee_span').removeClass('hidden');
        }
    });

    //sent mail
    $(document).on('click', '#button-submit-email', function (e) {
        e.preventDefault();
        var data = {type: "post", url: $('#form-mail').attr('action'), form: "#form-mail", doAfter: "datatable-reload", table: oTable, modal:"#sent_mail", messageError: ".modal-response"};
        DOM.submit(data);
    });

    /*-----------------------------------------------------------------------------*/

    /**
     * FUNCTIONS
     *
     */
    var requestCode = function (data) {
        var url = getUrl.baseUrl()+'loan_analysis/preview/'+data.loan_analysiId;
        return '<a href="javacript:void(0)" class="modal_trigger" data-toggle="modal" data-target="#loand_preview" data-url="'+url+'">'+data.request_code+'</a>';
    };

    var status = function (data) {
        return '<label class="label label-status border active-status" style="background-color: '+data.class+'">'+data.status+'</label>';
    };

    var itemCalculate = function () {
        var averageDeposit      = $('#average-deposit-h'),
            totalDeposit        = $('#total_deposit-h'),
            averageFact         = $('#average-fact-h'),
            percentFact         = $('#percent-fact-h'),
            averageDepositS     = $('#average_deposit_span'),
            totales             = $('#totales'),
            averageSalary       = $('#average_salary'),
            averageSalaryFat    = $('#average_salary_fat'),
            factPercent         = $('#fact_percent'),
            factPercentSpan     = $('#fact_percent_span'),
            monthlyFee          = $('#monthly_fee'),
            monthlyFeeSpan      = $('#monthly_fee_span'),
            approvedFact        = $('#approved_fact'),
            quotaMonthly        = $('#quota_monthly'),
            biweeklyQuota       = $('#biweekly_quota'),
            weeklyQuota         = $('#weekly_quota'),
            noDeposit           = parseFloat($('#no-deposit-in').val()),
            data                = {selector:'#deposit .row-item', input:'.deposit-salary'},
            result              = Calculate.itemCalculate(data),
            total               = 0,
            noDep               = 0,
            fat                 = 0,
            fatMonthlyFee       = 0,
            biweeklyQuotaTotal  = 0,
            weeklyQuotaTotal    = 0,
            fat_percent         = 0;

        /**
         * Deposit Promedi
         * @type {number}
         */
        totalDeposit.val(result.totalResult);
        averageFact.val(result.totalResult);
        $('#total_deposit').html('$'+formatCurrency(result.totalResult));
        noDep = (parseFloat(totalDeposit.val()) / noDeposit);
        averageDepositS.html(formatCurrency(noDep.toFixed(2)));

        total = totalDeposit.val() / noDeposit;
        averageDeposit.val(total.toFixed(2));

        /**
         * Factivilidad
         */

        fat             = (result.totalResult / 3);
        fat_percent     = (parseFloat(factPercent.val()) / 100);
        fat_percent     = fat * fat_percent;
        fatMonthlyFee   = (parseFloat(monthlyFee.val()) / 100);
        fatMonthlyFee   = (fat_percent * fatMonthlyFee);

        averageSalary.val(fat.toFixed(2));
        totales.html(formatCurrency(result.totalResult));
        averageSalaryFat.html(formatCurrency(fat.toFixed(2)));
        factPercentSpan.html('$'+formatCurrency(fat_percent.toFixed(2)));
        monthlyFeeSpan.html('$'+formatCurrency(fatMonthlyFee.toFixed(2)));
        approvedFact.html('$'+formatCurrency(fat_percent.toFixed(2)));


        /**
         *Cuotas
         */
        biweeklyQuotaTotal  = (fatMonthlyFee / 2);
        weeklyQuotaTotal    = (fatMonthlyFee / 4);

        quotaMonthly.html('$'+formatCurrency(fatMonthlyFee.toFixed(2)));
        biweeklyQuota.html('$'+formatCurrency(biweeklyQuotaTotal.toFixed(2)));
        weeklyQuota.html('$'+formatCurrency(weeklyQuotaTotal.toFixed(2)));

        /**
         * hidden
         */
        $('#no-deposit-in-h').val($('#no-deposit-in').val());
        $('#average_deposit-h').val(noDep.toFixed(2));
        $('#payment_day_name-h').val($('#payment_day_name').html());
        $('#total_fact-h').val(result.totalResult);
        $('#average_salary_fat-h').val(fat.toFixed(2));
        $('#fact_percent-h').val($('#fact_percent').val());
        $('#fact_percent_cal-h').val(fat_percent.toFixed(2));
        $('#approved_fact-h').val(fat_percent.toFixed(2));
        $('#monthly_fee_percent-h').val($('#monthly_fee').val());
        $('#monthly_fee-h').val(fatMonthlyFee.toFixed(2));
        $('#quota_monthly-h').val(fatMonthlyFee.toFixed(2));
        $('#biweekly_quota-h').val(biweeklyQuotaTotal.toFixed(2));
        $('#weekly_quota-h').val(weeklyQuotaTotal.toFixed(2));
    };

    var countRow = function () {
        $('.table tr.row-item .number-count').each(function (i) {
            $(this).text(i + 1);
        });
    };

    var sentMail = function (data) {
        return (data.sent_mail != 0)? '<i class="fa fa-paper-plane-o"></i>' : '';
    };

    var options = function(data){
        var id              = data.loan_analysiId,
            url_edit        = getUrl.baseUrl()+"loan_analysis/edit/"+id,
            url_delete      = getUrl.baseUrl()+"loan_analysis/delete/"+id,
            preview         = getUrl.baseUrl()+"loan_analysis/preview/"+id,
            receipt         = getUrl.baseUrl()+"loan_analysis/receipt/"+id,
            cancel          = getUrl.baseUrl()+"loan_analysis/cancel_document/"+id,
            sentMail        = getUrl.baseUrl()+"loan_analysis/send_by_mail/"+id+'/'+data.clientId,
            receiptStatus   = [1],
            html            = '';

            html += '<div class="btn-group" role="group">\n' +
                '    <button id="btnGroupDrop1" type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">\n' +
                '      Opciones' +
                '    </button>\n';
            html += '    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">\n';

            if(data.statusId != 1 && data.statusId != 2) {
                html += '      <a class="dropdown-item" href="' + url_edit + '"><i class="fa fa-edit" aria-hidden="true"></i> Editar</a>\n';
            }

            html += '      <a class="dropdown-item modal_trigger" href="javascript:void(0)" data-url="'+preview+'" data-toggle="modal" data-target="#loand_preview"><i class="fa fa-newspaper-o" aria-hidden="true"></i> Vista Previa</a>\n';

            if(data.statusId == 1) {
                html += '      <a class="dropdown-item cancel_document" href="javascript:void(0)" data-url="' + cancel + '"><i class="fa fa-arrows-alt" aria-hidden="true"></i> Cancelar Documento</a>\n';
            }

            if(inArray(data.statusId, receiptStatus)){
                html += '      <a class="dropdown-item" href="'+receipt+'" target="_blank"><i class="fa fa-file-o" aria-hidden="true"></i> Recibo</a>\n';
                html += '      <a class="dropdown-item modal_trigger" href="javascript:void(0)" data-url="'+sentMail+'" data-toggle="modal" data-target="#sent_mail"><i class="fa fa-envelope-o" aria-hidden="true"></i> Enviar Correo</a>\n';
            }

            if(data.statusId != 1){
                html += '      <a class="dropdown-item delete_dt" href="javascript:void(0)" data-url="'+url_delete+'" data-id="'+id+'"><i class="fa fa-trash-o" aria-hidden="true"></i> Eliminar</a>\n';
            }

            html += '    </div>\n';
            html += '  </div>';

        return html;
    };
});
