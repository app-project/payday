var resizefunc  = [],
    baseUrl     = getUrl.baseUrl(),
    module      = $('#module').val();

$('.'+module).addClass('active');

$( document ).ready(function() {
    $(function() {  
        $(".nicescroll").niceScroll({cursorcolor:"#858586"});

        var response   = DOM.getValue({type:"post", url: baseUrl+"calendar/events/json/FALSE/FALSE/FALSE/FALSE/TRUE", action: "return"}),
           actual_date = moment().format("YYYY-MM-DD"),
            i          = 0,
            item       = 0,
            count      = 1;

        if(response.length > 0){
            for(; i < response.length; i++){
                if(actual_date == response[i].date){
                    item++;
                    if(response[i].is_showed == 0){
                        Push.create(response[i].title, {
                            icon: baseUrl+'assets/images/favicon.ico',
                            timeout: 20000,
                            onClick: function () {
                                window.focus();
                                this.close();
                            }
                        });

                        DOM.getValue({type:"post", url: baseUrl+"calendar/showed_event_alert/"+response[i].id, action: "return"});
                    }
                    var html = '<a href="javascript:void(0)" class="dropdown-item notify-item">\n' +
                        '                                <p class="notify-details ml-0">\n' +
                        '                                    <b>'+response[i].title+'</b>\n' +
                        '                                </p>\n' +
                        '                            </a>';

                    $('.events').after(html).promise().done(function () {
                        $('.count-events').html(item);
                        $('.notif-bullet').css('display', 'block');
                    });
                }else{
                    if(count == 1){
                        var html = '<a href="javascript:void(0)" class="dropdown-item notify-item">\n' +
                            '                                <p class="notify-details ml-0">\n' +
                            '                                    <b>No tienes eventos programados para hoy</b>\n' +
                            '                                </p>\n' +
                            '                            </a>';

                        $('.events').after(html).promise().done(function () {
                            $('.count-events').html(0);
                        });
                    }
                    count++;
                }
            }
        }else{
            var html = '<a href="javascript:void(0)" class="dropdown-item notify-item">\n' +
                '                                <p class="notify-details ml-0">\n' +
                '                                    <b>No tienes eventos programados para hoy</b>\n' +
                '                                </p>\n' +
                '                            </a>';

            $('.events').after(html).promise().done(function () {
                $('.count-events').html(0);
            });
        }
	});

    $(document).on('click', '.modal_trigger', function(){
        var selector    = $(this),
            url         = selector.data('url'),
            target      = selector.data('target'),
            data        = {method : 'get', url : url, selector : target + ' .modal-dialog', doAfter : 'html'};

        $('.modal-dialog').html('');

        DOM.getAppend(data, function(){
            // if($('.phone').length > 0){$('.phone').inputmask("999-999-9999");}
            if($('.numeric').length > 0){$('.numeric').numeric({negative:false});}
            $('.select2').chosen({ width: '100%' });
            if($('.date').length > 0){$('.date').daterangepicker({singleDatePicker: true, showDropdowns: true, locale: {format: 'YYYY-MM-DD'}});}
            if($('.numeric-decimal').length > 0){$('.numeric-decimal').numeric({negative : false, decimalPlaces: 2 });}
            Ladda.bind('.ladda-button');
        });
    });

    $(document).on('click', '.delete_dt', function(){
        var selector    = $(this),
            url         = selector.data('url'),
            data        = {url : url, doAfter : 'datatable', table: $('#list').DataTable(), showAlert: true, titleResponse: "Exito!", textResponse: "Datos Eliminados correctamente"};

        DOM.setRequestAlert(data);
    });

    /**
     * Perfil Users
     */
    $(document).on("click", '#button-submit-perfil', function(e) {
        e.preventDefault();
        var data = {type: "post", url: $('#form').attr('action'), form: "#form", doAfter: "moda_hidden", modal:"#preview-users", messageError: ".modal-response", showAlert: true, titleResponse: "Exito!", textResponse: "Datos Actualizados Correctamente"};
        DOM.submitData(data);
    });


});

/* Validación de número de cédula dominicana
 * con longitud de 11 caracteres numéricos o 13 caracteres incluyendo los dos guiones de presentación
 * ejemplo sin guiones 00116454281, ejemplo con guiones 001-1645428-1
 * el retorno es 1 para el caso de cédula válida y 0 para la no válida
 */
var isValidCedula = function(ced) {
    var c            = ced.replace(/-/g,''),
        cedula       = c.substr(0, c.length - 1),
        verificador  = c.substr(c.length - 1, 1),
        suma         = 0,
        res          = 0,
        uno          = 0,
        dos          = 0,
        el_numero    = 0,
        mod          = "",
        cedulaValida = 0;

    if(ced.length < 11) { return false; }

    for (var i = 0; i < cedula.length; i++) {

        if((i % 2) == 0){mod = 1} else {mod = 2}

        res = cedula.substr(i,1) * mod;

        if (res > 9) {
            res = res.toString();
            uno = res.substr(0,1);
            dos = res.substr(1,1);
            res = eval(uno) + eval(dos);
        }

        suma += eval(res);
    }

    el_numero = (10 - (suma % 10)) % 10;

    if (el_numero == verificador && cedula.substr(0,3) != "000"){
        cedulaValida = 1;
    }
    else{
        cedulaValida = 0;
    }

    return cedulaValida;
};

!function($) {
    "use strict";
	
    var Sidemenu = function() {
        this.$body = $("body"),
        this.$openLeftBtn = $(".open-left"),
        this.$menuItem = $("#sidebar-menu a")
    };
    Sidemenu.prototype.openLeftBar = function() {
      $("#main").toggleClass("enlarged");
      $("#main").addClass("forced");

      if($("#main").hasClass("enlarged") && $("body").hasClass("adminbody")) {
        $("body").removeClass("adminbody").addClass("adminbody-void");
      } else if(!$("#main").hasClass("enlarged") && $("body").hasClass("adminbody-void")) {
        $("body").removeClass("adminbody-void").addClass("adminbody");
      }

      if($("#main").hasClass("enlarged")) {
        $(".left ul").removeAttr("style");
      } else {
        $(".subdrop").siblings("ul:first").show();
      }

    },
    //menu item click
    Sidemenu.prototype.menuItemClick = function(e) {
       if(!$("#main").hasClass("enlarged")){
        if($(this).parent().hasClass("submenu")) {

        }
        if(!$(this).hasClass("subdrop")) {
          // hide any open menus and remove all other classes
          $("ul",$(this).parents("ul:first")).slideUp(350);
          $("a",$(this).parents("ul:first")).removeClass("subdrop");
          $("#sidebar-menu .pull-right i").removeClass("md-remove").addClass("md-add");

          // open our new menu and add the open class
          $(this).next("ul").slideDown(350);
          $(this).addClass("subdrop");
          $(".pull-right i",$(this).parents(".submenu:last")).removeClass("md-add").addClass("md-remove");
          $(".pull-right i",$(this).siblings("ul")).removeClass("md-remove").addClass("md-add");
        }else if($(this).hasClass("subdrop")) {
          $(this).removeClass("subdrop");
          $(this).next("ul").slideUp(350);
          $(".pull-right i",$(this).parent()).removeClass("md-remove").addClass("md-add");
        }
      }
    },

    //init sidemenu
    Sidemenu.prototype.init = function() {
      var $this  = this;

      var ua = navigator.userAgent,
        event = (ua.match(/iP/i)) ? "touchstart" : "click";

      //bind on click
      this.$openLeftBtn.on(event, function(e) {
        e.stopPropagation();
        $this.openLeftBar();
      });

      // LEFT SIDE MAIN NAVIGATION
      $this.$menuItem.on(event, $this.menuItemClick);

      // NAVIGATION HIGHLIGHT & OPEN PARENT
      $("#sidebar-menu ul li.submenu a.active").parents("li:last").children("a:first").addClass("active").trigger("click");
    },

    //init Sidemenu
    $.Sidemenu = new Sidemenu, $.Sidemenu.Constructor = Sidemenu

}(window.jQuery),


//main app module
 function($) {
    "use strict";

    var App = function() {        
        this.pageScrollElement = "html, body",
        this.$body = $("body")
    };

     //on doc load
    App.prototype.onDocReady = function(e) {
      // FastClick.attach(document.body);
      resizefunc.push("changeptype");

      $('.animate-number').each(function(){
        $(this).animateNumbers($(this).attr("data-value"), true, parseInt($(this).attr("data-duration")));
      });

      //RUN RESIZE ITEMS
      $(window).resize(debounce(resizeitems,100));
      $("body").trigger("resize");

      // right side-bar toggle
      $('.right-bar-toggle').on('click', function(e){

          $('#main').toggleClass('right-bar-enabled');
      });


    },
    //initilizing
    App.prototype.init = function() {
        var $this = this;
        $(document).ready($this.onDocReady);
        $.Sidemenu.init();
    },

    $.App = new App, $.App.Constructor = App

}(window.jQuery),

//initializing main application module
function($) {
    "use strict";
    $.App.init();
}(window.jQuery);

function executeFunctionByName(functionName, context) {
  var args = [].slice.call(arguments).splice(2);
  var namespaces = functionName.split(".");
  var func = namespaces.pop();
  for(var i = 0; i < namespaces.length; i++) {
    context = context[namespaces[i]];
  }
  return context[func].apply(this, args);
}
var w,h,dw,dh;
var changeptype = function(){
    w = $(window).width();
    h = $(window).height();
    dw = $(document).width();
    dh = $(document).height();

    if(jQuery.browser.mobile === true){
        $("body").addClass("mobile").removeClass("adminbody");
    }

    if(!$("#main").hasClass("forced")){
      if(w > 990){
        $("body").removeClass("smallscreen").addClass("widescreen");
          $("#main").removeClass("enlarged");
      }else{
        $("body").removeClass("widescreen").addClass("smallscreen");
        $("#main").addClass("enlarged");
        $(".left ul").removeAttr("style");
      }
      if($("#main").hasClass("enlarged") && $("body").hasClass("adminbody")){
        $("body").removeClass("adminbody").addClass("adminbody-void");
      }else if(!$("#main").hasClass("enlarged") && $("body").hasClass("adminbody-void")){
        $("body").removeClass("adminbody-void").addClass("adminbody");
      }

  }
  
}


var debounce = function(func, wait, immediate) {
  var timeout, result;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) result = func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) result = func.apply(context, args);
    return result;
  };
}

function resizeitems(){
  if($.isArray(resizefunc)){
    for (i = 0; i < resizefunc.length; i++) {
        window[resizefunc[i]]();
    }
  }
}
