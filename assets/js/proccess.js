var request = function(e){
    var params = false;

    if(e.type == "post"){
        params = $(e.form).serialize();
    }

    return $.ajax({
        type: e.type,
        url: e.url,
        data: params,
        cache : false,
        contenType: false,
        processData: false
    });
};

var ajaxRequest = function (r) {

    var params = false,
        response = 0;

    if(r.type == "post") {
        params = $(r.form).serialize();
    }

    $.ajax({
        async:  false,
        type:   r.type,
        url:    r.url,
        data:   params,
        success:function(data){
            response = data;
        },
        error:function(){
            response = {
                result: false,
                error: '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><i class="fa fa-exclamation-triangle"></i><h3>Advertencia:</h3><p>Hubo problemas contacte con el administrador</p></div>'
            };
        }
    });

    return JSON.parse(response);
};


