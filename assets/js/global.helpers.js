/**
 * Created by Lordmicro on 16/2/2019.
 */

/**
 *
 * @param value
 * @param array
 * @returns {boolean}
 */
var inArray = function(value, array) {
    return array.indexOf(parseInt(value)) > -1;
};

$(document).ready(function () {
    if($('.phone').length > 0){$('.phone').mask("999-999-9999");}
    if($('.cedula').length > 0){$('.cedula').mask("999-9999999-9");}
    if($('.hour').length > 0){$('.hour').mask("99:99");}
    if($('.ext').length > 0){$('.ext').mask("9999");}
    if($('.numeric').length > 0){$('.numeric').numeric({negative:false});}
    if($('.numeric-decimal').length > 0){$('.numeric-decimal').numeric({negative : false, decimalPlaces: 2 });}
    if($('.date').length > 0){
        $('.date').daterangepicker({singleDatePicker: true, showDropdowns: true, locale: {format: 'YYYY-MM-DD'}});
    }

    Ladda.bind('.ladda-button');

    $(document).on('click', '.cancel', function (e) {
        e.preventDefault();
        var thisSelector       = $(this).data(),
            title              = 'Atención',
            text               = 'Esta Seguro que Desea Salir y Perder los Cambios?',
            type               = 'warning',
            confirmButtonColor = '#3085d6',
            cancelButtonColor  = '#d33',
            confirmButtonText  = 'OK',
            cancelButtonText   = 'Cancelar';
            swal({
                title              : title,
                text               : text,
                type               : type,
                showCancelButton   : true,
                confirmButtonColor : confirmButtonColor,
                cancelButtonColor  : cancelButtonColor,
                confirmButtonText  : confirmButtonText,
                cancelButtonText   : cancelButtonText,
            }).then((result) => {
                if (result.value == true) {
                window.location = thisSelector.redirect;
            }
        });
    });

    $(document).on('click', '.cancel_document', function (e) {
        e.preventDefault();
        var thisSelector       = $(this).data(),
            title              = 'Atención',
            text               = 'Esta Seguro que Desea Cancelar este Documento?',
            type               = 'warning',
            confirmButtonColor = '#3085d6',
            cancelButtonColor  = '#d33',
            confirmButtonText  = 'OK',
            cancelButtonText   = 'Cancelar';
        swal({
            title              : title,
            text               : text,
            type               : type,
            showCancelButton   : true,
            confirmButtonColor : confirmButtonColor,
            cancelButtonColor  : cancelButtonColor,
            confirmButtonText  : confirmButtonText,
            cancelButtonText   : cancelButtonText,
        }).then((result) => {
            if (result.value == true) {
            var res = DOM.getValue({type:'get', url:thisSelector.url, action:'return'});

            if(res.result != 0){
                $('#list').DataTable().ajax.reload();
            }
        }
    });
    });

    $(document).on('click', '.redirect', function (e) {
        e.preventDefault();
        var thisSelector = $(this).data(),
            url          = thisSelector.url,
            res          = DOM.getValue({type:'get', url:url, action:'return'});

        if(res.result != 0){
            window.location = res.url;
        }
    });

    $(document).on('click', '.trigger_print', function () {
        printExternalUrl('print_content');
    });

    $(document).on('click', '.export_word', function (e) {
        e.preventDefault();
        creaWord('print_content','document');
    });

    $(document).on('click', '.export_excel', function (e) {
        e.preventDefault();
        exportTableToExcel();
    });

    $(document).on('change', '.currency-format, .currency, .number', function() {
        $(this).val(formatCurrency($(this).val()));
    });

});

$(function() {
    $('.chosen-select').chosen({ width: '100%' });
});

var printExternalUrl = function (selector) {
    var content         = document.getElementById(selector).innerHTML,
       originContent    = document.body.innerHTML;

    document.body.innerHTML = content;
    window.print();
    document.body.innerHTML = originContent;
};

function creaWord(element, filename){
    var preHtml = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40"><head><meta charset="utf-8"><title>Export HTML To Doc</title></head><body>',
       postHtml = "</body></html>",
           html = preHtml+document.getElementById(element).innerHTML+postHtml;

    var blob = new Blob(['ufeff', html], {
        type: 'application/msword'
    });

    // Specify link url
    var url = 'data:application/vnd.ms-word;charset=utf-8,' + encodeURIComponent(html);

    // Specify file name
    filename = filename?filename+'.doc':'document.doc';

    // Create download link element
    var downloadLink = document.createElement("a");

    document.body.appendChild(downloadLink);

    if(navigator.msSaveOrOpenBlob ){
        navigator.msSaveOrOpenBlob(blob, filename);
    }else{
        // Create a link to the file
        downloadLink.href = url;

        // Setting the file name
        downloadLink.download = filename;

        //triggering the function
        downloadLink.click();
    }

    document.body.removeChild(downloadLink);
}

function exportTableToExcel(){
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent($('.card-body').html()));
}

var clearAmount = function(str) {
    return (typeof str =='string')?str.toString().replace(/\$|\,/g,''):'';
};

var formatCurrency = function(num) {
    num             = ((typeof num !== 'undefined') && (num !== null))? num : 0;
    num             = num.toString().replace(/\$|\,/g,'');

    if(isNaN(num)){
        num         = "0";
    }

    sign            = (num == (num = Math.abs(num)));
    num             = Math.floor(num*100+0.50000000001);
    cents           = num%100;
    num             = Math.floor(num/100).toString();

    if(cents < 10) {
        cents       = "0" + cents;
    }

    for(var i = 0; i < Math.floor((num.length-(1+i))/3); i++) {

        num = num.substring(0,num.length-(4*i+3))+','+ num.substring(num.length-(4*i+3));

    }

    return (((sign)?'':'-') + num + '.' + cents);
};

var operator = function (number1, number2, math) {

    var calculate                               = 0;

    switch(math) {
        case '+':
            calculate                           = parseFloat(number1) + parseFloat(number2);
            break;
        case '-':
            calculate                           = parseFloat(number1) - parseFloat(number2);
            break;
        case '/':
            calculate                           = parseFloat(number1) / parseFloat(number2);
            break;
        case '*':
            calculate                           = parseFloat(number1) * parseFloat(number2);
            break;
    }

    return parseFloat(calculate).toFixed(2);
};