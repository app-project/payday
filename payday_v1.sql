/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100138
 Source Host           : localhost:3306
 Source Schema         : payday_v1

 Target Server Type    : MySQL
 Target Server Version : 100138
 File Encoding         : 65001

 Date: 24/03/2019 18:44:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ai_calendar_events
-- ----------------------------
DROP TABLE IF EXISTS `ai_calendar_events`;
CREATE TABLE `ai_calendar_events`  (
  `eventId` int(11) NOT NULL AUTO_INCREMENT,
  `calendarId` int(11) NULL DEFAULT 0,
  `userId` int(11) NULL DEFAULT 0,
  `description` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `color` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `amount` decimal(11, 2) NULL DEFAULT 0.00,
  `date_start` date NULL DEFAULT NULL,
  `date_end` date NULL DEFAULT NULL,
  `all_day` tinyint(1) NULL DEFAULT 1,
  `time_start` varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `time_end` varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `repeat_type` tinyint(1) NULL DEFAULT 0,
  `rrule` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `creation_date` date NULL DEFAULT NULL,
  `hidden` tinyint(1) NULL DEFAULT 0,
  `is_showed` tinyint(1) NULL DEFAULT 0,
  `count` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`eventId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ai_calendar_events
-- ----------------------------
INSERT INTO `ai_calendar_events` VALUES (1, 2, 1, 'Cobrarle a Miguel Lopez', NULL, 3800.00, '2019-02-02', '2019-02-15', 1, '10:00', '11:30', 0, '', '2019-02-16', 1, 0, 0);
INSERT INTO `ai_calendar_events` VALUES (2, 1, 1, 'prueba de evento', NULL, 1500.00, '2019-02-16', '2019-02-16', 1, '10:00', '11:30', 0, '', '2019-02-16', 0, 1, 1);
INSERT INTO `ai_calendar_events` VALUES (3, 2, 1, 'mensaje', NULL, 1500.00, '2019-02-16', '2019-02-16', 1, '10:00', '11:30', 0, 'RRULE:dtstart=20190216;freq=DAILY;interval=1;until=20190216', '2019-02-16', 1, 0, 0);
INSERT INTO `ai_calendar_events` VALUES (4, 1, 1, 'hola como estas', NULL, 1500.00, '2019-02-17', '2019-02-17', 1, '10:00', '11:30', 0, '', '2019-02-17', 1, 1, 1);
INSERT INTO `ai_calendar_events` VALUES (5, 5, 1, 'fsdfsd', NULL, 0.00, '2019-02-19', '2019-02-17', 1, '10:00', '11:30', 0, '', '2019-02-17', 1, 0, 0);

-- ----------------------------
-- Table structure for ai_calendar_events_reminder
-- ----------------------------
DROP TABLE IF EXISTS `ai_calendar_events_reminder`;
CREATE TABLE `ai_calendar_events_reminder`  (
  `reminderId` int(11) NOT NULL AUTO_INCREMENT,
  `eventId` int(11) NULL DEFAULT 0,
  `typeId` int(11) NULL DEFAULT 0,
  `reminder_interval` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `before_date` int(11) NULL DEFAULT 0,
  `reminder_at` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`reminderId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ai_calendar_events_reminder
-- ----------------------------
INSERT INTO `ai_calendar_events_reminder` VALUES (1, 1, 1, 'DAY', 1, NULL);
INSERT INTO `ai_calendar_events_reminder` VALUES (2, 2, 1, 'DAY', 1, NULL);
INSERT INTO `ai_calendar_events_reminder` VALUES (3, 3, 1, 'DAY', 1, NULL);
INSERT INTO `ai_calendar_events_reminder` VALUES (4, 4, 1, 'DAY', 1, NULL);
INSERT INTO `ai_calendar_events_reminder` VALUES (5, 5, 1, 'DAY', 1, NULL);

-- ----------------------------
-- Table structure for ai_calendar_index
-- ----------------------------
DROP TABLE IF EXISTS `ai_calendar_index`;
CREATE TABLE `ai_calendar_index`  (
  `indexId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL DEFAULT 0,
  `eventIds` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`indexId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ai_calendar_recurrency
-- ----------------------------
DROP TABLE IF EXISTS `ai_calendar_recurrency`;
CREATE TABLE `ai_calendar_recurrency`  (
  `frequencyId` int(11) NOT NULL AUTO_INCREMENT,
  `value` int(3) NULL DEFAULT 0,
  `seconds` int(11) NOT NULL DEFAULT 0,
  `period` int(11) NOT NULL DEFAULT 0,
  `period_type` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `rrule` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `name` varchar(250) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `text` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`frequencyId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ai_calendar_recurrency
-- ----------------------------
INSERT INTO `ai_calendar_recurrency` VALUES (1, 1, 86400, 1, 'DAY', 'DAILY', 'Diario', 'días');
INSERT INTO `ai_calendar_recurrency` VALUES (2, 7, 604800, 1, 'WEEK', 'WEEKLY', 'Semanal', 'semanas');
INSERT INTO `ai_calendar_recurrency` VALUES (3, 30, 2592000, 1, 'MONTH', 'MONTHLY', 'Mensual', 'meses');
INSERT INTO `ai_calendar_recurrency` VALUES (4, 365, 31536000, 1, 'YEAR', 'YEARLY', 'Anual', 'años');

-- ----------------------------
-- Table structure for ai_calendar_reminder_types
-- ----------------------------
DROP TABLE IF EXISTS `ai_calendar_reminder_types`;
CREATE TABLE `ai_calendar_reminder_types`  (
  `typeId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`typeId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ai_calendar_reminder_types
-- ----------------------------
INSERT INTO `ai_calendar_reminder_types` VALUES (1, 'Email', 'fa-envelope-o');

-- ----------------------------
-- Table structure for ai_calendars_type
-- ----------------------------
DROP TABLE IF EXISTS `ai_calendars_type`;
CREATE TABLE `ai_calendars_type`  (
  `calendarId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `color` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `creation_date` datetime(0) NULL DEFAULT NULL,
  `hidden` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`calendarId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ai_calendars_type
-- ----------------------------
INSERT INTO `ai_calendars_type` VALUES (1, 'Nomina', '#263a4d', '2019-02-03 03:44:40', 0);
INSERT INTO `ai_calendars_type` VALUES (2, 'Cobros', '#f7000f', '2019-02-03 03:44:50', 0);
INSERT INTO `ai_calendars_type` VALUES (3, 'Pagos', '#00ff19', '2019-02-03 03:44:59', 0);
INSERT INTO `ai_calendars_type` VALUES (4, 'Visita', '#1a09db', '2019-02-03 03:45:18', 0);
INSERT INTO `ai_calendars_type` VALUES (5, 'Cobro compulsivo', '#b54983', '2019-02-17 16:15:16', 1);

-- ----------------------------
-- Table structure for ai_clients
-- ----------------------------
DROP TABLE IF EXISTS `ai_clients`;
CREATE TABLE `ai_clients`  (
  `clientId` int(11) NOT NULL AUTO_INCREMENT,
  `bankId` int(11) NULL DEFAULT 0,
  `living_placeId` int(11) NULL DEFAULT 0,
  `document` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `first_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_work` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ext_phone_work` varchar(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email_work` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `name_facebook` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `link_facebook` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `payment_dayId` int(11) NULL DEFAULT 0,
  `user_bank` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password_bank` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password_transactional` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `street` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `residency` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sector` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `province` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `entering_by` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `between_the_street` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `reference_point` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `living_color` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `apartament_of_house` tinyint(1) NULL DEFAULT 0,
  `levels_apartment` int(11) NULL DEFAULT 0,
  `iron_color` int(11) NULL DEFAULT 0,
  `awning_color` int(11) NULL DEFAULT 0,
  `remains_apartment` int(11) NULL DEFAULT 0,
  `from_hour` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `to_hour` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `day_work` date NULL DEFAULT NULL,
  `company` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sector_company` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `address_company` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `address_company_number` int(11) NULL DEFAULT 0,
  `time_workingId` int(11) NULL DEFAULT 0,
  `department_working` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `post_working` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `day_ingress` date NULL DEFAULT NULL,
  `salary` decimal(10, 2) NULL DEFAULT NULL,
  `mobile_boss` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `first_boss` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `last_boss` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_boss` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `document_reference` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `first_reference` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `last_reference` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `address_reference` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone_reference` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `bank_account` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mobile` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `type_accountId` int(11) NULL DEFAULT 0,
  `requested_amount` decimal(10, 2) NULL DEFAULT 0.00,
  `image` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `hidden` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`clientId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ai_clients
-- ----------------------------
INSERT INTO `ai_clients` VALUES (2, 1, 1, '001-0585592-8', 'Julia Moreta', 'Google Quezada', 'calle luna calle sol', '484-984-9488', '647-949-4988', '8484', 'julia04@gmail.com', 'juliatrabajo@gmail.com', '', '', 2, 'julia04', '123', '123', '8400 NW 25th St, STE 110 #20-00300', '05', 'Capotillo', 'Florida', 'Charle', '8400 NW 25th St', 'bomba de gas', '1', 1, 1, 1, 1, 2, '', '', '2019-02-28', 'Supliequipos Genesis', 'mameyes', 'isabelita', 0, 1, '', '', '2019-02-10', 22500.00, '', '', '', '', '', '', '', '', '', '6546545645646', '894-949-8488', 1, 18000.00, NULL, 0);
INSERT INTO `ai_clients` VALUES (3, 1, 1, '001-0078278-8', 'Danilo Medina', 'Sanchez', NULL, '484-848-4848', '324-234-2342', '1234', 'danilo@gmail.com', 'trabajo@gmail.com', '', '', 1, 'dani', '123', '123456', '8400 NW 25th St, STE 110 #20-00300', '11', 'brisas del este', 'Florida', 'calle', '8400 NW 25th St', 'charle', '2', 1, 1, 1, 2, 1, '9:17 AM', '9:17 AM', '2019-02-28', 'BmCargo', 'BmCargo', 'BmCargo', 0, 9, 'en su casa', 'secretario', '2019-02-23', 250000.00, '844-848-4848', 'Sterling', 'Jose', '484-848-4848', '465-4564564-5', 'Sterling', 'Juan', 'Ave. 4ta #18 Brisas Del Este', '809-974-4162', '684894894', '848-949-8489', 1, 150000.00, NULL, 0);

-- ----------------------------
-- Table structure for ai_clients_status
-- ----------------------------
DROP TABLE IF EXISTS `ai_clients_status`;
CREATE TABLE `ai_clients_status`  (
  `statusId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `color` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`statusId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ai_clients_type_accounts
-- ----------------------------
DROP TABLE IF EXISTS `ai_clients_type_accounts`;
CREATE TABLE `ai_clients_type_accounts`  (
  `typeId` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `hidden` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`typeId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ai_clients_type_accounts
-- ----------------------------
INSERT INTO `ai_clients_type_accounts` VALUES (1, 'CA', 'Cuenta de Ahorro', 0);
INSERT INTO `ai_clients_type_accounts` VALUES (2, 'CC', 'Cuenta Corriente', 0);

-- ----------------------------
-- Table structure for ai_compulsive_payments_items
-- ----------------------------
DROP TABLE IF EXISTS `ai_compulsive_payments_items`;
CREATE TABLE `ai_compulsive_payments_items`  (
  `compulsive_itemId` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) NULL DEFAULT 0,
  `loan_analysiId` int(11) NULL DEFAULT 0,
  `date` date NULL DEFAULT NULL,
  `capital` decimal(11, 2) NULL DEFAULT 0.00,
  `interest` decimal(11, 2) NULL DEFAULT 0.00,
  `late_payment` decimal(11, 2) NULL DEFAULT 0.00,
  `hidden` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`compulsive_itemId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ai_compulsive_payments_items
-- ----------------------------
INSERT INTO `ai_compulsive_payments_items` VALUES (1, 2, 1, '2019-03-23', 18000.00, 1616.76, 0.00, 0);
INSERT INTO `ai_compulsive_payments_items` VALUES (2, 2, 1, '2019-03-23', 16383.24, 1616.76, 0.00, 0);
INSERT INTO `ai_compulsive_payments_items` VALUES (3, 2, 1, '2019-03-23', 14766.48, 1616.76, 0.00, 0);
INSERT INTO `ai_compulsive_payments_items` VALUES (4, 2, 1, '2019-03-23', 13149.72, 1616.76, 0.00, 0);

-- ----------------------------
-- Table structure for ai_docs
-- ----------------------------
DROP TABLE IF EXISTS `ai_docs`;
CREATE TABLE `ai_docs`  (
  `docId` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) NOT NULL DEFAULT 0,
  `original_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `size` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `hidden` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`docId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ai_docs
-- ----------------------------
INSERT INTO `ai_docs` VALUES (1, 3, '375682_223919_1.jpg', 'XE4lI20190324091151.jpg', '107077', 1);
INSERT INTO `ai_docs` VALUES (2, 3, '375682_223919_1.jpg', 't3MPn20190324092151.jpg', '107077', 0);

-- ----------------------------
-- Table structure for ai_institution_banks
-- ----------------------------
DROP TABLE IF EXISTS `ai_institution_banks`;
CREATE TABLE `ai_institution_banks`  (
  `bankId` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `description` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `image` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `status` tinyint(1) NULL DEFAULT 0,
  `exclude` tinyint(1) NULL DEFAULT 0,
  `hidden` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`bankId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 54 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ai_institution_banks
-- ----------------------------
INSERT INTO `ai_institution_banks` VALUES (1, '001', 'Banco Popular Dominicano', 'banco popular dominicano de la republica dominicana', '3vQ_laFWiTw806BohAt1nVyebg7JqXcRpGKNPMImx5jsfCOzHZr9L2uEd4UDkYS.png', 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (2, '002', 'Banco Central de la República Dominicana', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (4, '004', 'Banco BHD Leon', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (5, '005', 'Banco del Progreso', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (6, '006', 'Banco Santa Cruz', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (7, '007', 'Banco Caribe', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (8, '008', 'Banco BDI', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (9, '009', 'Banco Vimenca', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (10, '010', 'Banco Lopez de Haro', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (11, '011', 'Bancamérica', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (12, '012', 'Banesco', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (13, '013', 'Scotiabank', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (14, '014', 'Banco Promerica', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (15, '015', 'Banco Atlántico', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (16, '016', 'Banco Bancotuí', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (17, '017', 'Banco BDA', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (18, '018', 'Banco Adopem', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (19, '019', 'Banco Agrícola', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (20, '020', 'Banco Pyme Bhd', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (21, '021', 'Banco Ademi', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (22, '022', 'Banco Capital', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (23, '023', 'Banco Confisa', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (24, '024', 'Banco De Desarrollo Idecosa', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (25, '025', 'Banco Empire', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (26, '026', 'Banco Motor Credito', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (27, '027', 'Banco Río', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (28, '028', 'Banco Providencial', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (29, '029', 'Banco Del Caribe', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (30, '030', 'Banco Inmobiliario (Banaci)', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (31, '031', 'Banco Gruficorp', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (32, '032', 'Banco Cofaci', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (33, '033', 'Banco Atlas', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (34, '034', 'Banco Bonanza', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (35, '035', 'Banco Bellbank', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (36, '036', 'Banco Fihogar', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (37, '037', 'Banco Federal', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (38, '038', 'Banco Micro', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (39, '039', 'Banco Unión', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (40, '040', 'Kneutt F.Bank', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (41, '041', 'Asociación Popular de Ahorros y Préstamos', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (42, '042', 'Asociación Cibao', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (43, '043', 'Asociación Nortena', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (44, '044', 'Asociación Romana', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (45, '045', 'Asociación Higuamo', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (46, '046', 'Asociación La Vega Real', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (47, '047', 'Asociación Duarte', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (48, '048', 'Asociación Barahona', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (49, '049', 'Asociación Maguana', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (50, '050', 'Asociación Mocana', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (51, '051', 'Asociación Bonao', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (52, '052', 'Asociación La Nacional', NULL, NULL, 1, 0, 0);
INSERT INTO `ai_institution_banks` VALUES (53, '053', 'Asociación Noroestana', NULL, NULL, 1, 0, 0);

-- ----------------------------
-- Table structure for ai_living_places
-- ----------------------------
DROP TABLE IF EXISTS `ai_living_places`;
CREATE TABLE `ai_living_places`  (
  `living_placeId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `hidden` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`living_placeId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ai_living_places
-- ----------------------------
INSERT INTO `ai_living_places` VALUES (1, 'Propia', 0);
INSERT INTO `ai_living_places` VALUES (2, 'Alquilado', 0);

-- ----------------------------
-- Table structure for ai_loan_analysis
-- ----------------------------
DROP TABLE IF EXISTS `ai_loan_analysis`;
CREATE TABLE `ai_loan_analysis`  (
  `loan_analysiId` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NULL DEFAULT 0,
  `date_issue` date NULL DEFAULT NULL,
  `date_creation` datetime(0) NULL DEFAULT NULL,
  `clientId` int(11) NULL DEFAULT 0,
  `request_code` int(11) NULL DEFAULT 0,
  `statusId` int(11) NULL DEFAULT 0,
  `notes` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `total_deposit` decimal(10, 2) NULL DEFAULT 0.00,
  `average_deposit` decimal(10, 2) NULL DEFAULT 0.00,
  `average_salary` decimal(10, 2) NULL DEFAULT 0.00,
  `no_deposit_percent` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `payment_day_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `total_fact` decimal(10, 2) NULL DEFAULT 0.00,
  `average_salary_fat` decimal(10, 2) NULL DEFAULT 0.00,
  `fact_percent` int(11) NULL DEFAULT 0,
  `fact_percent_total` decimal(10, 2) NULL DEFAULT 0.00,
  `approved_fact` decimal(10, 2) NULL DEFAULT 0.00,
  `monthly_fee_percent` int(11) NULL DEFAULT 0,
  `monthly_fee` decimal(10, 2) NULL DEFAULT 0.00,
  `quota_monthly` decimal(10, 2) NULL DEFAULT 0.00,
  `biweekly_quota` decimal(10, 2) NULL DEFAULT 0.00,
  `weekly_quota` decimal(10, 2) NULL DEFAULT 0.00,
  `document` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `first_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `company` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sector_company` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `apartament_of_house` tinyint(1) NULL DEFAULT 0,
  `bankId` int(11) NULL DEFAULT 0,
  `type_accountId` int(11) NULL DEFAULT 0,
  `bank_account` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mobile` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `salary` decimal(10, 2) NULL DEFAULT 0.00,
  `post_working` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `department_working` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_bank` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password_bank` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `sent_mail` tinyint(1) NULL DEFAULT 0,
  `hidden` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`loan_analysiId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ai_loan_analysis
-- ----------------------------
INSERT INTO `ai_loan_analysis` VALUES (1, 1, '2019-02-10', '2019-02-10 23:10:17', 2, 1, 1, 'Prestamo de Emergencia', 43113.57, 12318.16, 14371.19, '3.5', 'Quincenal', 43113.57, 14371.19, 50, 7185.60, 7185.60, 45, 45.00, 3233.52, 1616.76, 808.38, '001-0585592-8', 'Julia Moreta', 'Google Quezada', 'Supliequipos Genesis', 'mameyes', 1, 1, 1, '6546545645646', '484-984-9488', '894-949-8488', 22500.00, '', '', 'julia04', '123', 1, 0);
INSERT INTO `ai_loan_analysis` VALUES (5, 1, '2019-02-17', '2019-02-17 20:19:43', 0, 0, 0, NULL, 0.00, 0.00, 0.00, NULL, NULL, 0.00, 0.00, 0, 0.00, 0.00, 0, 0.00, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, 0.00, NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `ai_loan_analysis` VALUES (6, 1, '2019-02-27', '2019-02-27 14:56:38', 0, 0, 0, NULL, 0.00, 0.00, 0.00, NULL, NULL, 0.00, 0.00, 0, 0.00, 0.00, 0, 0.00, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, 0.00, NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `ai_loan_analysis` VALUES (7, 1, '2019-02-27', '2019-02-27 19:48:23', 0, 0, 0, NULL, 0.00, 0.00, 0.00, NULL, NULL, 0.00, 0.00, 0, 0.00, 0.00, 0, 0.00, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, 0.00, NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `ai_loan_analysis` VALUES (8, 1, '2019-03-09', '2019-03-09 21:49:31', 0, 0, 0, NULL, 0.00, 0.00, 0.00, NULL, NULL, 0.00, 0.00, 0, 0.00, 0.00, 0, 0.00, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, 0.00, NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `ai_loan_analysis` VALUES (9, 1, '2019-03-16', '2019-03-16 18:30:32', 0, 0, 0, NULL, 0.00, 0.00, 0.00, NULL, NULL, 0.00, 0.00, 0, 0.00, 0.00, 0, 0.00, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, 0.00, NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `ai_loan_analysis` VALUES (10, 1, '2019-03-23', '2019-03-23 08:08:21', 0, 0, 0, NULL, 0.00, 0.00, 0.00, NULL, NULL, 0.00, 0.00, 0, 0.00, 0.00, 0, 0.00, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, 0.00, NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `ai_loan_analysis` VALUES (11, 1, '2019-03-23', '2019-03-23 08:12:13', 0, 0, 0, NULL, 0.00, 0.00, 0.00, NULL, NULL, 0.00, 0.00, 0, 0.00, 0.00, 0, 0.00, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, 0.00, NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `ai_loan_analysis` VALUES (12, 1, '2019-03-23', '2019-03-23 08:13:25', 3, 2, 3, '', 22500.00, 6428.57, 7500.00, '3.5', 'Mensual', 22500.00, 7500.00, 50, 3750.00, 3750.00, 45, 45.00, 1687.50, 843.75, 421.88, '001-0078278-8', 'Danilo Medina', 'Sanchez', 'BmCargo', 'BmCargo', 1, 1, 1, '684894894', '484-848-4848', '848-949-8489', 250000.00, 'secretario', 'en su casa', 'dani', '123', 0, 0);
INSERT INTO `ai_loan_analysis` VALUES (13, 1, '2019-03-23', '2019-03-23 08:18:33', 0, 0, 0, NULL, 0.00, 0.00, 0.00, NULL, NULL, 0.00, 0.00, 0, 0.00, 0.00, 0, 0.00, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, 0.00, NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `ai_loan_analysis` VALUES (14, 1, '2019-03-23', '2019-03-23 08:26:26', 0, 0, 0, NULL, 0.00, 0.00, 0.00, NULL, NULL, 0.00, 0.00, 0, 0.00, 0.00, 0, 0.00, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, 0.00, NULL, NULL, NULL, NULL, 0, 1);
INSERT INTO `ai_loan_analysis` VALUES (15, 1, '2019-03-23', '2019-03-23 11:41:19', 3, 3, 3, '', 1000.00, 285.71, 333.33, '3.5', 'Mensual', 1000.00, 333.33, 50, 166.67, 166.67, 45, 45.00, 75.00, 37.50, 18.75, '001-0078278-8', 'Danilo Medina', 'Sanchez', 'BmCargo', 'BmCargo', 1, 1, 1, '684894894', '484-848-4848', '848-949-8489', 250000.00, 'secretario', 'en su casa', 'dani', '123', 0, 0);
INSERT INTO `ai_loan_analysis` VALUES (16, 1, '2019-03-23', '2019-03-23 12:09:32', 0, 0, 0, NULL, 0.00, 0.00, 0.00, NULL, NULL, 0.00, 0.00, 0, 0.00, 0.00, 0, 0.00, 0.00, 0.00, 0.00, NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, 0.00, NULL, NULL, NULL, NULL, 0, 1);

-- ----------------------------
-- Table structure for ai_loan_analysis_deposit_items
-- ----------------------------
DROP TABLE IF EXISTS `ai_loan_analysis_deposit_items`;
CREATE TABLE `ai_loan_analysis_deposit_items`  (
  `itemId` int(11) NOT NULL AUTO_INCREMENT,
  `loan_analysiId` int(11) NULL DEFAULT 0,
  `deposit_day` date NULL DEFAULT NULL,
  `deposit_salary` decimal(10, 2) NULL DEFAULT NULL,
  `hidden` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`itemId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ai_loan_analysis_deposit_items
-- ----------------------------
INSERT INTO `ai_loan_analysis_deposit_items` VALUES (1, 1, '2019-02-10', 5249.42, 0);
INSERT INTO `ai_loan_analysis_deposit_items` VALUES (2, 1, '2019-02-10', 7762.42, 1);
INSERT INTO `ai_loan_analysis_deposit_items` VALUES (3, 1, '2019-02-10', 5955.09, 0);
INSERT INTO `ai_loan_analysis_deposit_items` VALUES (4, 1, '2019-02-10', 8468.10, 0);
INSERT INTO `ai_loan_analysis_deposit_items` VALUES (5, 1, '2019-02-10', 5955.09, 0);
INSERT INTO `ai_loan_analysis_deposit_items` VALUES (6, 1, '2019-02-10', 9723.45, 0);
INSERT INTO `ai_loan_analysis_deposit_items` VALUES (7, 12, '2019-03-23', 22500.00, 0);
INSERT INTO `ai_loan_analysis_deposit_items` VALUES (8, 15, '2019-03-23', 1000.00, 0);

-- ----------------------------
-- Table structure for ai_loan_analysis_status
-- ----------------------------
DROP TABLE IF EXISTS `ai_loan_analysis_status`;
CREATE TABLE `ai_loan_analysis_status`  (
  `statusId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `class` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`statusId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ai_loan_analysis_status
-- ----------------------------
INSERT INTO `ai_loan_analysis_status` VALUES (1, 'Desembolsado', '#FFC000');
INSERT INTO `ai_loan_analysis_status` VALUES (2, 'Cancelado', '#FF0000');
INSERT INTO `ai_loan_analysis_status` VALUES (3, 'Archivado', '#9e9c9c');
INSERT INTO `ai_loan_analysis_status` VALUES (4, 'Saldado', '#53c766');

-- ----------------------------
-- Table structure for ai_payment_days
-- ----------------------------
DROP TABLE IF EXISTS `ai_payment_days`;
CREATE TABLE `ai_payment_days`  (
  `payment_dayId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `hidden` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`payment_dayId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ai_payment_days
-- ----------------------------
INSERT INTO `ai_payment_days` VALUES (1, 'Mensual', 0);
INSERT INTO `ai_payment_days` VALUES (2, 'Quincenal', 0);
INSERT INTO `ai_payment_days` VALUES (3, 'Semanal', 0);

-- ----------------------------
-- Table structure for ai_remains_living
-- ----------------------------
DROP TABLE IF EXISTS `ai_remains_living`;
CREATE TABLE `ai_remains_living`  (
  `remains_livingId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `hidden` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`remains_livingId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ai_remains_living
-- ----------------------------
INSERT INTO `ai_remains_living` VALUES (1, 'Parte Atras', 0);
INSERT INTO `ai_remains_living` VALUES (2, 'Parte Delantera', 0);
INSERT INTO `ai_remains_living` VALUES (3, 'Callejon', 0);
INSERT INTO `ai_remains_living` VALUES (4, 'Principal', 0);

-- ----------------------------
-- Table structure for ai_security_questions
-- ----------------------------
DROP TABLE IF EXISTS `ai_security_questions`;
CREATE TABLE `ai_security_questions`  (
  `questionId` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) NULL DEFAULT 0,
  `question` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `response` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `hidden` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`questionId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ai_security_questions
-- ----------------------------
INSERT INTO `ai_security_questions` VALUES (1, 3, 'Cual es el Nombre de tu madre', 'Josefa', 0);

-- ----------------------------
-- Table structure for ai_settings
-- ----------------------------
DROP TABLE IF EXISTS `ai_settings`;
CREATE TABLE `ai_settings`  (
  `settingId` int(11) NOT NULL AUTO_INCREMENT,
  `companyId` int(11) NULL DEFAULT 0,
  `company` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `mobil` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `image` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `politics` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `hidden` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`settingId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ai_settings
-- ----------------------------
INSERT INTO `ai_settings` VALUES (1, 1, 'PRESTAME DINERO', 'franklinpaulino04@gmail.com', '809-564-6545', '829-456-4564', NULL, 'LE INFORMAMOS QUE LE HEMOS DESEMBOLSADO SU  PRÉSTAMO DE EMERGENCIA  A LA CUENTA SUMINISTRADA POR USTED, DE MANERA SATISFACTORIA.\r\nRECORDANDO LE QUE EN ESTOS PRESTAMOS DE EMERGENCIA NO ACEPTAMOS ATRASOS, NI TAMPOCO ACEPTAMOS QUE USTED NOS CANCELE EL  INTERNET BANKING, SEA EL ACCESO, TARJETA DE CÓDIGOS, TOKEN O PREGUNTAS DE SEGURIDAD, YA QUE  ESA ES NUESTRA GARANTÍA PARA REALIZAR EL COBRO.\r\nDESPUÉS DE LOS 30 DÍAS USTED PODRÁ SALDAR SU PRÉSTAMO CUANDO QUIERA, AUNQUE  LE DAREMOS HASTA 5 MESES PARA PAGARLO. USTED PAGARA EL 45% DE INTERÉS MENSUAL  PUDIENDO ABONARLE AL CAPITAL O SALDAR CUANDO QUIERA.\r\nINFORMÁNDOLE QUE NOSOTROS SOMOS UNA OFICINA INTERMEDIARIA ENTRE PRESTAMISTAS PELIGROSOS  DE LA CALLE Y USTED, DONDE NOSOTROS SOMOS LOS ENCARGADOS DE HACERLE LOS COBROS POR INTERNET BANKING.\r\nINFORMÁNDOLE QUE SI USTED NO PAGA POR INTERNET BANKING, UN EQUIPO DE COBROS COMPULSIVO CON ALTO GRADO DE PREPARACIÓN  Y BIEN EQUIPADOS PROCEDERÁ INMEDIATAMENTE A  BUSCARLO, LOCALIZARLO Y HACERLE EL COBRO DE MANERA PERSONAL, EN SU CASA, TRABAJO O DONDE USTED SE ENCUENTRE.\r\nRECOMENDANDO LE PAGAR SIEMPRE POR INTERNET BANKING PARA EVITARSE SITUACIONES FUTURAS CON ESAS PERSONAS QUE TIENEN TODOS LOS DATOS SUYOS, DIRECCIONES Y FAMILIARES.\r\nRECORDANDO LE  QUE USTED FIRMO UN PAGARE NOTARIAL EN DONDE PONE EN GARANTÍA, SU CASA, VEHÍCULO Y  AHORROS EN MANOS PROPIAS O DE TERCEROS SEGÚN EL CÓDIGO PROCESAL CIVIL EN SU ARTICULO 541. INFORMÁNDOLE QUE SI NOS QUEDA MAL  PODEMOS SOMETERLO ANTE LA FISCALIA  POR ESTAFA Y ABUSO DE CONFIANZA  Y EN OTROS CASOS COBRARNOS LA DEUDA CON SUS PRESTACIONES LABORALES. \r\nINFORMÁNDOLE QUE TENERMOS MAS DE 20 AÑOS DE EXPERIENCIA EN EL ÁREA DE COBROS, YA QUE SOMOS UNA OFICINA DE COBROS COMPULSIVOS, DEBIDO HA QUE HAY MUCHOS ESTAFADORES Y DELINCUENTES QUE SE PONEN A TOMAR MUCHOS PRESTAMOS POR INTERNET PARA NO PAGAR Y LIQUIDARSE CON EL CAPITAL PRESTADO.\r\nNOSOTROS HEMOS HECHO  UN NEGOCIO DE BUENA FE CON USTED PARA SACARLO DE UNA SITUACIÓN DE EMERGENCIA POR ESO CONFIAMOS EN USTED Y ESPERAMOS  QUE CUMPLA PARA QUE NO SE EXPONGA A LAS SITUACIONES EXPLICADAS ANTERIORMENTE.\r\nESTAMOS PARA SERVIRLE  Y RESOLVERLE SUS SITUACIONES DE EMERGENCIA.\r\n\r\nLicenciada Alfonsina Silva/Gerente Regional\r\nPRÉSTAME-DINERO/Oficina 809-263-8272/ Móvil (829) 432-7109', 0);

-- ----------------------------
-- Table structure for ai_time_working
-- ----------------------------
DROP TABLE IF EXISTS `ai_time_working`;
CREATE TABLE `ai_time_working`  (
  `time_workingId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `hidden` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`time_workingId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ai_time_working
-- ----------------------------
INSERT INTO `ai_time_working` VALUES (1, 'Menos de 1 año', 0);
INSERT INTO `ai_time_working` VALUES (2, '1 año', 0);
INSERT INTO `ai_time_working` VALUES (3, '2 años', 0);
INSERT INTO `ai_time_working` VALUES (4, '3 años', 0);
INSERT INTO `ai_time_working` VALUES (5, '4 años', 0);
INSERT INTO `ai_time_working` VALUES (6, '5 años', 0);
INSERT INTO `ai_time_working` VALUES (7, '6 años', 0);
INSERT INTO `ai_time_working` VALUES (8, '7 años', 0);
INSERT INTO `ai_time_working` VALUES (9, '8 años', 0);
INSERT INTO `ai_time_working` VALUES (10, '9 años', 0);
INSERT INTO `ai_time_working` VALUES (11, '10 años', 0);

-- ----------------------------
-- Table structure for ai_users
-- ----------------------------
DROP TABLE IF EXISTS `ai_users`;
CREATE TABLE `ai_users`  (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `statusId` int(11) NULL DEFAULT 0,
  `typeId` int(11) NULL DEFAULT 0,
  `first_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `image` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `hash` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `hidden` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`userId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ai_users
-- ----------------------------
INSERT INTO `ai_users` VALUES (1, 1, 1, 'Jesus Enmanuel', 'De La Cruz', 'edelacruz9713@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '20190302205627.png', NULL, 0);
INSERT INTO `ai_users` VALUES (2, 1, 1, 'Jose Miguel', 'Rojas Guzman', 'jose@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '6aqh620190304015940.jpg', NULL, 0);

-- ----------------------------
-- Table structure for ai_users_status
-- ----------------------------
DROP TABLE IF EXISTS `ai_users_status`;
CREATE TABLE `ai_users_status`  (
  `statusId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `class` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `hidden` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`statusId`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of ai_users_status
-- ----------------------------
INSERT INTO `ai_users_status` VALUES (1, 'Activo', NULL, 0);
INSERT INTO `ai_users_status` VALUES (2, 'Inactivo', NULL, 0);
INSERT INTO `ai_users_status` VALUES (3, 'Bloqueado', NULL, 0);

-- ----------------------------
-- View structure for ai_clients_view
-- ----------------------------
DROP VIEW IF EXISTS `ai_clients_view`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `ai_clients_view` AS SELECT
a.clientId,
a.document,
a.first_name,
a.last_name,
a.salary,
CONCAT(b.`code`,'-',b.`name`)bank_name,
a.address,
a.image,
a.phone
FROM 	
ai_clients AS a LEFT JOIN ai_institution_banks AS b ON b.bankId = a.bankId 
WHERE a.hidden = 0 ;

-- ----------------------------
-- View structure for ai_compulsive_payments_view
-- ----------------------------
DROP VIEW IF EXISTS `ai_compulsive_payments_view`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `ai_compulsive_payments_view` AS SELECT
	a.loan_analysiId,
	a.date_issue,
	CONCAT(a.first_name,' ', a.last_name) AS full_name,
	 CONCAT('#00', a.request_code) AS request_code,
	 a.payment_day_name AS payment_day_name,
CASE
	 WHEN b.payment_dayId = 1 THEN
	 a.quota_monthly
	 
	 WHEN b.payment_dayId = 2 THEN
	 a.biweekly_quota
	 
	 WHEN b.payment_dayId = 3 THEN
	 a.weekly_quota
	 
	 END AS payment_day_total,
	c.`name` AS `status`,
	c.class AS class,
	c.statusId,
	CONCAT('$',FORMAT(b.requested_amount,2)) AS requested_amount ,
	b.document,
	d.`name` AS bank
FROM
	ai_loan_analysis AS a
	LEFT JOIN ai_clients AS b ON a.clientId = b.clientId 
	LEFT JOIN ai_loan_analysis_status AS c ON a.statusId = c.statusId 
	LEFT JOIN ai_institution_banks AS d ON b.bankId = d.bankId
WHERE
	a.hidden = 0 AND b.hidden = 0 AND a.statusId IN(1,4) ;

-- ----------------------------
-- View structure for ai_institution_banks_view
-- ----------------------------
DROP VIEW IF EXISTS `ai_institution_banks_view`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `ai_institution_banks_view` AS SELECT
	bankId,
	code,
	name,
	description,
	image,
	exclude,
status 
FROM
	ai_institution_banks 
WHERE
	hidden = 0 ;

-- ----------------------------
-- View structure for ai_loan_analysis_view
-- ----------------------------
DROP VIEW IF EXISTS `ai_loan_analysis_view`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `ai_loan_analysis_view` AS SELECT
	a.loan_analysiId,
	a.date_issue,
	CONCAT(a.first_name,' ', a.last_name) AS full_name,
	 CONCAT('AP-', a.request_code) AS request_code,
	 a.total_deposit AS total_deposit,
	 a.payment_day_name AS payment_day_name,
	 a.quota_monthly AS quota_monthly,
	 a.biweekly_quota AS biweekly_quota,
	 a.weekly_quota AS weekly_quota,
	 a.sent_mail AS sent_mail,
	 a.clientId AS clientId,
	c.`name` AS `status`,
	c.class AS class,
	c.statusId
FROM
	ai_loan_analysis AS a
	LEFT JOIN ai_clients AS b ON a.clientId = b.clientId 
	LEFT JOIN ai_loan_analysis_status AS c ON a.statusId = c.statusId 
WHERE
	a.hidden = 0 AND b.hidden = 0 GROUP BY a.loan_analysiId ;

-- ----------------------------
-- View structure for ai_users_view
-- ----------------------------
DROP VIEW IF EXISTS `ai_users_view`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `ai_users_view` AS SELECT
	userId,
	first_name,
	last_name,
	typeId AS user_type,
	email,
	image,
	hidden
FROM
	ai_users WHERE hidden = 0 ;

SET FOREIGN_KEY_CHECKS = 1;
